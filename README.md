Mulit-resolution lattice
========================

The multi-resolution lattice (mrlattice) project aims to serve as a Python library for
creating spacetime lattices that are differently resolved across their
domain.

## Examples

<table>
<tr><th>LatticeArray</th><th>PulseLatticeArray</th><th>HomogeneousLatticeArray</th></tr>
<tr><td>
<img src="docs/imgs/latticearray_example.png" width="80%">
</td><td>
<img src="docs/imgs/pulse_example.png" width="100%">
</td><td>
<img src="docs/imgs/homogeneous_example.png" width="60%">
</td></tr>
<tr><th>SuperimposeLatticeArray</th><th>SuperimposePulseLatticeArray</th><th>And more to come ...</th></tr>
<tr><td>
<img src="docs/imgs/superimpose_example.png" width="100%">
</td><td>
<img src="docs/imgs/superimpose_pulse_example.png" width="100%">
</td><td>
<!-- <img src="docs/imgs/.png" width="100%"> -->
</td></tr>
</table>


## How to install

The mrlattice package can be installed via pip:
```
$ pip install path/to/mrlattice[jupyter,dev,test]
```
There are three install targets: `jupyter`, `dev` and `test`. The `jupyter` target installs
the [Jupyter](jupyter) environment containing `jupyter lab`. Use it if you are working with
[Jupyter](jupyter) notebooks and want to use the custom widgets provided by `mrlattice`.
The two install targets `dev` and `test` are used for development and are described in the
[contributing guidelines](CONTRIBUTING.md#testing-code). The `path/to/mrlattice` can be:
- The path to the root of a local clone of this repository.
- A git URL:  
  `git+https://gitlab.com/openpixi/mrlattice#egg=mrlattice`  
  `git+ssh://gitlab.com/openpixi/mrlattice#egg=mrlattice`  
  Or optionally with a version tag `.git@X.Y.Z`:  
  `git+https://gitlab.com/openpixi/mrlattice.git@X.Y.Z#egg=mrlattice`  
  `git+ssh://gitlab.com/openpixi/mrlattice.git@X.Y.Z#egg=mrlattice`  
  For private repositories the ssh connection requires the `git@` user:  
  `git+ssh://git@gitlab.com/openpixi/mrlattice#egg=mrlattice`  

If you want to work on the source code of the installed mrlattice package, it is helpful
to use the `-e` or `--editable` flag with `pip install`. Please refer to the
[contributing guidelines](CONTRIBUTING.md) for more information on how to install in
editable mode.

When a new release tag is available run:
```
$ pip install --upgrade path/to/project[jupyter,dev,test]
```
to update the mrlattice package.

In order to test if the mrlattice package installed correctly, just try to import it in
an interactive Python session:
```python
import mrlattice
```


## Package structure

The mrlattice package is organized as a [PyPI][pypi] package. The part of the source
code that will be distributed as the [PyPI][pypi] package is located in `src/`. All
additional folders are only relevant for development. If mrlattice is installed in
`-e` / `--editable` mode all the folders will be included in the clone of the repository.

mrlattice consists of three modules:
- `mrlattice.lattice`
- `mrlattice.plotting`
- `mrlattice.ipywidgets`

The `lattice` module provides three modules:

- `mrlattice.lattice.configure`
- `mrlattice.lattice.generate`
- `mrlattice.lattice.validate`

The `configure` module defines the two configuration classes:
- `LatticeArrayProperty`
- `LatticeArrayScheme`

These classes are used by all `LatticeArray` types for managing their configuration
properties, such as the number of coarsening borders, time steps, etc...

The `generate` module defines all `LatticeArray` types in various submodules:
- `default.LatticeArray`
- `default.SuperimposeLatticeArray`
- `advanced.HomogeneousLatticeArray`
- `advanced.PulseLatticeArray`
- `advanced.SuperimposePulseLatticeArray`

They are all available directly from the `generate` namespace:
```python
from mrlattice.lattice.generate import LatticeArray
```

Below is an UML class diagram that shows the relations between all classes. The
LatticeArray types that are still in development are grayed out.

![](docs/imgs/uml/uml_diag_latticearray.svg)

The `validate` module contains the custom error types:
- `ConfigurationError`
- `EmptyLatticeArrayError`
- `LatticeGenerationError`
- `ValidationError`

and also defines the `ValidateLattice` class. This class is used to validate `LatticeArray`
and child class configurations. Below is an UML class diagram that shows all the custom
error types.

![](docs/imgs/uml/uml_diag_exceptions.svg)

The `plotting` module is currently under development and will change significantly.
<!-- TBD: plotting module description -->

The `ipywidgets` module contains three custom widgets derived from widgets provided by
the [ipywidgets](ipywidgets) module:
- `LatticeArrayPlotWidget`
- `SuperimposeLatticeArrayPlotWidget`
- `PulseLatticeArrayPlotWidget`
- `SuperimposePulseLatticeArrayPlotWidget`
- `LatticeArrayModifierWidget`

The plot widgets allow to configure and plot `LatticeArray` types.
Only `LatticeArrayPlotWidgets` defines the widget code, however.
The other widgets are for convenience and set the `LatticeArray` type included in their
name as the default type for the widget.
Other `LatticeArray` types can be also used with the `LatticeArrayPlotWidget` by setting
the `lattice` argument to an instance of the desired type.
The modifier widgets allows to apply modifiers on previously generated `LatticeArray`
instances. To make use of these widgets, `mrlattice` needs to be installed with the
`jupyter` install target.


### Additional development ressources

The `test/` folder contains the unit tests which are discussed in the [contributing
guidelines](CONTRIBUTING.md#testing-code).

The `docs/` folder contains documentation as [Jupyter][jupyter] notebooks.
Additionally, there are notebooks used for prototyping features and developing the modules
of this package. Rerunning the notebooks requires a specific version of mrlattice which
will be disclosed in the notebook itself. If the version mismatches there will be a
warning issued.


<!-- External links -->
[pypi]: https://pypi.org/
[jupyter]: https://jupyter.readthedocs.io/en/latest/
[ipywidgets]: https://ipywidgets.readthedocs.io
