How to contribute
=================

1. [Fork][gitlab-fork] the project into your own GitLab account.
1. Create a new branch with a descriptive name for your changes.
1. Push your changes to your fork (or use the online GitLab IDE directly and commit your
   changes). Please make sure to follow the coding guidelines [discussed below](#writing-code).
1. Create a [merge request (MR)][gitlab-mr] and follow its discussion until the MR gets
   approved. Your changes have to pass GitLab CI/CD testing [described below](#testing-code).
   For a fast approval make sure your changes pass those tests before submitting the MR.

Alternatively, if you don't want to contribute code, you can create a new feature request
at the [GitLab issue tracker][gitlab-issues] for the project. Please make sure to describe
the feature in detail and provide a possible implementation outline and/or code examples
if possible.


# Found a bug?

Check the [GitLab issue tracker][gitlab-issues] if the bug has already been reported and
take part in the discussion. If there is no issue for your bug yet, please create a new
issue keeping the following in mind:

- Use a clear and descriptive title.
- Describe the exact steps to reproduce the problem. Provide code examples that reproduce
  the problem.
- Describe the behavior your observed and contrast it to the expected behavior. Provide
  example output (as console log or screenshots).

Include the following details about your environment:
- Which version of mrlattice you are using.
- If you are using a Python virtual environment. I.e. Anaconda, virtualenv or venv.
- What packages are installed alongside mrlattice.


# Writing code

Make sure to install the package in editable mode with both, the `dev` and `test` targets:
```
$ pip install -e path/to/mrlattice[dev,test]
```
`path/to/mrlattice` in the install specifier can be:
- A local path to a clone of this repository. The path must point to the project root
  folder.
- A git URL pointing to this site hosting the repository:  
  `git+https://gitlab.com/your_account/mrlattice#egg=mrlattice`  
  `git+ssh://gitlab.com/your_account/mrlattice#egg=mrlattice`  
  For private repositories the ssh connection requires the `git@` user:  
  `git+ssh://git@gitlab.com/your_account/mrlattice#egg=mrlattice`  
  The part before the `#egg` should be the URL of the fork on your GitLab account.

[Editable mode][pip-editable] makes changes to the source code transparent without having
to reinstall the package. Additionally, when installing from a git URL, this repository
will be automatically cloned. In virtual environments the default install location for
the source code will then be `<venv path>/src/mrlattice` and `<cwd>/src/mrlattice`
for global installs. The `--src` option can be used to modify this location (for help
see the [pip documentation][pip-vcs]). You can branch, commit and push from this repository
as usual. The targets will install the tools required for [testing the code](#testing-code).

When pushing a new release tag or a new release tag becomes available upstream use the
`--upgrade` option to force pip to upgrade the package, if it is already installed.
```
$ pip install --upgrade -e path/to/project[dev,test]
```


## Codestyle

The general codestyle adopted in this project is governed by the autoformatter [black][black].
Matching configurations for a set of linters are included in the `setup.cfg` file and will
help you adhere to the style. These linters are:
- [black][black]
- [flake8][flake8]
- [pydocstyle][pydocstyle]
- [pylint][pylint]

For the style of the docstrings [pydocstyle][pydocstyle] is configured to expect the
[Google style][google-docstyle].

Installing with the `dev` target will install all of these tools as dependencies. You
can use all of them from the command line or configure your IDE to use them. When called
from the root directory of the project, these tools should automatically read their
configuration from the `setup.cfg` file.

The options set in the `setup.cfg` file mark the line length for code to 88 characters
(black default) and for comments and docstrings to 72 (Python default). There are also a
collection of linter errors disabled that are explained in the `setup.cfg` file.

The codestyle guidelines are not enforced for `*.ipynb` Jupyter notebook files.


## IDE Integration

Serving as examples, the instructions for how to configure two IDEs are provided below:


### VS CODE

[Visual Studio Code (VS code)][vscode] is a very popular multi-platform code editor. Make
sure to have the [Python extension][vscode-py] installed and if you want to edit Jupyter
notebooks in VS code use the [Jupyter extension][vscode-jupy]. Configuring VS code is as
simple as adding the following lines to the `settings.json` file of the workspace:

```json
    // path to python executable in virtual environment where
    // mrlattice is installed. skip if installed in global.
    "python.pythonPath": "path/to/venv/bin/python",

    // activate the linters and black as formatter
    "python.linting.pydocstyleEnabled": true,
    "python.linting.pylintEnabled": true,
    "python.linting.pylintUseMinimalCheckers": false,
    "python.linting.flake8Enabled": true,
    "python.formatting.provider": "black",
    // because black does not support "format selection"
    "editor.formatOnPaste": false,
    "editor.formatOnSaveMode": "file",
    // keep default settings empty in favor of setup.cfg config
    "python.linting.pylintArgs": [],
    "python.linting.flake8Args": [],
    "python.linting.pydocstyleArgs": [],

    // for the unittests you can enable included testing framework
    "python.testing.unittestEnabled": true,
```


### PyCharm

[PyCharm][pycharm] is a very popular Python IDE. Setting up the linters and black
requires a few steps via the `Preferences -> Tools -> External Tools` menu. They can then
be used via `Tools -> External Tools -> [the tool you want to use]` and print their
output to the console view.


#### **black**

To set up black with PyCharm follow the steps from the [black documentation][pycharm-black].


#### **flake8**

Click the + icon to add a new tool with the following values:  
- Name: flake8
- Program: \$PyInterpreterDirectory\$/flake8
- Arguments: \$FilePath\$


#### **pylint**

Click the + icon to add a new tool with the following values:  
- Name: pylint
- Program: \$PyInterpreterDirectory\$/pylint
- Arguments: \$FilePath\$


#### **pydocstyle**

Click the + icon to add a new tool with the following values:  
- Name: pydocstyle
- Program: \$PyInterpreterDirectory\$/pydocstyle
- Arguments: \$FilePath\$


## Git hooks

In order to help with git version control two git hooks are recommended.


### pre-commit

A [pre-commit][pre-commit] configuration `.pre-commit-config.yaml` is included in the
repository. In essence, this will run all linters on the staged files when trying to commit
and prevent a commit if the linters detect a problem. pre-commit will also be installed
when using the `dev` target.

To initialize pre-commit run:
```
$ pre-commit install --install-hooks
```
within the environment where mrlattice was installed. This will set up all hooks. If
you update mrlattice it is good practice to run the above command again. This will update
the pre-commit hooks if the versions for the linters were updated in the configuration file.


### nbdime

[nbdime][nbdime] is a git hook that allows you to easily work with `*ipynb` Jupyter notebook
files. It provides easy to read diffs and merge conflict handling. It is installed with
the `dev` target as well.

To use nbdime register its git drivers using:
```
$ nbdime config-git --enable
```
In addition to integration with standard `git diff` commands, it is also possible to
display a nice diff in the browser.


# Testing code

Testing code includes linter checks according to the [styleguide](#codestyle) as well as
unit tests. The linters will be installed as dependencies with the `dev` target.
The `test` target installs the [coverage][coverage] tool which executes the unit tests and
measures test coverage.
The test suite is located in `test/` under the project root directory and will not be
shipped with the default `pip install`.
Instead, use the editable installation explained above to get all files included in the
repository.


### Unit tests and coverage

The unit test suite makes use of the Python built-in [unittest framework][unittest].
Usage is simple. From the root directory of mrlattice run:
```
$ python -m unittest discover -v
```
This will trigger automatic test discovery end execute all tests.

Keep in mind, that this command will only work if mrlattice is installed via pip.
Otherwise the command will fail with import errors because the modules of mrlattice are
not importable from the package root directory. You can still run [unittest][unittest] if
you add the location of the `src/` directory to `PYTHONPATH` as shown below:
```
$ PYTHONPATH=src/ python -m unittest discover -v
```

coverage will run all unit tests and measure the test coverage. A suitable configuration
is included in the `setup.cfg` file, where the Python [unittest framework][unittest] will
be used to run the tests.

To use coverage run:
```
$ coverage run
$ coverage combine
$ coverage html
```
This will run all unit tests and generate a html test report located in `htmlcov/index.html`
in the project root which can be viewed in any browser.

Again, coverage can be used without "pip-installing" mrlattice. The same procedure as
explained for the [unittest][unittest] module above applies here.
If mrlattice is installed via pip, make sure that the editable option was set.
Otherwise, the library code copied to the virtual environment `lib` folder will be executed.
But coverage will expect execution of the files located in the mrlattice project directory.
This will result in 0% code coverage.


## Continuous integration

GitLab CI/CD is configured for automated testing. The tests include the previously
discussed linter checks as well as unit testing and coverage measurements. There is a
small difference with pylint though. On GitLab CI/CD pylint tests will only cover Python
files inside packages (folders containing `__init__.py` files). Note that the GitLab CI/CD
runner only has access to a single thread equivalent CPU. It is known that pylint behaves
differently in single threaded execution vs. multithreaded execution. The configuration
files in this project allow pylint to detect and use all available threads to speed up
linting during development. However, this can lead to conflicting results compared to
GitLab CI/CD testing. In that case running pylint with the `--jobs` flag set to 1 with
the exact same command line as used by GitLab CI/CD allows to reproduce those execution
conditions:
```
$ pylint --jobs=1 $(find . -type d -path './.venv' -prune -false -o -type f -name '__init__.py' -printf '%h\n' | sort -u)
```


## Unit tests

Under `test/` there are test setups for each module
- `test/test_lattice_configure.py`
- `test/test_lattice_generate.py`
- `test/test_lattice_validate.py`
- `test/test_plotting.py`

and a setup for testing the package interface and access to modules
- `test/test_package.py`

The `test_plotting.py` module also tests the custom IPython widgets used in the notebooks.
For this purpose a test is configured to exectue the IPython kernel on a special testing
notebook `test/testing.ipynb` in a subprocess.


<!-- External links -->
[gitlab-fork]: https://gitlab.com/openpixi/mrlattice/-/forks/new
[gitlab-issues]: https://gitlab.com/openpixi/mrlattice/-/issues
[gitlab-mr]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
[pip-vcs]: https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support
[pip-editable]: https://packaging.python.org/guides/distributing-packages-using-setuptools/#working-in-development-mode
[google-docstyle]: https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings
[black]: https://black.readthedocs.io/en/stable/
[flake8]: https://flake8.pycqa.org/en/latest/
[pylint]: http://pylint.pycqa.org/en/latest/
[pydocstyle]: http://www.pydocstyle.org/en/stable/
[vscode]: https://code.visualstudio.com/
[vscode-py]: https://marketplace.visualstudio.com/items?itemName=ms-python.python
[vscode-jupy]: https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter
[pycharm]: https://www.jetbrains.com/pycharm/
[pycharm-black]: https://black.readthedocs.io/en/stable/editor_integration.html#pycharm-intellij-idea
[pre-commit]: https://pre-commit.com/
[nbdime]: https://nbdime.readthedocs.io/en/latest/
[coverage]: https://coverage.readthedocs.io/en/latest/
[unittest]: https://docs.python.org/3/library/unittest.html
