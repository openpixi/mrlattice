"""Allow `pip install --editable mrlattice`.

Development mode is currently incompatible with PEP 517, thus this file
with this content is needed.

For the package config see `setup.cfg`.
"""

import setuptools

setuptools.setup()
