"""Test setup to verify functionality of the `lattice.validate` module.

Test Cases are:
- TestValidateLatticeClass
"""

import functools
import re
import unittest
from inspect import Parameter, isfunction, ismethod, signature
from io import StringIO
from time import time
from types import MethodType
from unittest.mock import MagicMock, Mock, call

from mrlattice.lattice.generate import (
    LatticeArray,
    HomogeneousLatticeArray,
)
from mrlattice.lattice.exceptions import ValidationError, SkipValidation
from mrlattice.lattice.validate import ValidateLattice


class TestValidateLatticeClass(unittest.TestCase):
    """Test ValidateLattice core functionality."""

    # pylint: disable=protected-access,unused-argument

    def setUp(self):
        # proper LatticeArray instance and test suite
        self.A = LatticeArray()
        self.A.scheme.set_fixed_width()
        self.A.generate_array()
        self.V = ValidateLattice(self.A)

        # mock test suite
        def count_calls(*args, test_instance=None, **kwargs):
            """Count calls in `test_instance.call_count`."""
            test_instance.call_count += 1

        self.Mock_A = MagicMock()
        self.Mock_V = ValidateLattice(
            self.Mock_A,
            sorted_props=[
                "buffer",
                "coarse_steps",
                "repeat",
            ],
            # prime numbers, so products are unique
            increments=[7, 5, 3],
            count_calls=count_calls,
        )
        self.Mock_V.call_count = 0
        # fake lattice array
        self.Mock_A.array = [0] * 19
        self.Mock_A.PROPERTY_DESCRIPTIONS = dict(
            HomogeneousLatticeArray.PROPERTY_DESCRIPTIONS
        )
        # correct numbers for different execution levels
        # product of all increments
        self.Mock_V.correct_loop_count = functools.reduce(
            lambda x, y: x * y, self.Mock_V.increments
        )
        # num of times the test funcs should be called
        self.Mock_V.correct_call_count = self.Mock_V.correct_loop_count * len(
            self.Mock_A.array
        )
        # increment is called per nested loop
        self.Mock_V.correct_increment_calls_count = 0
        for incr, _ in enumerate(self.Mock_V.increments):
            self.Mock_V.correct_increment_calls_count += functools.reduce(
                lambda x, y: x * y, self.Mock_V.increments[0 : incr + 1]
            )

        # fake increment functions that saves call time stamp
        def fake_increment(self):
            self.calls.append(time())

        for prop in self.Mock_V.sorted_props:
            setattr(self.Mock_A, prop, MagicMock())
            setattr(getattr(self.Mock_A, prop), "calls", [])
            setattr(
                getattr(self.Mock_A, prop),
                "increment",
                MethodType(fake_increment, getattr(self.Mock_A, prop)),
            )

    def test_init(self):
        """Test initialization."""

        with self.subTest(msg="verify array attribute"):
            V = ValidateLattice(self.A)
            self.assertIs(V.lattice, self.A)

        with self.subTest(what="verify has default suite"):
            V = ValidateLattice(self.A)
            self.assertEqual(V.suite, ValidateLattice._TEST_CASES)
            for test in V.suite:
                with self.subTest(test=test):
                    self.assertFalse(getattr(V, test))

        with self.subTest(what="verify has default test methods"):
            V = ValidateLattice(self.A)
            for test in V.suite:
                with self.subTest(test=test):
                    self.assertTrue(hasattr(V, test + "_func"))

        with self.subTest(what="init with valid preset"):
            for name in ValidateLattice._TEST_CASES:
                with self.subTest(name=name):
                    V = ValidateLattice(self.A, preset=set([name]))
                    self.assertTrue(getattr(V, name))

        with self.subTest(msg="init with invalid preset"):
            with self.assertRaises(KeyError):
                V = ValidateLattice(self.A, preset=set(["nonexist"]))

        with self.subTest(msg="valid test functions as kwargs"):

            def test_callable():
                pass

            V = ValidateLattice(self.A, new=test_callable)
            self.assertTrue(V.new)
            self.assertIs(V.new_func, test_callable)
            self.assertIn("new", V.suite)

        with self.subTest(msg="invalid test function as kwargs"):
            with self.assertRaises(TypeError):
                V = ValidateLattice(self.A, new=[])

    def test_add_test(self):
        """Test if using `add_test` works."""

        self.V.add_test("new", lambda x: None)
        self.assertTrue(hasattr(self.V, "new"))
        self.assertTrue(hasattr(self.V, "new_func"))
        self.assertIn("new", self.V.suite)

        with self.subTest(msg="test name with whitespace"):
            self.V.add_test("new test", lambda x: None)
            self.assertTrue(hasattr(self.V, "new_test"))
            self.assertTrue(hasattr(self.V, "new_test_func"))
            self.assertIn("new_test", self.V.suite)

    def test_invalid_member(self):
        """Test error for invalid attributes."""

        # test if something is broken when overwriting __getattr__
        with self.assertRaises(AttributeError, msg="__getattr__() malfunction"):
            _ = self.V.missing

    def test_test_time_slices(self):
        """Test if `test_time_slices` performs correctly."""

        # pylint: disable=unused-argument
        # define some tests
        validate1_pass = Mock(__doc__="This test will pass.")
        validate2_fail = Mock(
            __doc__="This test will fail.",
            side_effect=ValidationError(
                "Oops, something went wrong.", "validate2_fail"
            ),
        )
        validate3_skip = Mock(
            __doc__="This test will skip.",
            side_effect=SkipValidation(
                reason="for unit testing", test="validate3_skip"
            ),
        )

        # save test log
        test_log = StringIO()

        # reset suite
        self.V.suite = set()
        self.V.add_test("v1_pass", validate1_pass)
        self.V.add_test("v2_pass", validate1_pass)
        self.V.add_test("v3_pass", validate1_pass)
        self.V.add_test("v4_skip", validate1_pass)
        self.V.v4_skip = False
        # run tests
        result = self.V.test_time_slices(0, [0, 0], stdout=test_log, stderr=test_log)

        # check call args for test function
        validate1_pass.assert_has_calls(
            [call(self.A, 0, 0, 0), call(self.A, 0, 0, 0), call(self.A, 0, 0, 0)]
        )

        self.pass_result_test(result, test_log)

        # reset test_log
        test_log.truncate(0)
        test_log.seek(0)

        # add skipping tests
        self.V.add_test("v1_skip", validate3_skip)
        self.V.add_test("v2_skip", validate3_skip)
        # run tests
        result = self.V.test_time_slices(0, [0, 0], stdout=test_log, stderr=test_log)

        self.skip_result_test(result, test_log)

        # reset test_log
        test_log.truncate(0)
        test_log.seek(0)

        # add failing tests
        self.V.add_test("v1_fail", validate2_fail)
        self.V.add_test("v3_fail", validate2_fail)
        # run tests
        result = self.V.test_time_slices(0, [0, 0], stdout=test_log, stderr=test_log)

        self.fail_result_test(result, test_log)

    def pass_result_test(self, result, test_log):
        """Helper method for `test_test_time_slices`."""

        self.assertTrue(result, msg="callable return value")

        split_out = test_log.getvalue().split("\n\n" + "-" * 70 + "\n")
        self.assertEqual(len(split_out), 2, msg="output has more than 2 sections")
        tests, summary = split_out

        with self.subTest(output="tests"):
            tests = re.split("PASSED.", tests)
            # equal to num tests + 1, as PASSED. terminates with \n
            self.assertEqual(len(tests), 4, msg="number of passed tests incorrect")
            for name, t in zip(("v1_pass", "v2_pass", "v3_pass"), tests):
                # this also checks for the correct alphabetic order
                with self.subTest(name=name):
                    self.assertIn(name, t)
                    # docstring
                    self.assertIn("This test will pass.", t)

        with self.subTest(output="summary"):
            self.assertIn("Ran 3 tests:", summary)
            self.assertIn("\n\t3 passed", summary)
            self.assertIn("PASSED!", summary)

    def skip_result_test(self, result, test_log):
        """Helper method for `test_test_time_slices`."""

        self.assertTrue(result, msg="callable return value")

        split_out = test_log.getvalue().split("\n\n" + "-" * 70 + "\n")
        self.assertEqual(len(split_out), 2, msg="output has more than 2 sections")
        tests, summary = split_out

        with self.subTest(output="tests"):
            tests, *tbs = tests.split("\n\n" + "=" * 70 + "\n")
            tests = re.split("PASSED.|SKIPPED!", tests)
            # equal to num tests + 1, as last elem \n
            self.assertEqual(len(tests), 6, msg="number of passed tests incorrect")
            for name, t in zip(
                ("v1_pass", "v1_skip", "v2_pass", "v2_skip", "v3_pass"), tests
            ):
                # this also checks for the correct alphabetic order
                with self.subTest(name=name):
                    self.assertIn(name, t)
                    self.assertIn(f"This test will {re.sub('v[0-9]_', '', name)}.", t)

            # the tracebacks
            self.assertEqual(len(tbs), 2)
            for name, tb in zip(("v1_skip", "v2_skip"), tbs):
                with self.subTest(traceback=tb):
                    self.assertIn(f"SKIP: {name}", tb)
                    # docstring
                    self.assertIn("This test will skip.", tb)
                    # the exception
                    self.assertIn("SkipValidation", tb)
                    # the lattice array
                    self.assertIn(str(self.A), tb)

        with self.subTest(output="summary"):
            self.assertIn("Ran 5 tests:", summary)
            self.assertIn("\n\t3 passed\n\t2 skipped", summary)
            self.assertIn("PASSED!", summary)

    def fail_result_test(self, result, test_log):
        """Helper method for `test_test_time_slices`."""

        self.assertFalse(result, msg="callable return value")

        split_out = test_log.getvalue().split("\n\n" + "-" * 70 + "\n")
        self.assertEqual(len(split_out), 2, msg="output has more than 2 sections")
        tests, summary = split_out

        with self.subTest(output="tests"):
            tests, *tbs = tests.split("\n\n" + "=" * 70 + "\n")
            tests = re.split("PASSED.|SKIPPED!|FAILED!", tests)
            # equal to num tests + 1, as last elem is \n
            self.assertEqual(len(tests), 8)
            for name, t in zip(
                (
                    "v1_fail",
                    "v1_pass",
                    "v1_skip",
                    "v2_pass",
                    "v2_skip",
                    "v3_fail",
                    "v3_pass",
                ),
                tests,
            ):
                # this also checks for the correct alphabetic order
                with self.subTest(name=name):
                    self.assertIn(name, t)
                    self.assertIn(f"This test will {re.sub('v[0-9]_', '', name)}.", t)

            # the tracebacks, sorted first skipped, then failed
            self.assertEqual(len(tbs), 4)
            for name, tb in zip(("v1_skip", "v2_skip", "v1_fail", "v3_fail"), tbs):
                with self.subTest(traceback=tb):
                    if "skip" in name:
                        self.assertIn(f"SKIP: {name}", tb)
                        # docstring
                        self.assertIn("This test will skip.", tb)
                        # the exception
                        self.assertIn("SkipValidation", tb)
                        # the lattice array
                        self.assertIn(str(self.A), tb)
                    elif "fail" in name:
                        self.assertIn(f"FAIL: {name}", tb)
                        # docstring
                        self.assertIn("This test will fail.", tb)
                        # the exception
                        self.assertIn("ValidationError", tb)
                        # the lattice array
                        self.assertIn(str(self.A), tb)

        with self.subTest(output="summary"):
            self.assertIn("Ran 7 tests:", summary)
            self.assertIn("\n\t3 passed\n\t2 skipped\n\t2 failed", summary)
            self.assertIn("FAILED!", summary)

    def test_call_counts(self):
        """Test counts for calls of functions in property loops."""
        # due to use of mock LatticeArray, only explicit calls are
        # counted. no implicit calls via setters.

        # execute suite with custom kwarg for special test function
        self.Mock_V(test_instance=self.Mock_V)

        self.Mock_A.generate_array.assert_has_calls(
            [call()] * self.Mock_V.correct_loop_count
        )

        # innermost prop doesn't update any others
        self.Mock_A.update_properties.assert_has_calls(
            [call()] * (self.Mock_V.correct_loop_count // self.Mock_V.increments[-1])
        )

        # call count for test function
        self.assertEqual(self.Mock_V.correct_call_count, self.Mock_V.call_count)
        # access to private var, resolve name mangling manually
        self.assertEqual(
            len(self.Mock_V._ValidateLattice__suite_passed),
            self.Mock_V.correct_call_count,
        )

        # calls to increment() per property
        calls_increment = 0
        for idx, prop in enumerate(self.Mock_V.sorted_props):
            with self.subTest(msg="count increment() calls", property=prop):
                calls = len(getattr(self.Mock_A, prop).calls)
                self.assertEqual(
                    functools.reduce(
                        lambda x, y: x * y, self.Mock_V.increments[0 : idx + 1]
                    ),
                    calls,
                    msg=f"increment() calls of {prop} incorrect",
                )
                calls_increment += calls

        self.assertEqual(
            calls_increment,
            self.Mock_V.correct_increment_calls_count,
            msg="total increment() calls incorrect",
        )

        # calls to _check_bounds_and_update() per property
        # extra for first element
        getattr(
            self.Mock_A, self.Mock_V.sorted_props[0]
        )._check_bounds_and_update.assert_not_called()

        calls_update = 0
        for idx, prop in enumerate(self.Mock_V.sorted_props[1:]):
            with self.subTest(
                msg="count _check_bounds_and_update() calls", property=prop
            ):
                calls = functools.reduce(
                    lambda x, y: x * y, self.Mock_V.increments[0 : idx + 1]
                )
                getattr(self.Mock_A, prop)._check_bounds_and_update.assert_has_calls(
                    [call()] * calls
                )
            calls_update += calls

        # due to shift in increments association with count
        correct_update_calls_count = (
            self.Mock_V.correct_increment_calls_count - self.Mock_V.correct_loop_count
        )
        self.assertEqual(
            calls_update,
            correct_update_calls_count,
            msg="property update calls",
        )

    def test_property_loop_nesting(self):
        """Test if property loop is nested correctly."""

        # execute suite with custom kwarg for special test function
        self.Mock_V(test_instance=self.Mock_V)

        # create new list that sorts timestaps of calls to increment
        # from all props according to how it should be
        # explicit multiple loops possible, because Mock_A is hardcoded
        # with: sorted_props= [ "buffer", "coarse_steps", "repeat" ]
        bu_incr = self.Mock_V.increments[0]
        co_incr = self.Mock_V.increments[1]
        re_incr = self.Mock_V.increments[2]
        ordered_calls = []
        for bu in range(bu_incr):
            for co in range(co_incr):
                idx = bu * co_incr * re_incr + co * re_incr
                ordered_calls.extend(self.Mock_A.repeat.calls[idx : idx + re_incr])
                ordered_calls.append(self.Mock_A.coarse_steps.calls[bu * co_incr + co])
            ordered_calls.append(self.Mock_A.buffer.calls[bu])

        # assert if timestamps list has correct len
        self.assertEqual(
            len(ordered_calls),
            self.Mock_V.correct_increment_calls_count,
            msg="number of calls to increment() incorrect",
        )
        # assert if timestaps are ordered correctly
        self.assertTrue(
            all(i < j for i, j in zip(ordered_calls, ordered_calls[1:])),
            msg="loop nesting incorrect",
        )

    def test_sorted_props_increments_mismatch(self):
        """Test mismatched len of `sorted_props` and `increments`."""

        increments = list(self.Mock_V.increments)

        # adding more increments than there are sorted_props
        self.Mock_V.increments = increments + [13]
        # call instance with custom kwarg for special test function
        self.Mock_V(test_instance=self.Mock_V)
        self.assertEqual(self.Mock_V.correct_call_count, self.Mock_V.call_count)

        with self.assertRaises(
            IndexError, msg="increments shorter than sorted_props passes"
        ):
            self.Mock_V.increments = increments[:-1]
            self.Mock_V(test_instance=self.Mock_V)

    def test_empty_sorted_props(self):
        """Test if empty sorted props triggers one pass."""
        sorted_props_backup = self.Mock_V.sorted_props
        # empty sorted props, as would be if not supplied to constructor
        tmp_V = ValidateLattice(self.Mock_A)
        self.Mock_V.sorted_props = tmp_V.sorted_props
        self.Mock_V.increments = tmp_V.increments
        # call instance with custom kwarg for special test function
        self.Mock_V(test_instance=self.Mock_V)
        # correct call count of test function is len of lattice
        self.assertEqual(len(self.Mock_A.array), self.Mock_V.call_count)
        for prop in sorted_props_backup:
            with self.subTest(
                msg="increment() calls for empty sorted props", property=prop
            ):
                # increment() mustn't be called even once
                self.assertEqual(0, len(getattr(self.Mock_A, prop).calls))

    def test_suite_array_modification(self):
        """Test if lattice array is restored after running the suite."""

        # wrap unit tests in function
        def run_multiple_passes(backup_lattice):
            # repeat to make sure defaults are reset
            for _ in range(3):
                self.V()
                # test if starting array is restored after suite
                for prop in self.A.PROPERTY_DESCRIPTIONS:
                    with self.subTest(
                        msg="restore lattice after suite completed", prop=prop
                    ):
                        self.assertEqual(
                            getattr(backup_lattice, prop).value,
                            getattr(self.A, prop).value,
                            msg=f"{prop} not restored",
                        )

        # prepare test suite
        dummy_test = Mock(__doc__="A dummy validation test.")
        self.V.add_test("dummy", dummy_test)
        # lattice prop values
        PROPS = {
            "coarse_steps": 2,
            "borders_width": 12,
            "buffer": 2,
            "finest_width": 7,
            "repeat": 2,
        }

        # also test stock, with no sorted_props i.e. the lattice as is
        # fixed width scheme
        backup_lattice = LatticeArray(scheme="fixed_width", **PROPS)
        self.A.scheme.set_fixed_width()
        self.A.set_properties(PROPS)

        run_multiple_passes(backup_lattice)

        # fixed number scheme
        backup_lattice = LatticeArray(scheme="fixed_number", **PROPS)
        self.A.scheme.set_fixed_number()
        self.A.set_properties(PROPS)

        run_multiple_passes(backup_lattice)

        # prepare starting lattice with sorted props
        self.V.sorted_props = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]
        self.V.increments = [2] * len(self.V.sorted_props)

        # fixed width scheme
        backup_lattice = LatticeArray(scheme="fixed_width", **PROPS)
        self.A.scheme.set_fixed_width()
        self.A.set_properties(PROPS)

        run_multiple_passes(backup_lattice)

        # fixed number scheme
        backup_lattice = LatticeArray(scheme="fixed_number", **PROPS)
        self.A.scheme.set_fixed_number()
        self.A.set_properties(PROPS)

        run_multiple_passes(backup_lattice)

    def test_args_propagation(self):
        """Test if *args and **kwargs propagate to test functions."""

        # prepare suite
        dummy_test = Mock(__doc__="A dummy validation test.")
        self.V.add_test("dummy", dummy_test)
        self.V.sorted_props = list(self.Mock_V.sorted_props)
        self.V.increments = list(self.Mock_V.increments)
        self.V._lattice = self.Mock_A
        # execute suite
        self.V("1.pos", "2.pos", key1="1.key", key2="2.key")

        # build expected calls list
        calls_list = [
            call(
                self.V.lattice, t, ts, ts, "1.pos", "2.pos", key1="1.key", key2="2.key"
            )
            for t, ts in enumerate(self.Mock_A.array)
        ] * self.Mock_V.correct_loop_count

        dummy_test.assert_has_calls(calls_list)

    def test_suite_output(self):
        """Test stdout and stderr output from suite execution."""

        # set up suite
        validate1_pass = Mock(__doc__="This test will pass.")
        validate2_fail = Mock(
            __doc__="This test will fail.",
            side_effect=ValidationError(
                "Oops, something went wrong.", "validate2_fail"
            ),
        )
        validate3_skip = Mock(
            __doc__="This test will skip.",
            side_effect=SkipValidation(
                reason="for unit testing", test="validate3_skip"
            ),
        )
        self.Mock_V.add_test("v1_pass", validate1_pass)
        self.Mock_V.count_calls = False
        stdoutput, erroutput = StringIO(), StringIO()

        def reset_output():
            for var in (stdoutput, erroutput):
                var.truncate(0)
                var.seek(0)

        with self.subTest(config="passing"):
            result = self.Mock_V(stdout=stdoutput, stderr=erroutput)
            self.assertTrue(result, msg="pass doesn't return true")
            self.assertFalse(erroutput.getvalue(), msg="stderr not empty")

            summary = stdoutput.getvalue().rsplit("=" * 70, maxsplit=1)[-1]
            self.assertIn(
                f"{self.Mock_V.correct_call_count} calls passed!",
                summary,
                msg="count of passed runs",
            )
            self.assertIn("0 calls failed!", summary, msg="count of failed runs")

        reset_output()

        # Mock with exceptions takes very long, use fewer time slices
        self.Mock_A.array = self.Mock_A.array[:4]

        # add more tests, one that skips
        self.Mock_V.add_test("v4_skip", validate3_skip)

        with self.subTest(config="skipping"):
            result = self.Mock_V(stdout=stdoutput, stderr=erroutput)
            self.assertTrue(result, msg="skipping doesn't return true")
            # separate each traceback in erroutput
            tbs = erroutput.getvalue().split("\n\n\n\n" + "=" * 70 + "\n")
            self.assertEqual(
                len(tbs),
                self.Mock_V.correct_call_count / 19 * 4,
                msg="count of printed tracebacks",
            )
            # check traceback content
            for tb in tbs:
                with self.subTest():
                    self.assertIn("SKIP: v4_skip", tb)
                    # docstring
                    self.assertIn("This test will skip.", tb)
                    # the exception
                    self.assertIn("SkipValidation", tb)
                    # the lattice array
                    self.assertIn(str(self.Mock_A), tb)
            # check summary content
            summary = stdoutput.getvalue().rsplit("=" * 70, maxsplit=1)[-1]
            self.assertIn(
                f"{self.Mock_V.correct_call_count/19*4:.0f} calls passed!",
                summary,
                msg="count of passed runs",
            )
            self.assertIn(
                "0 calls failed!",
                summary,
                msg="count of failed runs",
            )

        reset_output()

        # add more tests, one that fails
        self.Mock_V.add_test("v2_pass", validate1_pass)
        self.Mock_V.add_test("v3_fail", validate2_fail)
        # disable skip
        self.Mock_V.v4_skip = False

        with self.subTest(config="failing"):
            result = self.Mock_V(stdout=stdoutput, stderr=erroutput)
            self.assertFalse(result, msg="failure doesn't return false")
            # separate each traceback in erroutput
            tbs = erroutput.getvalue().split("\n\n\n\n" + "=" * 70 + "\n")
            self.assertEqual(
                len(tbs),
                self.Mock_V.correct_call_count / 19 * 4,
                msg="count of printed tracebacks",
            )
            # check traceback content
            for tb in tbs:
                with self.subTest():
                    self.assertIn("FAIL: v3_fail", tb)
                    # docstring
                    self.assertIn("This test will fail.", tb)
                    # the exception
                    self.assertIn("ValidationError", tb)
                    # the lattice array
                    self.assertIn(str(self.Mock_A), tb)
            # check summary content
            summary = stdoutput.getvalue().rsplit("=" * 70, maxsplit=1)[-1]
            self.assertIn(
                f"{self.Mock_V.correct_call_count/19*4:.0f} calls failed!",
                summary,
                msg="count of failed runs",
            )
            self.assertIn(
                "0 calls passed!",
                summary,
                msg="count of passed runs",
            )


class TestValidateLatticeTestFunctionSigs(unittest.TestCase):
    """Test general properties of validation functions."""

    # pylint: disable=protected-access

    def setUp(self):
        self.A = MagicMock()
        self.V = ValidateLattice(lattice=self.A)
        parA = Parameter("lattice", Parameter.POSITIONAL_OR_KEYWORD)
        t = Parameter("t", Parameter.POSITIONAL_OR_KEYWORD)
        self.ts = Parameter("ts", Parameter.POSITIONAL_OR_KEYWORD)
        self.ts1 = Parameter("ts1", Parameter.POSITIONAL_OR_KEYWORD)
        self.ts2 = Parameter("ts2", Parameter.POSITIONAL_OR_KEYWORD)
        args = Parameter("_", Parameter.VAR_POSITIONAL)
        kwargs = Parameter("__", Parameter.VAR_KEYWORD)
        self.sig = [parA, t, args, kwargs]

    def test_correct_signature(self):
        """Test if validation functions have correct signatures."""

        # runs over all funcs in suite
        for case in self.V._TEST_CASES:
            with self.subTest(test_case=case):
                func = getattr(self.V, case + "_func")
                # isfunction for staticmethods
                # ismethod with check if __self__ is class for clsmethod
                if not isfunction(func):
                    if ismethod(func):
                        if func.__self__ is not ValidateLattice:
                            self.fail(
                                msg=f"{func} is a bound method"
                                "not a static function or classmethod"
                            )
                    else:
                        self.fail(msg=f"{func} is not a static function or classmethod")
                sig = signature(func).parameters
                for ref in self.sig:
                    # first check member
                    try:
                        self.assertIn(ref.name, sig)
                    except AssertionError:
                        # if arg is unused, allow for '_' in front
                        ref = ref.replace(name=f"_{ref.name}")
                        self.assertIn(ref.name, sig)
                    # check kind
                    self.assertEqual(sig[ref.name].kind, ref.kind)
                # extra for variable number of ts
                try:
                    self.assertIn(self.ts.name, sig)
                    self.assertEqual(sig[self.ts.name].kind, self.ts.kind)
                except AssertionError:
                    self.assertIn(self.ts1.name, sig)
                    self.assertEqual(sig[self.ts1.name].kind, self.ts1.kind)
                    self.assertIn(self.ts2.name, sig)
                    self.assertEqual(sig[self.ts2.name].kind, self.ts2.kind)


if __name__ == "__main__":
    unittest.main()
