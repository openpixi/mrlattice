"""Test setup to verify functionality of the `lattice.generate` module.

Test cases are:
- TestLatticeArrayBasics
- TestLatticeArraySpecials
- TestValidLatticeArrayGeneration
"""

import os
import unittest

import numpy as np
from mrlattice.lattice.configure import LatticeArrayScheme
from mrlattice.lattice.generate import (
    HomogeneousLatticeArray,
    LatticeArray,
    PulseLatticeArray,
    RegionTuple,
    SuperimposeLatticeArray,
    SuperimposePulseLatticeArray,
)
from mrlattice.lattice.exceptions import (
    ConfigurationError,
    LatticeGenerationError,
    ModifierHistoryError,
    VolatilePropertyError,
)
from mrlattice.lattice.validate import ValidateLattice


class TestLatticeArrayBasics(unittest.TestCase):
    """Test `LatticeArray` core functionality."""

    # pylint: disable=protected-access

    def setUp(self):
        # tests where new instances are created delete self.A
        self.A = LatticeArray(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME)

    def test_invalid_member(self):
        """Test error for invalid attributes."""

        # test if something is broken when overwriting __getattr__
        with self.assertRaises(AttributeError, msg="__getattr__() malfunction"):
            _ = self.A.missing

    def test_volatile_property_access(self):
        """Test `LatticeArray` volatile property getter."""

        # after init there is no array
        with self.subTest(msg="before array generation"):
            self.assertFalse(hasattr(self.A, "_array"))
            self.assertFalse(hasattr(self.A, "_max_num_cells"))
            self.assertFalse(hasattr(self.A, "_res_list"))
            self.assertFalse(hasattr(self.A, "_tx_dims"))
            with self.assertRaises(VolatilePropertyError, msg="unset prop returned"):
                _ = self.A.array
                _ = self.A.max_num_cells
                _ = self.A.resolution_list
                _ = self.A.tx_dims
            # same after update_properties
            self.A.update_properties()
            self.assertFalse(hasattr(self.A, "_array"))
            self.assertFalse(hasattr(self.A, "_max_num_cells"))
            self.assertFalse(hasattr(self.A, "_res_list"))
            self.assertFalse(hasattr(self.A, "_tx_dims"))
            with self.assertRaises(VolatilePropertyError, msg="unset prop returned"):
                _ = self.A.array
                _ = self.A.max_num_cells
                _ = self.A.resolution_list
                _ = self.A.tx_dims

        with self.subTest(msg="after array generation"):
            self.A.generate_array()
            self.assertTrue(hasattr(self.A, "_array"))
            self.assertTrue(hasattr(self.A, "_max_num_cells"))
            self.assertTrue(hasattr(self.A, "_res_list"))
            self.assertTrue(hasattr(self.A, "_tx_dims"))
            _ = self.A.array
            _ = self.A.max_num_cells
            _ = self.A.resolution_list
            _ = self.A.tx_dims
            # after update_properties array should be gone
            self.A.update_properties()
            self.assertFalse(hasattr(self.A, "_array"))
            self.assertFalse(hasattr(self.A, "_max_num_cells"))
            self.assertFalse(hasattr(self.A, "_res_list"))
            self.assertFalse(hasattr(self.A, "_tx_dims"))
            with self.assertRaises(VolatilePropertyError, msg="unset prop returned"):
                _ = self.A.array
                _ = self.A.max_num_cells
                _ = self.A.resolution_list
                _ = self.A.tx_dims

    def test_invalid_scheme_for_generate(self):
        """Test error using `generate_array` with invalid scheme."""

        # force non existing scheme
        self.A.scheme._scheme = "how is that?"
        with self.assertRaises(ConfigurationError, msg="unknown schemes not caught"):
            self.A.generate_array()

    def test_invalid_scheme_for_update(self):
        """Test error using `update_properties` with invalid scheme."""

        # force non existing scheme
        self.A.scheme._scheme = "how is that?"
        with self.assertRaises(ConfigurationError, msg="unknown schemes not caught"):
            self.A.update_properties()

    def test_set_properties(self):
        """Test `set_properties` with dict argument."""

        # full dict
        prop_dict = {
            "finest_width": 7,
            "borders_width": 12,
            "coarse_steps": 2,
            "buffer": 7,
            "repeat": 3,
        }
        with self.subTest(msg="dict has all keys"):
            self.A.set_properties(prop_dict)
            for key, val in prop_dict.items():
                self.assertEqual(val, getattr(self.A, key).value)
            # invalid
            prop_dict["no_key"] = None
            with self.assertRaises(
                ConfigurationError, msg="set_properties allows invalid properties"
            ):
                self.A.set_properties(prop_dict)

        # partial dict
        prop_dict = {
            "coarse_steps": 2,
            "buffer": 7,
            "repeat": 3,
        }
        with self.subTest(msg="dict has some keys"):
            self.A.set_properties(prop_dict)
            for key, val in prop_dict.items():
                self.assertEqual(val, getattr(self.A, key).value)
            # invalid
            prop_dict["no_key"] = None
            with self.assertRaises(
                ConfigurationError, msg="set_properties allows invalid properties"
            ):
                self.A.set_properties(prop_dict)

        # dict valid, but values not
        prop_dict = {
            "borders_width": 6,  # would need to be 2**4 *3
            "coarse_steps": 4,
        }
        with self.subTest(msg="dict with invalid values"):
            with self.assertRaises(
                ValueError, msg="set_properties allows invalid values"
            ):
                self.A.set_properties(prop_dict)

        # no scheme set caught
        prop_dict = {
            "coarse_steps": 2,
            "buffer": 7,
            "repeat": 3,
        }
        with self.subTest(msg="no active scheme"):
            A = LatticeArray()
            with self.assertRaises(
                ConfigurationError, msg="set_properies without active scheme possible"
            ):
                A.set_properties(prop_dict)
            A.scheme.set_fixed_number()
            A.set_properties(prop_dict)

        # Property setter still works and updates linked values
        prop_dict = {"coarse_steps": 16}
        self.A.set_properties(prop_dict)
        self.assertEqual(self.A.borders_width.value, 2**16 * 3)
        self.assertEqual(self.A.borders_width._min, 2**16 * 3)

    def test_init_with_args(self):
        """Test class `__init__` with args."""

        prop_dict = {
            "finest_width": 7,
            "borders_width": 14,
            "coarse_steps": 2,
            "buffer": 7,
            "repeat": 3,
        }

        with self.subTest(msg="standard init with full dict"):
            A = LatticeArray(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME, **prop_dict)
            for key, val in prop_dict.items():
                self.assertEqual(val, getattr(A, key).value)
            self.assertEqual(A.scheme.name, LatticeArrayScheme.FIXED_WIDTH_SCHEME)

        prop_dict = {
            "coarse_steps": 2,
            "buffer": 7,
            "repeat": 3,
        }
        with self.subTest(msg="init with partial dict"):
            A = LatticeArray(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME, **prop_dict)
            for key, val in prop_dict.items():
                self.assertEqual(val, getattr(A, key).value)
            self.assertEqual(A.scheme.name, LatticeArrayScheme.FIXED_WIDTH_SCHEME)

        with self.subTest(msg="init without scheme"):
            with self.assertRaises(
                ConfigurationError, msg="__init__ without scheme possible"
            ):
                A = LatticeArray(**prop_dict)

        with self.subTest(msg="test init with only setting scheme"):
            A = LatticeArray(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME)
            # fixed_width sets borders_width step == 2
            self.assertEqual(
                A.borders_width._step,
                2,
                msg="fixed_width scheme not updating prop values",
            )
            # fixed_number sets borders_width min == 6
            A = LatticeArray(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME)
            self.assertEqual(
                A.borders_width._min,
                6,
                msg="fixed_number scheme not updating prop values",
            )

        with self.subTest(msg="init with invalid scheme"):
            with self.assertRaises(
                ConfigurationError, msg="invalid scheme with other args not caught"
            ):
                A = LatticeArray(scheme="does_not_exist", **prop_dict)
            with self.assertRaises(
                ConfigurationError, msg="invalid scheme without other args not caught"
            ):
                A = LatticeArray(scheme="does_not_exist")

    def test_generate_array(self):
        """Test for correct logic in array generation."""
        # this will not test if the generated array is valid

        with self.subTest(scheme="fixed_width"):
            self.A.scheme.set_fixed_width()
            self.A.coarse_steps.value = 3
            out = self.A._generate_array_fixed_width()

            ts = max(map(len, out))
            self.assertTrue(all(ts == len(o) for o in out), msg="temporal size varies")
            for i in range(ts):
                ts = max((len(out[0][i]), len(out[1][i])))
                with self.subTest(time_step=i):
                    self.assertTrue(all(ts == len(o[i]) for o in out))

        with self.subTest(scheme="fixed_number"):
            self.A.scheme.set_fixed_number()
            self.A.coarse_steps.value = 3
            out = self.A._generate_array_fixed_number()

            ts = max(map(len, out))
            self.assertTrue(all(ts == len(o) for o in out), msg="temporal size varies")
            for i in range(ts):
                ts = max((len(out[0][i]), len(out[1][i])))
                with self.subTest(time_step=i):
                    self.assertTrue(all(ts == len(o[i]) for o in out))

        with self.subTest(msg="building numpy array"):
            regs_n = [[2, 3, 5], [5, 3, 2]]
            # mess up here
            regs_a = [[2, 1, 2]]
            ts = (len(regs_n), len(regs_a))

            with self.assertRaises(
                LatticeGenerationError, msg="time steps mismatch"
            ) as assert_context:
                self.A._generate_lattice_array(regs_n, regs_a)
            self.assertIn(str(ts), assert_context.exception.args[0])

            regs_a = [[2, 1], [2, 1, 2]]
            ts = (len(regs_n[0]), len(regs_a[0]))

            with self.assertRaises(
                LatticeGenerationError, msg="spatial steps mismatch"
            ) as assert_context:
                self.A._generate_lattice_array(regs_n, regs_a)
            self.assertIn(str(ts), assert_context.exception.args[0])

            out = self.A._generate_lattice_array([], [])
            self.assertEqual(out.size, 0)

    def test_volatile_property_generation(self):
        """Tests for volatile properties other than .array."""

        # prepare array
        self.A.scheme.set_fixed_number()
        self.A.set_properties(
            {
                "coarse_steps": 1,
                "borders_width": 6,
                "buffer": 2,
                "finest_width": 7,
                "repeat": 10,
            }
        )
        self.A.generate_array()

        self.assertEqual(
            self.A.max_num_cells, {1: 25, 2: 11}, msg="max_num_cells property wrong"
        )
        self.assertEqual(
            self.A.resolution_list, [1, 2], msg="resolution_list property wrong"
        )
        self.assertEqual(self.A.tx_dims, (20, (29,)), msg="tx_dims property wrong")

    def test_ts_resolution_list(self):
        """Test LatticeArray.ts_resolution_list."""
        self.A.generate_array()
        self.assertListEqual(self.A.ts_resolution_list(len(self.A.array) // 3), [1, 2])

    def test_spatial_mirror_array(self):
        """Test spatial mirror correct output and invalid inputs."""

        lat = np.empty(2, dtype=object)
        lat[0] = (RegionTuple(1, 2), RegionTuple(2, 1))
        lat[1] = (RegionTuple(1, 2), RegionTuple(2, 1))
        latm = np.empty(2, dtype=object)
        latm[0] = (RegionTuple(2, 1), RegionTuple(1, 2))
        latm[1] = (RegionTuple(2, 1), RegionTuple(1, 2))

        out = self.A.spatial_mirror_array(lat)
        self.assertTrue(np.array_equal(latm, out), msg="spatial mirroring incorrect")
        self.assertIs(out.dtype, np.dtype(object), msg="wrong type in out")
        out = np.empty(2, dtype=object)
        _ = self.A.spatial_mirror_array(lat, out=out)
        self.assertTrue(np.array_equal(latm, out), msg="out arg not changed in place")

        with self.assertRaises(
            LatticeGenerationError, msg="mismatched len(out) < len(lat)"
        ):
            _ = self.A.spatial_mirror_array(lat, out=np.empty(1, dtype=object))

        with self.assertRaises(
            LatticeGenerationError, msg="mismatched len(out) > len(lat)"
        ):
            _ = self.A.spatial_mirror_array(lat, out=np.empty(4, dtype=object))

        with self.assertRaises(TypeError, msg="out with incompatible type"):
            _ = self.A.spatial_mirror_array(lat, np.ones(2, dtype=int))

    def test_temporal_mirror_array(self):
        """Test temporal mirror correct output and invalid inputs."""

        lat = np.empty(2, dtype=object)
        lat[0] = (RegionTuple(1, 2), RegionTuple(2, 1))
        lat[1] = (RegionTuple(2, 1), RegionTuple(1, 2))
        latm = np.empty(2, dtype=object)
        latm[0] = (RegionTuple(2, 1), RegionTuple(1, 2))
        latm[1] = (RegionTuple(1, 2), RegionTuple(2, 1))

        out = self.A.temporal_mirror_array(lat)
        self.assertTrue(np.array_equal(latm, out), msg="temporal mirroring incorrect")
        self.assertIs(out.dtype, np.dtype(object), msg="wrong type in out")
        out = np.empty(2, dtype=object)
        _ = self.A.temporal_mirror_array(lat, out=out)
        self.assertTrue(np.array_equal(latm, out), msg="out arg not changed in place")

        with self.assertRaises(ValueError, msg="mismatched len(out) < len(lat)"):
            _ = self.A.temporal_mirror_array(lat, out=np.empty(1, dtype=object))

        with self.assertRaises(ValueError, msg="mismatched len(out) > len(lat)"):
            _ = self.A.temporal_mirror_array(lat, out=np.empty(4, dtype=object))

        with self.assertRaises(TypeError, msg="out with incompatible type"):
            _ = self.A.temporal_mirror_array(lat, np.ones(2, dtype=int))

    def test_spatial_extend_array(self):
        """Test spatial extend merges correctly."""

        def test_out(lat, latm, lato, msg):
            with self.subTest(msg=msg):
                out = self.A.spatial_extend_array(lat, latm)
                self.assertTrue(
                    np.array_equal(lato, out), msg="spatial extend incorrect"
                )
                self.assertIs(out.dtype, np.dtype(object), msg="wrong type in out")
                out = np.empty(2, dtype=object)
                _ = self.A.spatial_extend_array(lat, latm, out=out)
                self.assertTrue(
                    np.array_equal(lato, out), msg="out arg not changed in place"
                )

        lat = np.empty(2, dtype=object)
        lat[0] = (RegionTuple(1, 2), RegionTuple(2, 1))
        lat[1] = (RegionTuple(2, 1), RegionTuple(1, 2))
        latm = np.empty(2, dtype=object)
        latm[0] = (RegionTuple(2, 1), RegionTuple(1, 2))
        latm[1] = (RegionTuple(1, 2), RegionTuple(2, 1))
        lato = np.empty(2, dtype=object)
        lato[0] = (RegionTuple(1, 2), RegionTuple(2, 2), RegionTuple(1, 2))
        lato[1] = (RegionTuple(2, 1), RegionTuple(1, 4), RegionTuple(2, 1))

        test_out(lat, latm, lato, msg="merge all time slices")

        # resolution mismatch
        latm[0] = (RegionTuple(1, 1), RegionTuple(1, 2))
        lato[0] = (
            RegionTuple(1, 2),
            RegionTuple(2, 1),
            RegionTuple(1, 1),
            RegionTuple(1, 2),
        )
        lato[1] = (RegionTuple(2, 1), RegionTuple(1, 4), RegionTuple(2, 1))

        test_out(lat, latm, lato, msg="spatial mismatch")

        with self.assertRaises(
            LatticeGenerationError, msg="mismatched len(out) < len(left)"
        ):
            _ = self.A.spatial_extend_array(lat, latm, out=np.empty(1, dtype=object))

        with self.assertRaises(
            LatticeGenerationError, msg="mismatched len(out) > len(left)"
        ):
            _ = self.A.spatial_extend_array(lat, latm, out=np.empty(4, dtype=object))

        with self.assertRaises(
            LatticeGenerationError, msg="mismatched len(left) > len(right)"
        ):
            _ = self.A.spatial_extend_array(
                np.tile(lat, 2), latm, out=np.empty(4, dtype=object)
            )

        with self.assertRaises(
            LatticeGenerationError, msg="mismatched len(left) < len(right)"
        ):
            _ = self.A.spatial_extend_array(
                lat, np.tile(latm, 2), out=np.empty(2, dtype=object)
            )

        with self.assertRaises(TypeError, msg="out with incompatible type"):
            _ = self.A.spatial_extend_array(lat, latm, out=np.ones(2, dtype=int))

    def test_temporal_extend_array(self):
        """Test temporal extend implementation."""

        lat_top = np.empty(2, dtype=object)
        lat_top[0] = (RegionTuple(2, 5), RegionTuple(1, 2))
        lat_top[1] = (RegionTuple(2, 4), RegionTuple(1, 8))

        lat_bot = np.empty(2, dtype=object)
        lat_bot[0] = (RegionTuple(2, 4),)
        lat_bot[1] = (RegionTuple(4, 3),)

        out = LatticeArray.temporal_extend_array(bottom=lat_bot, top=lat_top)
        with self.subTest(msg="incorrect order"):
            self.assertEqual(out[0], lat_bot[0])
            self.assertEqual(out[1], lat_bot[1])
            self.assertEqual(out[2], lat_top[0])
            self.assertEqual(out[3], lat_top[1])

        self.assertIs(out.dtype, np.dtype(object), msg="output dtype wrong")

        out_inplace = np.empty(4, dtype=object)
        LatticeArray.temporal_extend_array(bottom=lat_bot, top=lat_top, out=out_inplace)
        self.assertTrue(
            np.array_equal(out, out_inplace), msg="given out not changed inplace"
        )

        lat_bot[1] = (RegionTuple(1, 3), RegionTuple(2, 4))
        with self.assertRaises(LatticeGenerationError, msg="spatial dimension wrong"):
            out = LatticeArray.temporal_extend_array(bottom=lat_bot, top=lat_top)

        lat_bot[1] = (RegionTuple(4, 3),)
        with self.assertRaises(ValueError, msg="out has wrong shape"):
            out = LatticeArray.temporal_extend_array(
                bottom=lat_bot, top=lat_top, out=np.empty(5, dtype=object)
            )
        with self.assertRaises(TypeError, msg="out has wrong type"):
            out = LatticeArray.temporal_extend_array(
                bottom=lat_bot, top=lat_top, out=np.empty(4)
            )

    def test_latice_array_repr(self):
        """Test if `repr(LatticeArray)` gives correct output."""

        repr_out = repr(self.A)
        for prop in self.A.PROPERTY_DESCRIPTIONS:
            self.assertIn(str(getattr(self.A, prop).value), repr_out)
        self.assertIn(self.A.scheme.name, repr_out)
        self.assertIn(str(hex(id(self.A))), repr_out)


def do_for_all_lattice_arrays(*args, class_list=None):
    """Decorator wrapping test in loop for all LatticeArrays.

    Args:
        test_f: Test function to wrap with loop.
        class_list: List of `LatticeArray` classes to loop over.
    """
    if not class_list:
        class_list = (LatticeArray, SuperimposeLatticeArray)

    def _loop_test(test_f):
        def loop_test(test_instance):
            """Loop test for LatticeArray types."""

            print(f"\n{test_f.__doc__} ... ", end="", flush=True)

            for LA in class_list:
                A = LA()
                with test_instance.subTest():
                    test_f(test_instance, A)

        return loop_test

    if len(args) == 1 and callable(args[0]):
        return _loop_test(args[0])
    # return decorator if called with only kwarg set
    return _loop_test


class TestLatticeArrayCommons(unittest.TestCase):
    """Test functionality common for all LatticeArrays."""

    # pylint: disable=protected-access
    ALL_LATTICES = (
        LatticeArray,
        SuperimposeLatticeArray,
        HomogeneousLatticeArray,
        PulseLatticeArray,
        SuperimposePulseLatticeArray,
    )

    @do_for_all_lattice_arrays(class_list=ALL_LATTICES)
    def test_propagate(self, A: LatticeArray):
        """Test if propagate does correct modifications."""
        # this does not validate the output lattice

        scheme: str
        for scheme in filter(None, A.scheme.SCHEME_DESCRIPTIONS):
            with self.subTest(
                msg="\n''Test if propagate does correct modifications.''"
                f" for: {type(A)} and: {scheme=}"
            ):
                # set up lattice
                if not isinstance(A, HomogeneousLatticeArray):
                    # scheme not defined for HomogeneousLatticeArray
                    A.scheme.name = scheme
                    A.borders_width.increment(3)
                    A.finest_width.increment(4)
                A.coarse_steps.value = 3
                A.buffer.value = 3
                A.repeat.increment(2)
                # implicit call through generate_array()
                A.generate_array()

                # number of time slices:
                # repeat * coarsest cell
                num = A.repeat.value * 2**A.coarse_steps.value
                self.assertEqual(len(A.array), num, msg="wrong number of time slices")
                self.assertIs(A.array.dtype, np.dtype(object), msg="wrong type in out")

    @do_for_all_lattice_arrays
    def test_fingerprint(self, A: LatticeArray):
        """Test if fingerprint property returns correct values."""

        prop_dict, mod_hist, lattice_type = A.fingerprint
        self.assertIs(type(A), lattice_type)
        self.assertEqual(A._modifier_history, mod_hist)
        self.assertIsNot(A._modifier_history, mod_hist)
        self.assertEqual(prop_dict["scheme"], A.scheme.name)
        for prop in A.PROPERTY_DESCRIPTIONS:
            self.assertEqual(prop_dict[prop], getattr(A, prop).value)

    @do_for_all_lattice_arrays
    def test_copy(self, A: LatticeArray):
        """Test if LatticeArray.copy works."""

        with self.subTest(msg="not generated array"):
            A.scheme.set_fixed_number()
            copy = A.copy()
            fp = A.fingerprint
            copy_fp = copy.fingerprint
            self.assertDictEqual(copy_fp[0], fp[0])
            self.assertIsNot(copy_fp[0], fp[0])
            self.assertListEqual(copy_fp[1], fp[1])
            for copy_mh, mh in zip(copy_fp[1], fp[1]):
                self.assertIsNot(copy_mh, mh)
            self.assertIsNot(copy_fp[1], fp[1])
            self.assertIs(copy_fp[2], fp[2])
            self.assertFalse(hasattr(copy, "_array"))

        with self.subTest(msg="with generated array"):
            A.scheme.set_fixed_number()
            A.generate_array()
            copy = A.copy()
            fp = A.fingerprint
            copy_fp = copy.fingerprint
            self.assertDictEqual(copy_fp[0], fp[0])
            self.assertIsNot(copy_fp[0], fp[0])
            self.assertListEqual(copy_fp[1], fp[1])
            for copy_mh, mh in zip(copy_fp[1], fp[1]):
                self.assertIsNot(copy_mh, mh)
            self.assertIsNot(copy_fp[1], fp[1])
            self.assertIs(copy_fp[2], fp[2])
            self.assertTrue(hasattr(copy, "_array"))
            self.assertIsNot(copy._array, A._array)
            self.assertTrue(np.array_equal(copy.array, A.array))

    def test_modifer_history(self):
        """Test if modifiers record history."""

        A = LatticeArray(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME)
        A.generate_array()

        with self.subTest(msg="spatial mirror history"):
            A.spatial_mirror()
            latest = A._modifier_history[-1]
            self.assertIn(LatticeArray.spatial_mirror, latest)
            self.assertIn(tuple(), latest)
            self.assertIn({}, latest)

        with self.subTest(msg="temporal mirror history"):
            A.temporal_mirror()
            latest = A._modifier_history[-1]
            self.assertIn(LatticeArray.temporal_mirror, latest)
            self.assertIn(tuple(), latest)
            self.assertIn({}, latest)

        with self.subTest(msg="spatial extend history"):
            fp = A.fingerprint
            A.spatial_extend(A)
            latest = A._modifier_history[-1]
            # private function is recorded
            self.assertIn(LatticeArray._spatial_extend, latest)
            self.assertIn(fp, latest)
            self.assertIn({}, latest)

        with self.subTest(msg="temporal extend history"):
            fp = A.fingerprint
            A.temporal_extend(A)
            latest = A._modifier_history[-1]
            # private function is recorded
            self.assertIn(LatticeArray._temporal_extend, latest)
            self.assertIn(fp, latest)
            self.assertIn({}, latest)

        A = SuperimposeLatticeArray(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME)
        A.generate_array()

        with self.subTest(msg="superimpose history"):
            fp = A.fingerprint
            A.superimpose(A)
            latest = A._modifier_history[-1]
            # private function is recorded
            self.assertIn(SuperimposeLatticeArray._superimpose, latest)
            self.assertIn(fp, latest)
            self.assertIn({}, latest)

        A = HomogeneousLatticeArray()
        A.generate_array()

        with self.subTest(msg="homogeneous array"):
            # HomogeneousLatticeArray doesn't add any modifiers,
            # but check if correct type is recorded in history
            A.spatial_mirror()
            self.assertIn(
                HomogeneousLatticeArray.spatial_mirror, A._modifier_history[-1]
            )
            A.temporal_mirror()
            self.assertIn(
                HomogeneousLatticeArray.temporal_mirror, A._modifier_history[-1]
            )
            A.spatial_extend(A)
            self.assertIn(
                HomogeneousLatticeArray._spatial_extend, A._modifier_history[-1]
            )
            A.temporal_extend(A)
            self.assertIn(
                HomogeneousLatticeArray._temporal_extend, A._modifier_history[-1]
            )
            for step in A._modifier_history[2:]:
                # check if fingerprint has correct type
                self.assertIn(HomogeneousLatticeArray, step[1])

    @do_for_all_lattice_arrays
    def test_modifier_replay(self, A: LatticeArray):
        """Test if replaying modifier history works."""

        # set any of the available schemes
        if not A.scheme.name:
            A.scheme.name = next(iter(A.scheme.SCHEME_DESCRIPTIONS))
        A.coarse_steps.value = 3
        A.buffer.value = 3
        A.repeat.increment(2)
        A.generate_array()

        A.temporal_mirror()
        A.spatial_extend(A)
        A.temporal_extend(A)
        A.spatial_mirror()

        prop_dict, hist, la_type = A.fingerprint
        B: LatticeArray = la_type(**prop_dict)
        B._modifier_history = hist
        B.generate_array()

        self.assertTrue(np.array_equal(A.array, B.array), msg="replay fresh history")

        A.buffer.increment()
        with self.assertRaises(
            ModifierHistoryError, msg="incompatible modifier after property change"
        ):
            A.generate_array()

        # test if dependency on A leaked into history
        A._array = None
        B.generate_array()

    def test_superimpose_modifier_replay(self):
        """Test if `_superimpose` works."""

        A = SuperimposeLatticeArray(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME)
        A.coarse_steps.increment(2)
        A.finest_width.increment(5)
        A.buffer.increment(2)
        A.repeat.increment(3)
        A.generate_array()

        # prepare homogeneous lattice of proper dimension
        t_dim, (x_dim,) = A.tx_dims
        B = HomogeneousLatticeArray(
            coarse_steps=3, buffer=x_dim // 2**3, repeat=t_dim // 2**3
        )
        B.generate_array()

        A.superimpose(B)
        B._array = None

        array_backup = A.array
        A.generate_array()
        self.assertTrue(np.array_equal(array_backup, A.array))


class TestSuperimposeLatticeArray(unittest.TestCase):
    """Test features specific to SuperimposeLatticeArray."""

    def test_superimpose_array(self):
        """Test if superimpose logic works."""

        lati = np.empty(2, dtype=object)
        lati[0] = (RegionTuple(2, 5), RegionTuple(1, 22))
        lati[1] = (
            RegionTuple(8, 1),
            RegionTuple(4, 2),
            RegionTuple(2, 4),
            RegionTuple(1, 8),
        )
        latm = np.empty(2, dtype=object)
        latm[0] = (RegionTuple(8, 3), RegionTuple(4, 2))
        latm[1] = (RegionTuple(8, 4),)
        lato = np.empty(2, dtype=object)
        lato[0] = (RegionTuple(2, 5), RegionTuple(1, 22))
        lato[1] = (
            RegionTuple(8, 1),
            RegionTuple(4, 2),
            RegionTuple(2, 4),
            RegionTuple(1, 8),
        )

        A = SuperimposeLatticeArray()

        out = A.superimpose_array(lati, latm)
        self.assertTrue(np.array_equal(lato, out), msg="spatial extend incorrect")
        self.assertIs(out.dtype, np.dtype(object), msg="wrong type in out")
        out = np.empty(2, dtype=object)
        _ = A.superimpose_array(lati, latm, out=out)
        self.assertTrue(np.array_equal(lato, out), msg="out arg not changed in place")

        with self.assertRaises(TypeError, msg="wrong dtype for out"):
            A.superimpose_array(lati, latm, out=np.empty(2, dtype=int))

        with self.assertRaises(LatticeGenerationError, msg="input temporal size wrong"):
            A.superimpose_array(lati, np.tile(latm, 2))

        with self.assertRaises(LatticeGenerationError, msg="out temporal size wrong"):
            A.superimpose_array(lati, latm, np.empty(4, dtype=object))

        # last region is to wide
        lati[0] = (RegionTuple(2, 5), RegionTuple(1, 24))
        with self.assertRaises(LatticeGenerationError, msg="input spatial size wrong"):
            A.superimpose_array(lati, latm)

        # spill from previous regions
        lati[0] = (RegionTuple(2, 6), RegionTuple(1, 22))
        with self.assertRaises(LatticeGenerationError, msg="input spatial size wrong"):
            A.superimpose_array(lati, latm)

    def test_volatile_properties(self):
        """Test values of volatile properties except .array."""

        A = SuperimposeLatticeArray(
            scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME,
            coarse_steps=1,
            borders_width=6,
            buffer=1,
            finest_width=8,
            repeat=8,
        )
        A.generate_array()

        self.assertEqual(
            A.max_num_cells, {1: 24, 2: 4}, msg="max_num_cells property wrong"
        )
        self.assertEqual(
            A.resolution_list, [1, 2], msg="resolution_list property wrong"
        )
        self.assertEqual(A.tx_dims, (16, (24,)), msg="tx_dims property wrong")


class TestHomogeneousLatticeArray(unittest.TestCase):
    """Test features specific to HomogeneousLatticeArray."""

    # pylint: disable=attribute-defined-outside-init

    def test_homogeneous_lattice(self):
        """Test `HomogeneousLatticeArray`."""

        with self.subTest(msg="HomogeneousLA.generate() and array prop test"):
            self.A = HomogeneousLatticeArray()
            TestLatticeArrayBasics.test_volatile_property_access(self)

        A = HomogeneousLatticeArray(coarse_steps=3, buffer=8, repeat=4)
        A.generate_array()

        with self.subTest(msg="test array shape"):
            temp_res = A.array[0][0].a
            space_res = A.array[0][0].n
            self.assertEqual(
                temp_res * A.repeat.value,
                len(A.array),
                msg="incorrect number of time slices",
            )
            self.assertEqual(
                A.buffer.value, space_res, msg="incorrect spatial width per region"
            )

        self.assertCountEqual(
            np.unique(A.array), [A.array[0]], msg="array not homogeneous"
        )

    def test_volatile_properties(self):
        """Test values of volatile properties except .array."""

        A = HomogeneousLatticeArray(coarse_steps=3, buffer=8, repeat=4)
        A.generate_array()

        self.assertEqual(A.max_num_cells, {8: 8}, msg="max_num_cells property wrong")
        self.assertEqual(A.resolution_list, [8], msg="resolution_list property wrong")
        self.assertEqual(A.tx_dims, (32, (64,)), msg="tx_dims property wrong")


class TestValidLatticeArrayGeneration(unittest.TestCase):
    """Verify that arrays generated by `LatticeArray` are valid."""

    # pylint: disable=consider-using-with

    @classmethod
    def setUpClass(cls):
        cls.devnull = open(os.devnull, "w", encoding="utf_8")

    @classmethod
    def tearDownClass(cls):
        cls.devnull.close()

    def test_lattice_array_minimal_config(self):
        """Test if LatticeArray instances are valid lattices."""
        # minimal lattice - all props

        prop_sort = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]

        def reset_fixture():
            lattice = LatticeArray()
            V = ValidateLattice(
                lattice,
                preset=ValidateLattice.LATTICEARRAY_TEST_CASES,
                sorted_props=prop_sort,
                increments=[4] * len(prop_sort),
            )
            return lattice, V

        def reset_lattice(lat):
            lat.clear_modifier_history()
            lat.generate_array()

        with self.subTest(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME):
            lattice, V = reset_fixture()
            lattice.scheme.set_fixed_width()
            with self.subTest(modifier="none"):
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="temporal mirror"):
                reset_lattice(lattice)
                lattice.temporal_mirror()
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="spatial mirror"):
                reset_lattice(lattice)
                lattice.spatial_mirror()
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="spatial and temporal mirror"):
                reset_lattice(lattice)
                lattice.temporal_mirror()
                lattice.spatial_mirror()
                self.assertTrue(V(stderr=self.devnull))

        with self.subTest(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME):
            lattice, V = reset_fixture()
            lattice.scheme.set_fixed_number()
            with self.subTest(modifier="none"):
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="temporal mirror"):
                reset_lattice(lattice)
                lattice.temporal_mirror()
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="spatial mirror"):
                reset_lattice(lattice)
                lattice.spatial_mirror()
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="spatial and temporal mirror"):
                reset_lattice(lattice)
                lattice.temporal_mirror()
                lattice.spatial_mirror()
                self.assertTrue(V(stderr=self.devnull))

    def test_lattice_array_minimal_scaled(self):
        """Test if LatticeArray instances are valid lattices."""
        # minimal lattice - one prop scaled

        prop_sort = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]

        def reset_fixture(prop):
            lattice = LatticeArray()
            V = ValidateLattice(
                lattice,
                preset=ValidateLattice.LATTICEARRAY_TEST_CASES,
                sorted_props=[prop],
                increments=[10],
            )
            return lattice, V

        def reset_lattice(lat):
            lat.clear_modifier_history()
            lat.generate_array()

        for prop in prop_sort:
            with self.subTest(prop=prop):

                with self.subTest(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME):
                    lattice, V = reset_fixture(prop)
                    lattice.scheme.set_fixed_width()
                    with self.subTest(modifier="none"):
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="temporal mirror"):
                        reset_lattice(lattice)
                        lattice.temporal_mirror()
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="spatial mirror"):
                        reset_lattice(lattice)
                        lattice.spatial_mirror()
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="spatial and temporal mirror"):
                        reset_lattice(lattice)
                        lattice.temporal_mirror()
                        lattice.spatial_mirror()
                        self.assertTrue(V(stderr=self.devnull))

                with self.subTest(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME):
                    lattice, V = reset_fixture(prop)
                    lattice.scheme.set_fixed_number()
                    with self.subTest(modifier="none"):
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="temporal mirror"):
                        reset_lattice(lattice)
                        lattice.temporal_mirror()
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="spatial mirror"):
                        reset_lattice(lattice)
                        lattice.spatial_mirror()
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="spatial and temporal mirror"):
                        reset_lattice(lattice)
                        lattice.temporal_mirror()
                        lattice.spatial_mirror()
                        self.assertTrue(V(stderr=self.devnull))

    def test_superimpose_lattice_array_minimal_config(self):
        """Test if SuperimposeLatticeArray instances are valid."""

        # minimal lattice - all props

        prop_sort = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]

        def reset_fixture():
            lattice = SuperimposeLatticeArray()
            V = ValidateLattice(
                lattice,
                preset=ValidateLattice.SUPERIMPOSE_TEST_CASES,
                sorted_props=prop_sort,
                increments=[3] * len(prop_sort),
            )
            return lattice, V

        def reset_lattice(lat):
            lat.clear_modifier_history()
            lat.generate_array()

        with self.subTest(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME):
            lattice, V = reset_fixture()
            lattice.scheme.set_fixed_width()
            with self.subTest(modifier="none"):
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="temporal mirror"):
                reset_lattice(lattice)
                lattice.temporal_mirror()
                self.assertTrue(V(stderr=self.devnull))

        with self.subTest(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME):
            lattice, V = reset_fixture()
            lattice.scheme.set_fixed_number()
            with self.subTest(modifier="none"):
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="temporal mirror"):
                reset_lattice(lattice)
                lattice.temporal_mirror()
                self.assertTrue(V(stderr=self.devnull))

    def test_superimpose_lattice_array_minimal_scaled(self):
        """Test if SuperimposeLatticeArray instances are valid."""

        # minimal lattice - one prop scaled
        prop_sort = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]

        def reset_fixture(prop):
            lattice = SuperimposeLatticeArray()
            V = ValidateLattice(
                lattice,
                preset=ValidateLattice.SUPERIMPOSE_TEST_CASES,
                sorted_props=[prop],
                increments=[7],
            )
            return lattice, V

        def reset_lattice(lat):
            lat.clear_modifier_history()
            lat.generate_array()

        for prop in prop_sort:
            with self.subTest(prop=prop):

                with self.subTest(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME):
                    lattice, V = reset_fixture(prop)
                    lattice.scheme.set_fixed_width()
                    with self.subTest(modifier="none"):
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="temporal mirror"):
                        reset_lattice(lattice)
                        lattice.temporal_mirror()
                        self.assertTrue(V(stderr=self.devnull))

                with self.subTest(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME):
                    lattice, V = reset_fixture(prop)
                    lattice.scheme.set_fixed_number()
                    with self.subTest(modifier="none"):
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="temporal mirror"):
                        reset_lattice(lattice)
                        lattice.temporal_mirror()
                        self.assertTrue(V(stderr=self.devnull))

    def test_homogeneous_lattice(self):
        """Test if HomogeneousLatticeArray instances are valid."""

        PROPS = {
            "coarse_steps": 4,
            "buffer": 10,
            "repeat": 10,
        }
        lattice = HomogeneousLatticeArray(**PROPS)
        lattice.generate_array()
        prop_sort = [
            "buffer",
            "coarse_steps",
            "repeat",
        ]
        V = ValidateLattice(
            lattice,
            preset=ValidateLattice.HOMOGENEOUS_TEST_CASES,
            sorted_props=prop_sort,
            increments=[2] * 3,
        )
        self.assertTrue(V(stderr=self.devnull))

    def test_pulse_lattice_minimal_config(self):
        """Test if PulseLatticeArray instances are valid."""
        # minimal lattice - all props

        prop_sort = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]

        def reset_fixture():
            lattice = PulseLatticeArray()
            V = ValidateLattice(
                lattice,
                preset=ValidateLattice.LATTICEARRAY_TEST_CASES - {"test_coarse_steps"},
                sorted_props=prop_sort,
                increments=[4] * len(prop_sort),
            )
            return lattice, V

        def reset_lattice(lat):
            lat.clear_modifier_history()
            lat.generate_array()

        with self.subTest(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME):
            lattice, V = reset_fixture()
            lattice.scheme.set_fixed_width()
            with self.subTest(modifier="none"):
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="temporal mirror"):
                reset_lattice(lattice)
                lattice.temporal_mirror()
                self.assertTrue(V(stderr=self.devnull))

        with self.subTest(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME):
            lattice, V = reset_fixture()
            lattice.scheme.set_fixed_number()
            with self.subTest(modifier="none"):
                self.assertTrue(V(stderr=self.devnull))
            with self.subTest(modifier="temporal mirror"):
                reset_lattice(lattice)
                lattice.temporal_mirror()
                self.assertTrue(V(stderr=self.devnull))

    def test_pulse_lattice_minimal_scaled(self):
        """Test if PulseLatticeArray instances are valid."""
        # minimal lattice - one prop scaled

        prop_sort = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]

        def reset_fixture(prop):
            lattice = PulseLatticeArray()
            V = ValidateLattice(
                lattice,
                preset=ValidateLattice.LATTICEARRAY_TEST_CASES - {"test_coarse_steps"},
                sorted_props=[prop],
                increments=[10],
            )
            return lattice, V

        def reset_lattice(lat):
            lat.clear_modifier_history()
            lat.generate_array()

        for prop in prop_sort:
            with self.subTest(prop=prop):

                with self.subTest(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME):
                    lattice, V = reset_fixture(prop)
                    lattice.scheme.set_fixed_width()
                    with self.subTest(modifier="none"):
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="temporal mirror"):
                        reset_lattice(lattice)
                        lattice.temporal_mirror()
                        self.assertTrue(V(stderr=self.devnull))

                with self.subTest(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME):
                    lattice, V = reset_fixture(prop)
                    lattice.scheme.set_fixed_number()
                    with self.subTest(modifier="none"):
                        self.assertTrue(V(stderr=self.devnull))
                    with self.subTest(modifier="temporal mirror"):
                        reset_lattice(lattice)
                        lattice.temporal_mirror()
                        self.assertTrue(V(stderr=self.devnull))

    def test_superimpose_pulse_lattice_minimal_config(self):
        """Test if SuperimposePulseLatticeArray instances are valid."""
        # minimal lattice - all props

        prop_sort = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]

        def reset_fixture():
            lattice = SuperimposePulseLatticeArray()
            V = ValidateLattice(
                lattice,
                preset=ValidateLattice.SUPERIMPOSE_TEST_CASES,
                sorted_props=prop_sort,
                increments=[3] * len(prop_sort),
            )
            return lattice, V

        with self.subTest(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME):
            lattice, V = reset_fixture()
            lattice.scheme.set_fixed_width()
            self.assertTrue(V(stderr=self.devnull))

        with self.subTest(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME):
            lattice, V = reset_fixture()
            lattice.scheme.set_fixed_number()
            self.assertTrue(V(stderr=self.devnull))

    def test_superimpose_pulse_lattice_minimal_scaled(self):
        """Test if SuperimposePulseLatticeArray instances are valid."""
        # minimal lattice - one prop scaled

        prop_sort = [
            "buffer",
            "coarse_steps",
            "borders_width",
            "finest_width",
            "repeat",
        ]

        def reset_fixture(prop):
            lattice = SuperimposePulseLatticeArray()
            V = ValidateLattice(
                lattice,
                preset=ValidateLattice.SUPERIMPOSE_TEST_CASES,
                sorted_props=[prop],
                increments=[7],
            )
            return lattice, V

        for prop in prop_sort:
            with self.subTest(prop=prop):

                with self.subTest(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME):
                    lattice, V = reset_fixture(prop)
                    lattice.scheme.set_fixed_width()
                    self.assertTrue(V(stderr=self.devnull))

                with self.subTest(scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME):
                    lattice, V = reset_fixture(prop)
                    lattice.scheme.set_fixed_number()
                    self.assertTrue(V(stderr=self.devnull))


if __name__ == "__main__":
    unittest.main()
