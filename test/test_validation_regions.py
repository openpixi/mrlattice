"""Setup with validation test functions for LatticeArray regions."""

import unittest
from io import StringIO
from unittest.mock import MagicMock

import numpy as np

from mrlattice.lattice.generate import LatticeArray, RegionTuple
from mrlattice.lattice.validate import ValidateLattice


class TestValidateLatticeSpatialTests(unittest.TestCase):
    """Test if ValidateLattice tests for spatial are working."""

    def setUp(self):
        self.errout = StringIO()
        self.A = MagicMock()
        self.A.PROPERTY_DESCRIPTIONS = {"dummy": "a dummy property"}
        self.A.coarse_steps.value = 3
        self.A.buffer.value = 1
        self.A.finest_width.value = 8
        self.A.borders_width.value = 5
        self.t = 13
        self.V = ValidateLattice(lattice=self.A, sorted_props=["dummy"], increments=[1])

    def test_spatial_width(self):
        """Test if inconsistent spatial width is caught."""

        self.V.test_spatial_width = True
        self.A.array = [
            (  # t=0
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 9),
            ),
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
            ),
        ]
        ts = [
            (RegionTuple(8, 2), RegionTuple(4, 6), RegionTuple(2, 6), RegionTuple(1, 8))
        ]

        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"width of time slice {self.t} differs", errout)
        self.assertIn("FAIL: test_spatial_width", errout)

    def test_zero_width(self):
        """Test if zero width regions are caught."""

        self.V.test_zero_width = True
        ts = [
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
                RegionTuple(4, 0),
            )
        ]

        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"slice {self.t}", errout)
        self.assertIn(f"zero width: {[ts[0][-1]]}", errout)
        self.assertIn("FAIL: test_zero_width", errout)

    def test_neighbor_merge(self):
        """Test if possible merges are caught."""

        self.V.test_neighbor_merge = True
        ts = [
            (RegionTuple(8, 2), RegionTuple(4, 6), RegionTuple(2, 6), RegionTuple(2, 8))
        ]

        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"slice {self.t}", errout)
        self.assertIn(f"same resolution {ts[0][-1].a}", errout)
        self.assertIn("FAIL: test_neighbor_merge", errout)

    def test_spatial_neighbors(self):
        """Test if spatial neighbor res difference > 2x is caught."""

        self.V.test_spatial_neighbors = True

        with self.subTest(msg="step up to big"):
            ts = [
                (
                    RegionTuple(8, 2),
                    RegionTuple(4, 6),
                    RegionTuple(16, 6),
                    RegionTuple(8, 8),
                )
            ]
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn("more than a factor of 2", errout)
            self.assertIn(f"reg 1: {ts[0][1]}\treg 2: {ts[0][2]}", errout)
            self.assertIn("FAIL: test_spatial_neighbors", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(msg="step down to small"):
            ts = [
                (
                    RegionTuple(8, 2),
                    RegionTuple(16, 6),
                    RegionTuple(4, 6),
                    RegionTuple(8, 8),
                )
            ]
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn("more than a factor of 2", errout)
            self.assertIn(f"reg 1: {ts[0][1]}\treg 2: {ts[0][2]}", errout)
            self.assertIn("FAIL: test_spatial_neighbors", errout)


class NeighborTests(unittest.TestCase):
    """Common setup for all neighbor tests."""

    def setUp(self):
        self.errout = StringIO()
        self.A = MagicMock()
        self.A.PROPERTY_DESCRIPTIONS = {"dummy": "a dummy property"}
        self.A.coarse_steps.value = 3
        self.A.buffer.value = 1
        self.A.finest_width.value = 8
        self.A.borders_width.value = 5
        self.t = 13
        self.V = ValidateLattice(lattice=self.A, sorted_props=["dummy"], increments=[1])


class TestValidateHomogenNeighborTests(NeighborTests):
    """Test if ValidateLattice neighbor tests for homogen are good."""

    def setUp(self):
        super().setUp()
        self.V.test_homogeneous = True

    def test_multiple_regions(self):
        """Test inhomogen with multiple resolutions."""
        ts = [
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
            )
        ]
        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"slice {self.t}", errout)
        self.assertIn("more than one region", errout)
        self.assertIn("FAIL: test_homogeneous", errout)

    def test_resolution_change(self):
        """Test inhomogen with one change of resolution."""
        self.A.array = [(RegionTuple(8, 4),), (RegionTuple(4, 4),)]
        self.assertFalse(
            self.V.test_time_slices(self.t, [self.A.array[-1]], stderr=self.errout)
        )
        errout = self.errout.getvalue()
        self.assertIn(f"slice {self.t}", errout)
        self.assertIn("changes resolution", errout)
        self.assertIn(f"from {self.A.array[0][0].a} to {self.A.array[-1][0].a}", errout)
        self.assertIn("FAIL: test_homogeneous", errout)

    def test_num_cells_change(self):
        """Test inhomogen with change of number of cells."""

        self.A.array = [(RegionTuple(8, 2),), (RegionTuple(8, 4),)]
        self.assertFalse(
            self.V.test_time_slices(self.t, [self.A.array[-1]], stderr=self.errout)
        )
        errout = self.errout.getvalue()
        self.assertIn(f"slice {self.t}", errout)
        self.assertIn("changes number of cells", errout)
        self.assertIn(f"from {self.A.array[0][0].n} to {self.A.array[-1][0].n}", errout)
        self.assertIn("FAIL: test_homogeneous", errout)


class TestValidateDiagNeighborTests(NeighborTests):
    """Test if ValidateLattice neighbor tests for diag are good."""

    def setUp(self):
        super().setUp()
        self.V.test_diagonal_borders = True

    def test_wrong_finest(self):
        """Test diag with wrong finest region."""
        ts = [
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
            ),
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 12),
            ),
        ]
        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"slices {self.t} and {self.t +1}", errout)
        self.assertIn("discontinuous diagonal borders", errout)
        self.assertIn(f"for regions:\n{ts[0][3]} and {ts[1][3]}", errout)
        self.assertIn("FAIL: test_diagonal_borders", errout)

        ts = [
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
            ),
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 4),
            ),
        ]
        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"slices {self.t} and {self.t +1}", errout)
        self.assertIn("discontinuous diagonal borders", errout)
        self.assertIn(f"for regions:\n{ts[0][3]} and {ts[1][3]}", errout)
        self.assertIn("FAIL: test_diagonal_borders", errout)

    def test_wrong_generic(self):
        """Test diag with wrong generic region."""
        ts = [
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
            ),
            (
                RegionTuple(8, 2),
                RegionTuple(4, 2),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
            ),
        ]
        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"slices {self.t} and {self.t +1}", errout)
        self.assertIn("discontinuous diagonal borders", errout)
        self.assertIn(f"for regions:\n{ts[0][1]} and {ts[1][1]}", errout)
        self.assertIn("FAIL: test_diagonal_borders", errout)

        ts = [
            (
                RegionTuple(8, 2),
                RegionTuple(4, 6),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
            ),
            (
                RegionTuple(8, 2),
                RegionTuple(4, 10),
                RegionTuple(2, 6),
                RegionTuple(1, 8),
            ),
        ]
        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"slices {self.t} and {self.t +1}", errout)
        self.assertIn("discontinuous diagonal borders", errout)
        self.assertIn(f"for regions:\n{ts[0][1]} and {ts[1][1]}", errout)
        self.assertIn("FAIL: test_diagonal_borders", errout)


class TestValidateTempNeighborTests(NeighborTests):
    """Test if ValidateLattice neighbor tests for temporal are good."""

    def setUp(self):
        super().setUp()
        self.V.test_temporal_neighbors = True

    def temp_neighbors_fixture(self, time_slices, res, msg):
        """Helper fixture for testing."""

        with self.subTest(mirrors=msg):
            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)
            self.assertFalse(
                self.V.test_time_slices(self.t, time_slices, stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn(f"slices {self.t} and {self.t +1}", errout)
            self.assertIn("differ more than 2x", errout)
            for r in res:
                self.assertIn(f"{r[0]} and {r[1]}", errout)
            self.assertIn("FAIL: test_temporal_neighbors", errout)

    def test_temporal_neighbors(self):
        """Test if wrong temporal neighbors are caught."""

        # this lattice has all tests checked
        # full valid lattice for plotting:
        # lattice = [
        #     (RegtionTuple(4, 1), RegtionTuple(2, 1)),
        #     (RegtionTuple(4, 1), RegtionTuple(2, 1)),
        #     (RegtionTuple(4, 1), RegtionTuple(2, 1)),
        #     (RegtionTuple(4, 1), RegtionTuple(2, 1)),
        #     (RegtionTuple(1, 2), RegtionTuple(2, 2)),
        #     (RegtionTuple(1, 2), RegtionTuple(2, 2)),
        # ]
        # this test requires np arrays, no tricks allowed,
        # because LatticeArray modifiers always return np arrays and
        # would build new ones for non np array input
        ts = np.empty(2, dtype=object)
        ts[0] = (RegionTuple(4, 1), RegionTuple(2, 1))
        ts[1] = (RegionTuple(1, 2), RegionTuple(2, 2))
        res = iter(
            [
                (("[0 1]", "[0 1]"), ("[1 2]", "[0 1]"), ("[0]", "[1]")),
                (("[4 5]", "[4 5]"), ("[5]", "[4]"), ("[3 4]", "[4 5]")),
                (("[0 1]", "[0 1]"), ("[1]", "[0]"), ("[0 1]", "[1 2]")),
                (("[4 5]", "[4 5]"), ("[4 5]", "[3 4]"), ("[4]", "[5]")),
            ]
        )
        # execute fixture for each temporal and spatial mirror combi
        for tmirr in [lambda x: x, LatticeArray.temporal_mirror_array]:
            for xmirr in [lambda x: x, LatticeArray.spatial_mirror_array]:

                self.temp_neighbors_fixture(
                    tmirr(xmirr(ts)), next(res), msg=f"{tmirr}\t{xmirr}"
                )


class TestValidateDoubleCellCusps(NeighborTests):
    """Test if ValidateLattice neighbor tests for dc cusps are good."""

    def setUp(self):
        super().setUp()
        self.V.test_double_cell_cusps = True

    def assert_errout_commons(self, errout_str):
        """Helper fixture for assertions."""
        self.assertIn(f"{self.t} and {self.t +1}", errout_str)
        self.assertIn("FAIL: test_double_cell_cusps", errout_str)

    def test_emerging_cusp(self):
        """Test if non-dc emerging cusps are caught."""
        ts = [
            [RegionTuple(1, 30), RegionTuple(2, 10), RegionTuple(1, 30)],
            [
                RegionTuple(1, 28),
                RegionTuple(2, 4),
                RegionTuple(4, 3),
                RegionTuple(2, 4),
                RegionTuple(1, 28),
            ],
        ]
        with self.subTest(config="centered"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assert_errout_commons(errout)
            self.assertIn("has no double cell tip", errout)
            self.assertIn(f"Region:\n{ts[1][2]} number 2", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        for regs in ts:
            del regs[0]

        with self.subTest(config="off-center"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assert_errout_commons(errout)
            self.assertIn("has no double cell tip", errout)
            self.assertIn(f"Region:\n{ts[1][1]} number 1", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

    def test_disappearing_cusp(self):
        """Test if non-dc disappearing cusps are caught."""
        ts = [
            [
                RegionTuple(1, 28),
                RegionTuple(2, 4),
                RegionTuple(4, 3),
                RegionTuple(2, 4),
                RegionTuple(1, 28),
            ],
            [RegionTuple(1, 30), RegionTuple(2, 10), RegionTuple(1, 30)],
        ]
        with self.subTest(config="centered"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assert_errout_commons(errout)
            self.assertIn("has no double cell tip", errout)
            self.assertIn(f"Region:\n{ts[0][2]} number 2", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        for regs in ts:
            del regs[0]

        with self.subTest(config="off-center"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assert_errout_commons(errout)
            self.assertIn("has no double cell tip", errout)
            self.assertIn(f"Region:\n{ts[0][1]} number 1", errout)

    def test_no_cusp(self):
        """Test if no cusp passes dc cusp tests."""

        with self.subTest(msg="diag borders - no cusp"):
            ts = [
                (RegionTuple(1, 30), RegionTuple(2, 10), RegionTuple(1, 30)),
                (RegionTuple(1, 28), RegionTuple(2, 10), RegionTuple(1, 32)),
            ]
            self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            self.assertFalse(self.errout.getvalue(), msg="stderr not empty")

        with self.subTest(msg="no cusp - something else"):
            ts = [
                (RegionTuple(1, 30), RegionTuple(2, 10), RegionTuple(1, 30)),
                (
                    RegionTuple(1, 28),
                    RegionTuple(2, 4),
                    RegionTuple(4, 2),
                    RegionTuple(2, 4),
                ),
            ]
            self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"{self.t} and {self.t +1}", errout)
            self.assertIn("SKIP: test_double_cell_cusps", errout)
            self.assertIn("cusp couldn't be identified", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(msg="invalid regs"):
            ts = [
                (RegionTuple(1, 10),),
                (RegionTuple(1, 10), RegionTuple(-1, -1), RegionTuple(-1, -1)),
            ]
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"{self.t} or {self.t +1}", errout)
            self.assertIn("FAIL: test_double_cell_cusps", errout)
            self.assertIn("invalid regions", errout)
            self.assertIn("[RegionTuple(a=-1, n=-1), RegionTuple(a=-1, n=-1)]", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(msg="some other type of insert"):
            # this is an invalid lattice
            ts = [
                (RegionTuple(2, 10), RegionTuple(1, 8)),
                (
                    RegionTuple(2, 10),
                    RegionTuple(4, 2),
                    RegionTuple(-1, -1),
                    RegionTuple(-1, -1),
                ),
            ]
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assert_errout_commons(errout)
            self.assertIn(f"1:\n{ts[1][1]}", errout)
            self.assertIn("4 =/= 2", errout)


class TestValidateCuspProxim(NeighborTests):
    """Test if ValidateLattice neighbor tests for cusp proxim are ok."""

    def setUp(self):
        super().setUp()
        self.V.test_cusp_proximity = True

    def assert_errout_commons(self, case, errout_str):
        """Helper fixture for assertions."""
        self.assertIn(f"{self.t} and {self.t +1}", errout_str)
        self.assertIn(f"{case}: test_cusp_proximity", errout_str)

    def test_no_cusp(self):
        """Test if no cusp is skipped."""
        # the tested time slices don't contain a cusp
        self.A.array = [
            (RegionTuple(1, 10),),
            (RegionTuple(1, 8), RegionTuple(2, 1)),
        ]
        self.assertTrue(
            self.V.test_time_slices(self.t, self.A.array, stderr=self.errout)
        )
        errout = self.errout.getvalue()
        self.assert_errout_commons("SKIP", errout)
        self.assertIn("couldn't be identified", errout)

    def test_no_cusp_other_insert(self):
        """Test if some other type of insert is caught."""
        # this is an invalid lattice
        # covers if a cusp is skipped, or reg res change but no cusp
        self.A.array = [
            (RegionTuple(2, 10), RegionTuple(1, 8)),
            (
                RegionTuple(2, 10),
                RegionTuple(4, 2),
                RegionTuple(-1, -1),
                RegionTuple(-1, -1),
            ),
        ]
        self.assertFalse(
            self.V.test_time_slices(self.t, self.A.array, stderr=self.errout)
        )
        errout = self.errout.getvalue()
        self.assert_errout_commons("FAIL", errout)
        self.assertIn("4 =/= 2", errout)

    def test_invalid_regs(self):
        """Test if invalid regs as fill value are caught."""
        # this is an invalid lattice
        # covers if zip_longest fill value is matched in lattice
        self.A.array = [
            (RegionTuple(1, 10),),
            (RegionTuple(1, 10), RegionTuple(-1, -1), RegionTuple(-1, -1)),
        ]
        self.assertFalse(
            self.V.test_time_slices(self.t, self.A.array, stderr=self.errout)
        )
        errout = self.errout.getvalue()
        self.assertIn(f"{self.t} or {self.t +1}", errout)
        self.assertIn("FAIL: test_cusp_proximity", errout)
        self.assertIn("invalid regions", errout)
        self.assertIn("[RegionTuple(a=-1, n=-1), RegionTuple(a=-1, n=-1)]", errout)

    def test_cusps_too_close(self):
        """Test for cascading cusps too close together."""

        with self.subTest(scale="1"):
            self.A.array = [None] * self.t + [
                (RegionTuple(1, 20),),
                (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
                (
                    RegionTuple(1, 4),
                    RegionTuple(2, 2),
                    RegionTuple(1, 4),
                    RegionTuple(2, 2),
                    RegionTuple(1, 4),
                ),
            ]
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("1 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="2"):
            self.A.array = [None] * self.t + [
                (RegionTuple(2, 20),),
                (RegionTuple(2, 8), RegionTuple(4, 4), RegionTuple(2, 8)),
                (RegionTuple(2, 8), RegionTuple(4, 4), RegionTuple(2, 8)),
                (RegionTuple(2, 8), RegionTuple(4, 4), RegionTuple(2, 8)),
                (
                    RegionTuple(2, 4),
                    RegionTuple(4, 4),
                    RegionTuple(2, 4),
                    RegionTuple(4, 2),
                    RegionTuple(2, 4),
                ),
            ]
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("3 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="4"):
            self.A.array = [None] * self.t + [
                (RegionTuple(4, 20),),
                (RegionTuple(4, 8), RegionTuple(8, 4), RegionTuple(4, 8)),
                (RegionTuple(4, 8), RegionTuple(8, 4), RegionTuple(4, 8)),
                (RegionTuple(4, 8), RegionTuple(8, 4), RegionTuple(4, 8)),
                (RegionTuple(4, 8), RegionTuple(8, 4), RegionTuple(4, 8)),
                (RegionTuple(4, 8), RegionTuple(8, 4), RegionTuple(4, 8)),
                (RegionTuple(4, 8), RegionTuple(8, 4), RegionTuple(4, 8)),
                (RegionTuple(4, 8), RegionTuple(8, 4), RegionTuple(4, 8)),
                (
                    RegionTuple(4, 4),
                    RegionTuple(8, 4),
                    RegionTuple(4, 4),
                    RegionTuple(8, 2),
                    RegionTuple(4, 4),
                ),
            ]
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("7 time steps later", errout)

    def test_same_x_too_close_f_c_f(self):
        """Test for fine-coarse-fine cusps at same x too close."""
        # fine - coarse - fine cusp transitions
        # these are not strictly enforceable, but nice to have
        # for no particular reason

        with self.subTest(scale="1"):
            self.A.array = [None] * self.t + [
                (RegionTuple(1, 20),),
                (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
                (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
                (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
                (RegionTuple(1, 20),),
            ]
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("3 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="2"):
            self.A.array = (
                [None] * self.t
                + [(RegionTuple(1, 5), RegionTuple(2, 20))]
                + [
                    (
                        RegionTuple(1, 5),
                        RegionTuple(2, 8),
                        RegionTuple(4, 2),
                        RegionTuple(2, 8),
                    )
                ]
                * 7
                + [(RegionTuple(1, 5), RegionTuple(2, 20))]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("7 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="4"):
            self.A.array = (
                [None] * self.t
                + [(RegionTuple(2, 5), RegionTuple(4, 20))]
                + [
                    (
                        RegionTuple(2, 5),
                        RegionTuple(4, 8),
                        RegionTuple(8, 2),
                        RegionTuple(4, 8),
                    )
                ]
                * 15
                + [(RegionTuple(2, 5), RegionTuple(4, 20))]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("15 time steps later", errout)

    def test_same_x_too_close_c_f_c(self):
        """Test for coarse-fine-coarse cusps at same x too close."""
        # coarse - fine - coarse transition
        # typical center of collision lattices

        with self.subTest(scale="1"):
            self.A.array = [None] * self.t + [
                (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
                (RegionTuple(1, 20),),
                (RegionTuple(1, 20),),
                (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
            ]
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("2 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="2"):
            self.A.array = (
                [None] * self.t
                + [
                    (
                        RegionTuple(1, 5),
                        RegionTuple(2, 8),
                        RegionTuple(4, 2),
                        RegionTuple(2, 8),
                    )
                ]
                + [(RegionTuple(1, 5), RegionTuple(2, 20))] * 7
                + [
                    (
                        RegionTuple(1, 5),
                        RegionTuple(2, 8),
                        RegionTuple(4, 2),
                        RegionTuple(2, 8),
                    )
                ]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("7 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="4"):
            self.A.array = (
                [None] * self.t
                + [
                    (
                        RegionTuple(2, 5),
                        RegionTuple(4, 8),
                        RegionTuple(8, 2),
                        RegionTuple(4, 8),
                    )
                ]
                + [(RegionTuple(2, 5), RegionTuple(4, 20))] * 15
                + [
                    (
                        RegionTuple(2, 5),
                        RegionTuple(4, 8),
                        RegionTuple(8, 2),
                        RegionTuple(4, 8),
                    )
                ]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("15 time steps later", errout)

    def test_same_x_too_close_invalid(self):
        """Test if invalid cusps at same x are skipped."""

        # covers both zip_longest fill value and invalid lattices
        self.A.array = [None] * self.t + [
            (RegionTuple(1, 20),),
            (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
            (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
            (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
            (
                RegionTuple(1, 8),
                RegionTuple(2, 2),
                RegionTuple(1, 8),
                RegionTuple(-1, -1),
                RegionTuple(-1, -1),
            ),
        ]
        self.assertTrue(
            self.V.test_time_slices(
                self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
            )
        )
        self.assertFalse(self.errout.getvalue(), msg="output not empty")

    def test_same_x_too_close_other_insert(self):
        """Test if other types of inserts at same x are skipped."""

        # next detected reg num change is something else
        self.A.array = [None] * self.t + [
            (RegionTuple(1, 20),),
            (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
            (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
            (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
            (
                RegionTuple(1, 8),
                RegionTuple(2, 2),
                RegionTuple(1, 8),
                RegionTuple(-1, -1),
            ),
        ]
        self.assertTrue(
            self.V.test_time_slices(
                self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
            )
        )
        self.assertFalse(self.errout.getvalue(), msg="output not empty")

    def test_other_x_close_f_c_f(self):
        """Test for fine-coarse-fine cusps closer than 4 at other x."""
        # next cusp is at different x and closer than 4 time slices
        # this lattice is invalid
        with self.subTest(scale="1"):
            self.A.array = [None] * self.t + [
                (RegionTuple(1, 20),),
                (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
                (RegionTuple(1, 6), RegionTuple(2, 2), RegionTuple(1, 10)),
                (RegionTuple(1, 20),),
            ]
            self.assertTrue(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            self.assertFalse(self.errout.getvalue(), msg="output not empty")

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            self.A.array = [None] * self.t + [
                (RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)),
                (RegionTuple(1, 20),),
                (RegionTuple(1, 20),),
                (RegionTuple(1, 6), RegionTuple(2, 2), RegionTuple(1, 10)),
            ]
            self.assertTrue(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            self.assertFalse(self.errout.getvalue(), msg="output not empty")

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="2"):
            self.A.array = (
                [None] * self.t
                + [(RegionTuple(2, 20),)]
                + [(RegionTuple(2, 8), RegionTuple(4, 2), RegionTuple(2, 8))] * 5
                + [(RegionTuple(2, 6), RegionTuple(4, 2), RegionTuple(2, 10))]
                + [(RegionTuple(2, 20),)]
            )
            self.assertTrue(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            self.assertFalse(self.errout.getvalue(), msg="output not empty")

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            self.A.array = (
                [None] * self.t
                + [(RegionTuple(2, 8), RegionTuple(4, 2), RegionTuple(2, 8))]
                + [(RegionTuple(2, 20),)] * 6
                + [(RegionTuple(2, 6), RegionTuple(4, 2), RegionTuple(2, 10))]
            )
            self.assertTrue(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            self.assertFalse(self.errout.getvalue(), msg="output not empty")

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="4"):
            self.A.array = (
                [None] * self.t
                + [(RegionTuple(4, 20),)]
                + [(RegionTuple(4, 8), RegionTuple(8, 2), RegionTuple(4, 8))] * 14
                + [(RegionTuple(4, 6), RegionTuple(8, 2), RegionTuple(4, 10))]
                + [(RegionTuple(4, 20),)]
            )
            self.assertTrue(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            self.assertFalse(self.errout.getvalue(), msg="output not empty")

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            self.A.array = (
                [None] * self.t
                + [(RegionTuple(4, 8), RegionTuple(8, 2), RegionTuple(4, 8))]
                + [(RegionTuple(4, 20),)] * 15
                + [(RegionTuple(4, 6), RegionTuple(8, 2), RegionTuple(4, 10))]
            )
            self.assertTrue(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            self.assertFalse(self.errout.getvalue(), msg="output not empty")

    def test_same_x_same_type_invalid(self):
        """Test if invalid inserts at same x are skipped."""
        # covers both zip_longest fill value and invalid lattices
        self.A.array = (
            [None] * self.t
            + [(RegionTuple(1, 20),)]
            + [(RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8))] * 6
            + [
                (
                    RegionTuple(1, 8),
                    RegionTuple(2, 2),
                    RegionTuple(1, 8),
                    RegionTuple(-1, -1),
                    RegionTuple(-1, -1),
                )
            ]
        )
        self.assertTrue(
            self.V.test_time_slices(
                self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
            )
        )
        self.assertFalse(self.errout.getvalue(), msg="output not empty")

    def test_same_x_same_type_other_insert(self):
        """Test if some other insert at same x are skipped."""
        # next detected reg num change is something else
        self.A.array = (
            [None] * self.t
            + [(RegionTuple(1, 20),)]
            + [(RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8))] * 6
            + [
                (
                    RegionTuple(1, 8),
                    RegionTuple(2, 2),
                    RegionTuple(1, 8),
                    RegionTuple(-1, -1),
                )
            ]
        )
        self.assertTrue(
            self.V.test_time_slices(
                self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
            )
        )
        self.assertFalse(self.errout.getvalue(), msg="output not empty")

    def test_same_x_same_type_too_close_f_c_c(self):
        """Test for fine-coarse-coarse same type cusp at same x."""
        # same type of cups at same x position too close in t
        # fine - coarse - coarser
        with self.subTest(scale="1"):
            self.A.array = (
                [None] * self.t
                + [(RegionTuple(1, 28),)]
                + [(RegionTuple(1, 12), RegionTuple(2, 2), RegionTuple(1, 12))] * 3
                + [
                    (RegionTuple(1, 10), RegionTuple(2, 4), RegionTuple(1, 10)),
                    (RegionTuple(1, 8), RegionTuple(2, 6), RegionTuple(1, 8)),
                    (RegionTuple(1, 6), RegionTuple(2, 8), RegionTuple(1, 6)),
                    (
                        RegionTuple(1, 4),
                        RegionTuple(2, 3),
                        RegionTuple(4, 2),
                        RegionTuple(2, 3),
                        RegionTuple(1, 4),
                    ),
                ]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("6 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="2"):
            self.A.array = (
                [None] * self.t
                + [(RegionTuple(2, 28),)]
                + [(RegionTuple(2, 12), RegionTuple(4, 2), RegionTuple(2, 12))] * 10
                + [
                    (RegionTuple(2, 10), RegionTuple(4, 4), RegionTuple(2, 10)),
                    (RegionTuple(2, 8), RegionTuple(4, 6), RegionTuple(2, 8)),
                    (RegionTuple(2, 6), RegionTuple(4, 8), RegionTuple(2, 6)),
                    (
                        RegionTuple(2, 4),
                        RegionTuple(4, 3),
                        RegionTuple(8, 2),
                        RegionTuple(4, 3),
                        RegionTuple(2, 4),
                    ),
                ]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("13 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="4"):
            self.A.array = (
                [None] * self.t
                + [(RegionTuple(4, 28),)]
                + [(RegionTuple(4, 12), RegionTuple(8, 2), RegionTuple(4, 12))] * 24
                + [
                    (RegionTuple(4, 10), RegionTuple(8, 4), RegionTuple(4, 10)),
                    (RegionTuple(4, 8), RegionTuple(8, 6), RegionTuple(4, 8)),
                    (RegionTuple(4, 6), RegionTuple(8, 8), RegionTuple(4, 6)),
                    (
                        RegionTuple(4, 4),
                        RegionTuple(8, 3),
                        RegionTuple(16, 2),
                        RegionTuple(8, 3),
                        RegionTuple(4, 4),
                    ),
                ]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("27 time steps later", errout)

    def test_same_x_same_type_too_close_c_f_c(self):
        """Test for coarse-fine-coarse same type cusp at same x."""
        # coarse - fine - finer
        # arrays include first reg to cover another distinct branch
        # for calculating the x pos of the cusp
        with self.subTest(scale="2"):
            self.A.array = (
                [None] * self.t
                + [
                    (
                        RegionTuple(1, 5),
                        RegionTuple(2, 4),
                        RegionTuple(4, 3),
                        RegionTuple(8, 2),
                        RegionTuple(4, 3),
                        RegionTuple(2, 4),
                    ),
                    (
                        RegionTuple(1, 5),
                        RegionTuple(2, 6),
                        RegionTuple(4, 8),
                        RegionTuple(2, 6),
                    ),
                    (
                        RegionTuple(1, 5),
                        RegionTuple(2, 8),
                        RegionTuple(4, 6),
                        RegionTuple(2, 8),
                    ),
                    (
                        RegionTuple(1, 5),
                        RegionTuple(2, 10),
                        RegionTuple(4, 4),
                        RegionTuple(2, 10),
                    ),
                ]
                + [
                    (
                        RegionTuple(1, 5),
                        RegionTuple(2, 12),
                        RegionTuple(4, 2),
                        RegionTuple(2, 12),
                    )
                ]
                * 24
                + [(RegionTuple(1, 5), RegionTuple(2, 28))]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("27 time steps later", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(scale="4"):
            self.A.array = (
                [None] * self.t
                + [
                    (
                        RegionTuple(4, 4),
                        RegionTuple(8, 3),
                        RegionTuple(16, 2),
                        RegionTuple(8, 3),
                        RegionTuple(4, 4),
                    ),
                    (RegionTuple(4, 6), RegionTuple(8, 8), RegionTuple(4, 6)),
                    (RegionTuple(4, 8), RegionTuple(8, 6), RegionTuple(4, 8)),
                    (RegionTuple(4, 10), RegionTuple(8, 4), RegionTuple(4, 10)),
                ]
                + [(RegionTuple(4, 12), RegionTuple(8, 2), RegionTuple(4, 12))] * 52
                + [(RegionTuple(4, 28),)]
            )
            self.assertFalse(
                self.V.test_time_slices(
                    self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
                )
            )
            errout = self.errout.getvalue()
            self.assert_errout_commons("FAIL", errout)
            self.assertIn("55 time steps later", errout)

    def test_other_x_same_type_close(self):
        """Test for cusps closer than limit at other x passing."""
        # to close in time slices, but at other x position
        self.A.array = (
            [None] * self.t
            + [(RegionTuple(2, 28),)]
            + [(RegionTuple(2, 12), RegionTuple(4, 2), RegionTuple(2, 12))] * 7
            + [
                (RegionTuple(2, 10), RegionTuple(4, 4), RegionTuple(2, 10)),
                (RegionTuple(2, 8), RegionTuple(4, 6), RegionTuple(2, 8)),
                (RegionTuple(2, 6), RegionTuple(4, 8), RegionTuple(2, 6)),
                (
                    RegionTuple(2, 2),
                    RegionTuple(4, 2),
                    RegionTuple(8, 2),
                    RegionTuple(4, 4),
                    RegionTuple(2, 6),
                ),
            ]
        )
        self.assertTrue(
            self.V.test_time_slices(
                self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
            )
        )
        self.assertFalse(self.errout.getvalue(), msg="output not empty")

    def test_same_x_other_type_close(self):
        """Test for cusps closer than limit at other x passing."""
        # because cusp type is different, it can be closer in t
        self.A.array = (
            [None] * self.t
            + [(RegionTuple(2, 28),)]
            + [(RegionTuple(2, 12), RegionTuple(4, 2), RegionTuple(2, 12))] * 10  # >=8
            + [(RegionTuple(2, 28),)]
        )
        self.assertTrue(
            self.V.test_time_slices(
                self.t, self.A.array[self.t : self.t + 2], stderr=self.errout
            )
        )
        self.assertFalse(self.errout.getvalue(), msg="output not empty")


if __name__ == "__main__":
    unittest.main()
