"""Setup with validation test functions for LatticeArray proximity."""

import unittest
from io import StringIO
from unittest.mock import MagicMock

from mrlattice.lattice.generate import RegionTuple
from mrlattice.lattice.validate import ValidateLattice


class EdgePromxitityTests(unittest.TestCase):
    """Common setup for all edge proximity tests."""

    def setUp(self):
        self.errout = StringIO()
        self.A = MagicMock()
        self.A.PROPERTY_DESCRIPTIONS = {"dummy": "a dummy property"}
        self.A.coarse_steps.value = 2
        self.t = 13
        self.V = ValidateLattice(lattice=self.A, sorted_props=["dummy"], increments=[1])


class TestValidateFCDiagLatticeEdgeProximTests(EdgePromxitityTests):
    """Test ValidateLattice tests for diag lattice edge proximity."""

    def setUp(self):
        super().setUp()
        self.V.test_diag_border_f_c_proxim = True

    def test_f_c_coa_too_narrow(self):
        """Test fine-to-coarse with coarsest reg too narrow."""
        tss = (
            [
                [
                    RegionTuple(4, 1),
                    RegionTuple(2, 10),
                    RegionTuple(1, 5),
                    RegionTuple(2, 10),
                    RegionTuple(1, 5),
                ],
                [
                    RegionTuple(4, 1),
                    RegionTuple(2, 10),
                    RegionTuple(1, 5),
                    RegionTuple(2, 10),
                    RegionTuple(1, 5),
                ],
            ],
            [
                [RegionTuple(4, 1), RegionTuple(1, 5)],
                [RegionTuple(4, 1), RegionTuple(1, 5)],
            ],
        )
        for ts, config in zip(tss, ("with coarsening regs", "no coarsening regs")):
            with self.subTest(config=config):
                self.assertFalse(
                    self.V.test_time_slices(self.t, ts, stderr=self.errout)
                )
                errout = self.errout.getvalue()
                self.assertIn(f"first region {ts[0][0]} of time slice {self.t}", errout)
                self.assertIn("with coarsest resolution '4'", errout)
                self.assertIn("1<2", errout)
                self.assertIn("FAIL: test_diag_border_f_c_proxim", errout)

                # reset stringIO
                self.errout.truncate(0)
                self.errout.seek(0)

                # now flipped time slice
                for reg in ts:
                    reg.reverse()

                self.assertFalse(
                    self.V.test_time_slices(self.t, ts, stderr=self.errout)
                )
                errout = self.errout.getvalue()
                self.assertIn(f"last region {ts[0][-1]} of time slice {self.t}", errout)
                self.assertIn("with coarsest resolution '4'", errout)
                self.assertIn("1<2", errout)
                self.assertIn("FAIL: test_diag_border_f_c_proxim", errout)

                # reset stringIO
                self.errout.truncate(0)
                self.errout.seek(0)

    def test_f_c_fin_too_narrow(self):
        """Test fine-to-coarse with finest reg too narrow."""
        tss = (
            [
                [
                    RegionTuple(4, 4),
                    RegionTuple(2, 10),
                    RegionTuple(1, 5),
                    RegionTuple(2, 10),
                    RegionTuple(1, 1),
                ],
                [
                    RegionTuple(4, 4),
                    RegionTuple(2, 10),
                    RegionTuple(1, 5),
                    RegionTuple(2, 10),
                    RegionTuple(1, 1),
                ],
            ],
            [
                [RegionTuple(4, 4), RegionTuple(1, 1)],
                [RegionTuple(4, 4), RegionTuple(1, 1)],
            ],
        )
        for ts, config in zip(tss, ("with coarsening regs", "no coarsening regs")):
            with self.subTest(config=config):
                self.assertFalse(
                    self.V.test_time_slices(self.t, ts, stderr=self.errout)
                )
                errout = self.errout.getvalue()
                self.assertIn(f"last region {ts[0][-1]} of time slice {self.t}", errout)
                self.assertIn("with finest resolution '1'", errout)
                self.assertIn("1<3", errout)
                self.assertIn("FAIL: test_diag_border_f_c_proxim", errout)

                # reset stringIO
                self.errout.truncate(0)
                self.errout.seek(0)

                # now flipped time slice
                for reg in ts:
                    reg.reverse()

                self.assertFalse(
                    self.V.test_time_slices(self.t, ts, stderr=self.errout)
                )
                errout = self.errout.getvalue()
                self.assertIn(f"first region {ts[0][0]} of time slice {self.t}", errout)
                self.assertIn("with finest resolution '1'", errout)
                self.assertIn("1<3", errout)
                self.assertIn("FAIL: test_diag_border_f_c_proxim", errout)

                # reset stringIO
                self.errout.truncate(0)
                self.errout.seek(0)

    def test_f_c_coa_regs_too_narrow(self):
        """Test fine-to-coarse with coarsening regs too narrow."""
        ts = [
            [
                RegionTuple(4, 4),
                RegionTuple(2, 2),
                RegionTuple(1, 5),
                RegionTuple(2, 10),
                RegionTuple(1, 5),
            ],
            [
                RegionTuple(4, 4),
                RegionTuple(2, 2),
                RegionTuple(1, 5),
                RegionTuple(2, 10),
                RegionTuple(1, 5),
            ],
        ]
        with self.subTest(config="with coarsening regs"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"num. 1: {RegionTuple(2, 2)} of time slice {self.t}", errout)
            self.assertIn("2<4", errout)
            self.assertIn("FAIL: test_diag_border_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            # now flipped time slice
            for reg in ts:
                reg.reverse()

            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"num. 3: {RegionTuple(2, 2)} of time slice {self.t}", errout)
            self.assertIn("2<4", errout)
            self.assertIn("FAIL: test_diag_border_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

        ts = [
            [RegionTuple(4, 4), RegionTuple(2, 2), RegionTuple(1, 5)],
            [RegionTuple(4, 4), RegionTuple(2, 2), RegionTuple(1, 5)],
        ]

        with self.subTest(config="no coarsening regs"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"num. 1: {RegionTuple(2, 2)} of time slice {self.t}", errout)
            self.assertIn("2<4", errout)
            self.assertIn("FAIL: test_diag_border_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            # now flipped time slice
            for reg in ts:
                reg.reverse()

            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"num. 1: {RegionTuple(2, 2)} of time slice {self.t}", errout)
            self.assertIn("2<4", errout)
            self.assertIn("FAIL: test_diag_border_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

    def test_f_c_no_coa_regs(self):
        """Test fine-to-coarse with no coarsening regs."""
        ts = [
            (RegionTuple(4, 4), RegionTuple(1, 5)),
            (RegionTuple(4, 4), RegionTuple(1, 5)),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))

    def test_f_c_coa_wrong_place(self):
        """Test fine-to-coarse with coarsest reg not first/last."""
        ts = [
            (RegionTuple(2, 10), RegionTuple(4, 8), RegionTuple(2, 5)),
            (RegionTuple(2, 10), RegionTuple(4, 8), RegionTuple(2, 5)),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn("SKIP: test_diag_border_f_c_proxim", errout)
        self.assertIn(
            f"first region {RegionTuple(2, 10)} of time slice {self.t}", errout
        )
        self.assertIn("coarsest (=4)", errout)

    def test_f_c_fin_wrong_place(self):
        """Test fine-to-coarse with finest reg not first/last."""
        ts = [
            (RegionTuple(2, 10), RegionTuple(1, 16), RegionTuple(2, 5)),
            (RegionTuple(2, 10), RegionTuple(1, 16), RegionTuple(2, 5)),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn("SKIP: test_diag_border_f_c_proxim", errout)
        self.assertIn(
            f"first region {RegionTuple(2, 10)} of time slice {self.t}", errout
        )
        self.assertIn("coarsest (=4)", errout)


class TestValidateCFDiagLatticeEdgeProximTests(EdgePromxitityTests):
    """Test ValidateLattice tests for diag lattice edge proximity."""

    def setUp(self):
        super().setUp()
        self.V.test_diag_border_c_f_proxim = True

    def test_c_f_coa_pass(self):
        """Test fine-to-coarse coarsest always passing."""
        ts = [
            [RegionTuple(4, 1), RegionTuple(1, 10)],
            [RegionTuple(4, 1), RegionTuple(1, 10)],
        ]

        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        # now flipped time slice
        for reg in ts:
            reg.reverse()

        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))

    def test_c_f_fin_too_narrow(self):
        """Test coarse-to-fine with finest reg too narrow."""
        tss = (
            [
                [
                    RegionTuple(4, 4),
                    RegionTuple(2, 10),
                    RegionTuple(1, 10),
                    RegionTuple(2, 10),
                    RegionTuple(1, 1),
                ],
                [
                    RegionTuple(4, 4),
                    RegionTuple(2, 10),
                    RegionTuple(1, 10),
                    RegionTuple(2, 10),
                    RegionTuple(1, 1),
                ],
            ],
            [
                [RegionTuple(4, 4), RegionTuple(1, 1)],
                [RegionTuple(4, 4), RegionTuple(1, 1)],
            ],
        )
        for ts, config in zip(tss, ("with coarsening regs", "no coarsening regs")):
            with self.subTest(config=config):
                self.assertFalse(
                    self.V.test_time_slices(self.t, ts, stderr=self.errout)
                )
                errout = self.errout.getvalue()
                self.assertIn(f"last region {ts[0][-1]} of time slice {self.t}", errout)
                self.assertIn("with finest resolution '1'", errout)
                self.assertIn("1<7", errout)
                self.assertIn("FAIL: test_diag_border_c_f_proxim", errout)

                # reset stringIO
                self.errout.truncate(0)
                self.errout.seek(0)

                # now flipped time slice
                for reg in ts:
                    reg.reverse()

                self.assertFalse(
                    self.V.test_time_slices(self.t, ts, stderr=self.errout)
                )
                errout = self.errout.getvalue()
                self.assertIn(f"first region {ts[0][0]} of time slice {self.t}", errout)
                self.assertIn("with finest resolution '1'", errout)
                self.assertIn("1<7", errout)
                self.assertIn("FAIL: test_diag_border_c_f_proxim", errout)

    def test_c_f_coa_regs_too_narrow(self):
        """Test coarse-to-fine with coarsening regs too narrow."""

        with self.subTest(location="off-center"):
            ts = [
                [
                    RegionTuple(4, 4),
                    RegionTuple(2, 2),
                    RegionTuple(1, 10),
                    RegionTuple(2, 10),
                    RegionTuple(1, 10),
                ],
                [
                    RegionTuple(4, 4),
                    RegionTuple(2, 2),
                    RegionTuple(1, 10),
                    RegionTuple(2, 10),
                    RegionTuple(1, 10),
                ],
            ]
            with self.subTest(config="with coarsening regs"):
                self.assertFalse(
                    self.V.test_time_slices(self.t, ts, stderr=self.errout)
                )
                errout = self.errout.getvalue()
                self.assertIn(
                    f"num. 1: {RegionTuple(2, 2)} of time slice {self.t}", errout
                )
                self.assertIn("2<6", errout)
                self.assertIn("FAIL: test_diag_border_c_f_proxim", errout)

                # reset stringIO
                self.errout.truncate(0)
                self.errout.seek(0)

                # now flipped time slice
                for reg in ts:
                    reg.reverse()

                self.assertFalse(
                    self.V.test_time_slices(self.t, ts, stderr=self.errout)
                )
                errout = self.errout.getvalue()
                self.assertIn(
                    f"num. 3: {RegionTuple(2, 2)} of time slice {self.t}", errout
                )
                self.assertIn("2<6", errout)
                self.assertIn("FAIL: test_diag_border_c_f_proxim", errout)

                # reset stringIO
                self.errout.truncate(0)
                self.errout.seek(0)

        with self.subTest(location="centered"):
            ts = [
                [RegionTuple(4, 4), RegionTuple(2, 2), RegionTuple(1, 10)],
                [RegionTuple(4, 4), RegionTuple(2, 2), RegionTuple(1, 10)],
            ]

            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"num. 1: {RegionTuple(2, 2)} of time slice {self.t}", errout)
            self.assertIn("2<6", errout)
            self.assertIn("FAIL: test_diag_border_c_f_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            # now flipped time slice
            for reg in ts:
                reg.reverse()

            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"num. 1: {RegionTuple(2, 2)} of time slice {self.t}", errout)
            self.assertIn("2<6", errout)
            self.assertIn("FAIL: test_diag_border_c_f_proxim", errout)

    def test_c_f_no_coa_regs(self):
        """Test coarse-to-fine with no coarsening regs."""
        ts = [
            (RegionTuple(4, 4), RegionTuple(1, 10)),
            (RegionTuple(4, 4), RegionTuple(1, 10)),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))

    def test_c_f_coa_wrong_place(self):
        """Test coarse-to-fine with coarsest reg not first/last."""
        ts = [
            (RegionTuple(2, 10), RegionTuple(4, 8), RegionTuple(2, 5)),
            (RegionTuple(2, 10), RegionTuple(4, 8), RegionTuple(2, 5)),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn("SKIP: test_diag_border_c_f_proxim", errout)
        self.assertIn(
            f"first region {RegionTuple(2, 10)} of time slice {self.t}", errout
        )
        self.assertIn("coarsest (=4)", errout)

    def test_c_f_fin_wrong_place(self):
        """Test coarse-to-fine with finest reg not first/last."""
        ts = [
            (RegionTuple(2, 10), RegionTuple(1, 16), RegionTuple(2, 5)),
            (RegionTuple(2, 10), RegionTuple(1, 16), RegionTuple(2, 5)),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn("SKIP: test_diag_border_c_f_proxim", errout)
        self.assertIn(
            f"first region {RegionTuple(2, 10)} of time slice {self.t}", errout
        )
        self.assertIn("coarsest (=4)", errout)


class TestValidateFCCuspEdgeProximTests(EdgePromxitityTests):
    """ValidateLattice tests for fine-to-coarse cusp edge proximity."""

    def setUp(self):
        super().setUp()
        self.V.test_cusp_f_c_proxim = True

    def test_res_1_2(self):
        """Test if fine(1)-to-coarse(2) cusp proximity to t=0 is ok."""
        self.t = 0
        ts = [
            [RegionTuple(1, 20)],
            [RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)],
        ]
        with self.subTest(cusp="centered"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("0 and 1 is 1", errout)
            self.assertIn("FAIL: test_cusp_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

        ts = [
            [RegionTuple(1, 20)],
            [RegionTuple(1, 4), RegionTuple(2, 2), RegionTuple(1, 12)],
        ]
        with self.subTest(cusp="off-center"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("0 and 1 is 1", errout)
            self.assertIn("FAIL: test_cusp_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            for regs in ts:
                regs.reverse()

            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("0 and 1 is 1", errout)
            self.assertIn("FAIL: test_cusp_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

    def test_res_4_8(self):
        """Test if fine(4)-to-coarse(8) cusp proximity to t=0 is ok."""
        self.t = 1
        ts = [
            [RegionTuple(2, 10), RegionTuple(4, 20), RegionTuple(2, 10)],
            [
                RegionTuple(2, 10),
                RegionTuple(4, 8),
                RegionTuple(8, 2),
                RegionTuple(4, 8),
                RegionTuple(2, 10),
            ],
        ]
        with self.subTest(cusp="centered"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 6", errout)
            self.assertIn("FAIL: test_cusp_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

        ts = [
            [RegionTuple(2, 4), RegionTuple(4, 20), RegionTuple(2, 16)],
            [
                RegionTuple(2, 4),
                RegionTuple(4, 8),
                RegionTuple(8, 2),
                RegionTuple(4, 8),
                RegionTuple(2, 16),
            ],
        ]
        with self.subTest(cusp="off-center"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 6", errout)
            self.assertIn("FAIL: test_cusp_f_c_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            for regs in ts:
                regs.reverse()

            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 6", errout)
            self.assertIn("FAIL: test_cusp_f_c_proxim", errout)

    def test_invalid_regs(self):
        """Test if invalid regs are caught."""
        self.t = 1
        ts = [
            (RegionTuple(1, 10),),
            (RegionTuple(1, 10), RegionTuple(-1, -1), RegionTuple(-1, -1)),
        ]
        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn("1 or 2", errout)
        self.assertIn("[RegionTuple(a=-1, n=-1), RegionTuple(a=-1, n=-1)]", errout)
        self.assertIn("FAIL: test_cusp_f_c_proxim", errout)

    def test_no_cusp(self):
        """Test if diag borders lattice without cusps passes."""
        ts = [
            (RegionTuple(1, 30), RegionTuple(2, 10), RegionTuple(1, 30)),
            (RegionTuple(1, 28), RegionTuple(2, 10), RegionTuple(1, 32)),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        self.assertFalse(self.errout.getvalue(), msg="stderr not empty")

    def test_other_insert(self):
        """Test if other type of insert passes."""
        self.t = 1
        ts = [
            (RegionTuple(1, 30), RegionTuple(2, 10), RegionTuple(1, 30)),
            (
                RegionTuple(1, 28),
                RegionTuple(2, 4),
                RegionTuple(4, 2),
                RegionTuple(2, 4),
            ),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn("1 and 2", errout)
        self.assertIn("SKIP: test_cusp_f_c_proxim", errout)
        self.assertIn("cusp couldn't be identified", errout)


class TestValidateCFCuspEdgeProximTests(EdgePromxitityTests):
    """ValidateLattice tests for coarse-to-fine cusp edge proximity."""

    def setUp(self):
        super().setUp()
        self.V.test_cusp_c_f_proxim = True
        self.t = 1

    def test_res_2_1(self):
        """Test if coarse(2)-to-fine(1) cusp proximity to t=0 is ok."""
        ts = [
            [RegionTuple(1, 8), RegionTuple(2, 2), RegionTuple(1, 8)],
            [RegionTuple(1, 20)],
        ]
        with self.subTest(cusp="centered"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 2", errout)
            self.assertIn("FAIL: test_cusp_c_f_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

        ts = [
            [RegionTuple(1, 4), RegionTuple(2, 2), RegionTuple(1, 12)],
            [RegionTuple(1, 20)],
        ]
        with self.subTest(cusp="off-center"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 2", errout)
            self.assertIn("FAIL: test_cusp_c_f_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            for regs in ts:
                regs.reverse()

            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 2", errout)
            self.assertIn("FAIL: test_cusp_c_f_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

    def test_res_8_4(self):
        """Test if coarse(8)-to-fine(4) cusp proximity to t=0 is ok."""
        ts = [
            [
                RegionTuple(2, 10),
                RegionTuple(4, 8),
                RegionTuple(8, 2),
                RegionTuple(4, 8),
                RegionTuple(2, 10),
            ],
            [RegionTuple(2, 10), RegionTuple(4, 20), RegionTuple(2, 10)],
        ]
        with self.subTest(cusp="centered"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 14", errout)
            self.assertIn("FAIL: test_cusp_c_f_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

        ts = [
            [
                RegionTuple(2, 4),
                RegionTuple(4, 8),
                RegionTuple(8, 2),
                RegionTuple(4, 8),
                RegionTuple(2, 16),
            ],
            [RegionTuple(2, 4), RegionTuple(4, 20), RegionTuple(2, 16)],
        ]
        with self.subTest(cusp="off-center"):
            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 14", errout)
            self.assertIn("FAIL: test_cusp_c_f_proxim", errout)

            # reset stringIO
            self.errout.truncate(0)
            self.errout.seek(0)

            for regs in ts:
                regs.reverse()

            self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn("1 and 2 is 14", errout)
            self.assertIn("FAIL: test_cusp_c_f_proxim", errout)

    def test_invalid_regs(self):
        """Test if invalid regs are caught."""
        ts = [
            (RegionTuple(1, 10), RegionTuple(-1, -1), RegionTuple(-1, -1)),
            (RegionTuple(1, 10),),
        ]
        self.assertFalse(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn("1 or 2", errout)
        self.assertIn("[RegionTuple(a=-1, n=-1), RegionTuple(a=-1, n=-1)]", errout)
        self.assertIn("FAIL: test_cusp_c_f_proxim", errout)

    def test_no_cusp(self):
        """Test if diag borders lattice without cusps passes."""
        ts = [
            (RegionTuple(1, 30), RegionTuple(2, 10), RegionTuple(1, 30)),
            (RegionTuple(1, 28), RegionTuple(2, 10), RegionTuple(1, 32)),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        self.assertFalse(self.errout.getvalue(), msg="stderr not empty")

    def test_other_insert(self):
        """Test if other type of insert passes."""
        ts = [
            (RegionTuple(1, 30), RegionTuple(2, 10), RegionTuple(1, 30)),
            (
                RegionTuple(1, 28),
                RegionTuple(2, 4),
                RegionTuple(4, 2),
                RegionTuple(2, 4),
            ),
        ]
        self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn("1 and 2", errout)
        self.assertIn("cusp couldn't be identified", errout)
        self.assertIn("SKIP: test_cusp_c_f_proxim", errout)


if __name__ == "__main__":
    unittest.main()
