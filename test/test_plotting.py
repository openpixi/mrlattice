"""Test setup for the `plotting` module and IPython widgets.

The test cases for `plotting` are:
- TBD: some details

The test case for the IPython widgets is `TestWidgets`. It executes the
IPython kernel on the `testing.ipynb` notebook in a subprocess.
"""

from os import curdir, path
import subprocess
import sys
import unittest

# from mrlattice import plotting


class TestPlotting(unittest.TestCase):
    """Test."""


class TestWidgets(unittest.TestCase):
    """Test custom IPython widget integration."""

    def test_widgets(self):
        """Run IPython with testing.ipynb notebook in subprocess."""

        try:
            subprocess.check_output(
                [
                    path.join(sys.exec_prefix, path.join("bin", "ipython")),
                    path.join("test", "testing.ipynb"),
                ],
                stderr=subprocess.STDOUT,
                text=True,
                env={
                    "COVERAGE_PROCESS_START": path.abspath(
                        path.join(curdir, "setup.cfg")
                    )
                },
            )
        except subprocess.CalledProcessError as e:
            self.fail(f"\nIPython execution Failed:\n{e.output}")


if __name__ == "__main__":
    unittest.main()
