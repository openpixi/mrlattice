"""Setup with validation test functions for LatticeArray parameters."""

import unittest
from io import StringIO
from unittest.mock import MagicMock

from mrlattice.lattice.configure import LatticeArrayScheme
from mrlattice.lattice.generate import RegionTuple
from mrlattice.lattice.validate import ValidateLattice


class TestValidateLatticeParameterTests(unittest.TestCase):
    """Test if ValidateLattice tests for parameters are working."""

    def setUp(self):
        self.errout = StringIO()
        self.A = MagicMock()
        self.A.PROPERTY_DESCRIPTIONS = {"dummy": "a dummy property"}
        self.A.coarse_steps.value = 3
        self.A.buffer.value = 1
        self.A.finest_width.value = 8
        self.A.borders_width.value = 5
        self.ts = [
            (RegionTuple(8, 2), RegionTuple(4, 6), RegionTuple(2, 6), RegionTuple(1, 8))
        ]
        self.t = 13
        self.V = ValidateLattice(lattice=self.A, sorted_props=["dummy"], increments=[1])

    def test_buffer_size(self):
        """Test if wrong min. buffer size is caught."""

        self.V.test_buffer_size = True

        with self.subTest(msg="no buffer region"):
            # skip first region
            self.assertTrue(
                self.V.test_time_slices(self.t, [self.ts[0][1:]], stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn(f"buffer ({2**self.A.coarse_steps.value})", errout)
            self.assertIn("SKIP: test_buffer_size", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(msg="buffer region with wrong spatial size"):
            self.A.buffer.value = 4
            self.assertFalse(
                self.V.test_time_slices(self.t, self.ts, stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn(f"{self.ts[0][0]} is not >= {self.A.buffer.value}", errout)
            self.assertIn("FAIL: test_buffer_size", errout)

    def test_finest_width(self):
        """Test if wrong finest_width is caught."""

        self.V.test_finest_width = True

        with self.subTest(msg="no finest region"):
            # skip last region
            self.assertTrue(
                self.V.test_time_slices(self.t, [self.ts[0][:-1]], stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn(f"{self.ts[0][:-1]}", errout)
            self.assertIn("SKIP: test_finest_width", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(msg="finest region with wrong spatial size"):
            self.A.finest_width.value = 10
            self.assertFalse(
                self.V.test_time_slices(self.t, self.ts, stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn(
                f"{self.ts[0][-1]} is not >= {self.A.finest_width.value}", errout
            )
            self.assertIn("FAIL: test_finest_width", errout)

    def test_coarse_steps(self):
        """Test if wrong number of borders is caught."""

        self.V.test_coarse_steps = True
        self.A.coarse_steps.value = 12

        self.assertFalse(self.V.test_time_slices(self.t, self.ts, stderr=self.errout))
        errout = self.errout.getvalue()
        self.assertIn(f"slice {self.t}", errout)
        self.assertIn(f"{len(self.ts[0])-1} is not {self.A.coarse_steps.value}", errout)
        self.assertIn("FAIL: test_coarse_steps", errout)

    def test_borders_width_fixed_width_scheme(self):
        """Test for wrong borders width in fixed width scheme."""

        self.V.test_borders_width = True
        self.A.scheme.name = LatticeArrayScheme.FIXED_WIDTH_SCHEME
        self.A.scheme.FIXED_WIDTH_SCHEME = LatticeArrayScheme.FIXED_WIDTH_SCHEME

        with self.subTest(reason="too small"):
            self.A.borders_width.value = 100
            self.assertFalse(
                self.V.test_time_slices(self.t, self.ts, stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn("region number 1", errout)
            self.assertIn(f"requested width of {self.A.borders_width.value}", errout)
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn("FAIL: test_borders_width", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(reason="too wide"):
            self.A.borders_width.value = 2
            self.assertFalse(
                self.V.test_time_slices(self.t, self.ts, stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn("region number 1", errout)
            self.assertIn(f"requested width of {self.A.borders_width.value}", errout)
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn("FAIL: test_borders_width", errout)

    def test_borders_width_fixed_number_scheme(self):
        """Test for wrong borders width in fixed number scheme."""

        self.V.test_borders_width = True
        self.A.scheme.name = LatticeArrayScheme.FIXED_NUMBER_SCHEME
        self.A.scheme.FIXED_NUMBER_SCHEME = LatticeArrayScheme.FIXED_NUMBER_SCHEME

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(reason="too wide"):
            self.A.borders_width.value = 4
            self.assertFalse(
                self.V.test_time_slices(self.t, self.ts, stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn("region number 1", errout)
            self.assertIn(f"requested number of {self.A.borders_width.value}", errout)
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn("FAIL: test_borders_width", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(reason="too small"):
            self.A.borders_width.value = 30
            self.assertFalse(
                self.V.test_time_slices(self.t, self.ts, stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn("region number 1", errout)
            self.assertIn(f"requested number of {self.A.borders_width.value}", errout)
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn("FAIL: test_borders_width", errout)

    def test_borders_width_general(self):
        """Test for wrong borders width in fixed width scheme."""

        self.V.test_borders_width = True

        with self.subTest(msg="no coarsening regions"):
            coa = 2**self.A.coarse_steps.value
            # only finest and coarsest
            ts = [
                (
                    RegionTuple(1, 10),
                    RegionTuple(coa, 10),
                    RegionTuple(1, 10),
                    RegionTuple(coa, 10),
                )
            ]
            self.assertTrue(self.V.test_time_slices(self.t, ts, stderr=self.errout))
            errout = self.errout.getvalue()
            self.assertIn(f"slice {self.t}", errout)
            self.assertIn(f"{ts[0]}", errout)
            self.assertIn("test_borders_width", errout)

        # reset stringIO
        self.errout.truncate(0)
        self.errout.seek(0)

        with self.subTest(msg="invalid scheme"):
            self.A.scheme.name = "noexist"
            self.assertTrue(
                self.V.test_time_slices(self.t, self.ts, stderr=self.errout)
            )
            errout = self.errout.getvalue()
            self.assertIn(f"scheme: {self.A.scheme.name}", errout)
            self.assertIn("SKIP: test_borders_width", errout)


if __name__ == "__main__":
    unittest.main()
