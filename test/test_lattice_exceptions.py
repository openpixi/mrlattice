"""Test setup to verify functionality of the `lattice.validate` module.

Test Cases are:
- TestMainModule
"""
import unittest
from io import StringIO
from unittest.mock import patch

from mrlattice.lattice.exceptions import main


class TestMainModule(unittest.TestCase):
    """Test execution as main module."""

    @patch("sys.stderr", new_callable=StringIO)
    def test_main(self, mock_print):
        """Test if repr output contains args."""

        main()
        # format output
        outputs = mock_print.getvalue().rstrip("\n\n").split("\n\n")
        for repr_out in outputs:
            if "ConfigurationError" in repr_out:
                self.assertIn("Test error message.", repr_out)
                self.assertIn("property object", repr_out)

            elif "LatticeGenerationError" in repr_out:
                self.assertIn("Test error message.", repr_out)
                self.assertIn("property object", repr_out)

            elif "ModifierHistoryError" in repr_out:
                self.assertIn("Test error message.", repr_out)
                self.assertIn("step=6", repr_out)
                self.assertIn("test history", repr_out)

            elif "SkipValidation" in repr_out:
                self.assertIn("Test error message.", repr_out)
                self.assertIn("function name", repr_out)

            elif "ValidationError" in repr_out:
                self.assertIn("Test error message.", repr_out)
                self.assertIn("function name", repr_out)

            elif "VolatilePropertyError" in repr_out:
                self.assertIn("object object", repr_out)
                self.assertIn("`test_prop` property", repr_out)

            else:
                raise Exception(f"Output from unknown Error class:\n{repr_out}")


if __name__ == "__main__":
    unittest.main()
