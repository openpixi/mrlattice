"""Test setup to verify `mrlattice` package setup and install.

This unittest module executes a modified duplicate of itself as a
subprocess to ensure that a fresh interpreter is used for the tests.
"""

import fileinput
import os
import shutil
import subprocess
import tempfile
import unittest


class TempCopyOfFile:
    """Copy active file to temp file and return closed file-like obj."""

    # pylint: disable=consider-using-with

    def __init__(self):
        """Create context manager."""
        self.temp_file = None

    def __enter__(self):
        """Enter context manager."""
        self.temp_file = tempfile.NamedTemporaryFile(suffix=".py", delete=False)
        with open(os.path.abspath(__file__), "r+b") as src:
            shutil.copyfileobj(src, self.temp_file)

        self.temp_file.close()
        return self.temp_file

    def __exit__(self, exc_type, exc_value, traceback):
        """Exit context manager."""
        os.remove(self.temp_file.name)


class SkipTest(unittest.TestCase):
    """Child classes skip all tests."""

    @classmethod
    def setUpClass(cls):
        cls.skipTest(cls, f"{cls} not in subprocess")


class TestPackageRunner(unittest.TestCase):
    """Launch TestPackage in subprocess."""

    def test_start_runner(self):
        """Set up test file and launch subprocess."""

        # create temp file
        with TempCopyOfFile() as temp_test_package:

            # activate TestPackage by changing superclass
            with fileinput.FileInput(temp_test_package.name, inplace=True) as file:
                for line in file:
                    if "TestPackageRunner(unittest.TestCase)" in line:
                        print(
                            line.replace(
                                "TestPackageRunner(unittest.TestCase)",
                                "TestPackageRunner(SkipTest)",
                            ),
                            end="",
                        )
                    elif "TestPackage(SkipTest)" in line:
                        print(
                            line.replace(
                                "TestPackage(SkipTest)",
                                "TestPackage(unittest.TestCase)",
                            ),
                            end="",
                        )
                    else:
                        print(line, end="")

            # launch subprocess
            try:
                print(
                    "\nTestPackage Results:\n",
                    subprocess.check_output(
                        [
                            "python",
                            temp_test_package.name,
                        ],
                        stderr=subprocess.STDOUT,
                        text=True,
                    ),
                    flush=True,
                )
            except subprocess.CalledProcessError as e:
                self.fail(f"\nTestPackage Failed:\n{e.output}")


class TestPackage(SkipTest):
    """Verify module structure and package installation.

    This TestCase will always skip. `TestPackageRunner` will change
    superclass to `unittest.TestCase` and execute tests in subprocess.
    Tests are ordered! Important for import statements.
    """

    # pylint: disable=import-outside-toplevel,invalid-name

    def assertSetIsDisjoint(self, first: set, second: set, msg=None):
        """Convenience function to test if sets are disjoint."""

        self.assertTrue(first.isdisjoint(second), msg=msg)

    def assertSetIsSubset(self, subset: set, superset: set, msg=None):
        """Convenience function to test if first is subset of second."""

        self.assertTrue(subset.issubset(superset), msg=msg)

    def test_1_version_string(self):
        """Test if version string is correct.

        Compare tuples instead of `__version__` string.
        """
        import mrlattice

        # test for version types
        self.assertIsInstance(
            mrlattice.__version__, str, msg="__version__ attribute is not str!"
        )
        self.assertIsInstance(
            mrlattice.VERSION, tuple, msg="VERSION attribute is not tuple!"
        )
        # check tuple equality
        version = tuple(map(int, mrlattice.__version__.split(sep=".")))
        self.assertEqual(
            version, mrlattice.VERSION, "__version__ and VERSION do not match!"
        )

    def test_2_mrlattice_import(self):
        """Test mrlattice import of submodules."""

        members = {"plotting", "lattice"}

        import mrlattice

        self.assertSetIsDisjoint(
            members, set(dir(mrlattice)), msg="base import has submodules"
        )
        self.assertSetEqual(members, set(mrlattice.__all__))

    def test_3_lattice_subpackage(self):
        """Test if lattice subpackage and its modules are accessible."""

        lattice_members = {"configure", "exceptions", "generate", "validate"}
        generate_members = {
            "RegionTuple",
            "RegionSlice",
            "LatticeArray",
            "SuperimposeLatticeArray",
            "HomogeneousLatticeArray",
            "PulseLatticeArray",
            "SuperimposePulseLatticeArray",
        }

        from mrlattice import lattice

        self.assertSetIsSubset(lattice_members, set(dir(lattice)))
        self.assertSetEqual(lattice_members, set(lattice.__all__))

        self.assertSetIsSubset(generate_members, set(dir(lattice.generate)))
        self.assertSetEqual(generate_members, set(lattice.generate.__all__))

    def test_4_submodules_import(self):
        """Test if submodules are accessible."""
        try:
            import mrlattice
            from mrlattice import lattice, plotting
            from mrlattice.lattice import configure, generate, validate
            from mrlattice.lattice.generate import advanced, default

        except ImportError as e:
            self.fail(f"importing submodules failed:\nImportError: {e}")

        self.assertTrue(hasattr(mrlattice, "__doc__"))
        self.assertTrue(hasattr(lattice, "__doc__"))
        self.assertTrue(hasattr(configure, "__doc__"))
        self.assertTrue(hasattr(generate, "__doc__"))
        self.assertTrue(hasattr(default, "__doc__"))
        self.assertTrue(hasattr(advanced, "__doc__"))
        self.assertTrue(hasattr(validate, "__doc__"))
        self.assertTrue(hasattr(plotting, "__doc__"))


if __name__ == "__main__":
    unittest.main()
