"""Test setup to verify functionality of the `lattice.configure` module.

Test cases are:
- TestLatticeArrayProperties
- TestLatticeArrayScheme

All derived from a custom `DefaultTestCase` class that inherits from
`unittest.TestCase`. Changes are a new test method for testing
`hasattr()`.
"""

import unittest
from unittest.mock import patch, MagicMock

from mrlattice.lattice.configure import LatticeArrayProperty, LatticeArrayScheme
from mrlattice.lattice.generate import LatticeArray
from mrlattice.lattice.exceptions import ConfigurationError


class DefaultTestCase(unittest.TestCase):
    """Extends the default `TestCase` with attribute testing."""

    # pylint: disable=invalid-name

    def assertHasAttr(self, obj, attr):
        """Fail if `hasattr(obj, attr)` is false."""
        testBool = hasattr(obj, attr)
        self.assertTrue(testBool, msg=f"{obj} does not have attribute {attr}.")


class TestLatticeArrayProperties(DefaultTestCase):
    """Test `LatticeArrayProperty` properties."""

    # pylint: disable=protected-access

    def setUp(self):
        # tests where new instances are created delete self.A, self.P
        self.A = LatticeArray(scheme=LatticeArrayScheme.FIXED_WIDTH_SCHEME)
        self.P = self.A.coarse_steps

    def test_has_config_properties(self):
        """Test if `LatticeArray` instance has config properties."""

        for prop, prop_val in self.A.PROPERTY_DESCRIPTIONS.items():
            with self.subTest(prop=prop):
                self.assertHasAttr(self.A, prop)
                self.assertIsInstance(getattr(self.A, prop), LatticeArrayProperty)
                self.assertIs(getattr(self.A, prop)._lattice_array, self.A)
                self.assertEqual(getattr(self.A, prop).description, prop_val)
                with self.assertRaises(
                    AttributeError, msg=f"self.A.{prop} is writable"
                ):
                    setattr(self.A, prop, None)
                with self.assertRaises(
                    AttributeError, msg=f"self.A.{prop} is deletable"
                ):
                    delattr(self.A, prop)

    def test_config_init(self):
        """Test constructor."""

        with self.subTest(msg="manual direct init"):
            A = LatticeArray()

            with self.assertRaises(
                ConfigurationError, msg="init new prop without name"
            ):
                LatticeArrayProperty(
                    lattice=A,
                    description="does not exist yet",
                )

            with self.assertRaises(TypeError, msg="init new prop with invalid name"):
                LatticeArrayProperty(
                    lattice=A, description="does not exist yet", name=42
                )

            P = LatticeArrayProperty(
                lattice=A,
                description="does not exist yet",
                name="new_prop",
            )
            self.assertHasAttr(A, "new_prop")
            self.assertIs(A.new_prop, P)
            self.assertEqual(P.description, "does not exist yet")

        with self.subTest(msg="manual init, lattice already has prop"):
            with self.assertRaises(AssertionError):
                LatticeArrayProperty(
                    lattice=LatticeArray(),
                    description=LatticeArray.PROPERTY_DESCRIPTIONS["repeat"],
                    name="new_name",
                )

    def test_config_property_value_setter(self):
        """Test `LatticeArrayProperty` instance's value setter."""

        # test after initialization
        with self.subTest(msg="setting prop value"):
            self.P.value = 10

        with self.subTest(msg="changed limits"):
            self.P._min = 5
            self.P._step = 2
            with self.assertRaises(ValueError, msg="setting below min possible"):
                self.P.value = 3
            with self.assertRaises(ValueError, msg="stepping not enforced"):
                self.P.value = 6
            # should pass
            self.P.value = 9
            self.P.value = self.P._min

        with self.subTest(msg="test float values"):
            # this could work, if min and step are also float and match
            with self.assertRaises(ValueError, msg="float values possible"):
                self.P.value = 3.14

    def test_config_property_setter_calls_update(self):
        """Test if `.value` setter calls update on `LatticeArray`."""

        with patch.object(self.A, "update_properties") as mock_method:
            self.P.value = 10
            mock_method.assert_called_once_with()

    def test_config_property_bounds(self):
        """Test if bounds update propagates to value correctly."""

        with self.subTest(msg="value below min"):
            self.P._min = 5
            self.P._check_bounds_and_update()  # value => 5
            self.assertEqual(self.P.value, 5)
            self.P._check_bounds_and_update()  # value == 5 == min
            self.assertEqual(self.P.value, 5)
        with self.subTest(msg="value above min but wrong"):
            # close to min == 5
            self.P._value = 7
            self.P._step = 3
            self.P._check_bounds_and_update()  # value => 5
            self.assertEqual(self.P.value, 5)
            self.P._check_bounds_and_update()  # value == 5
            self.assertEqual(self.P.value, 5)
            # far from min
            self.P._value = 27
            self.P._min = 4
            self.P._step = 11
            self.P._check_bounds_and_update()  # value => 26
            self.assertEqual(self.P.value, 26)
            self.P._check_bounds_and_update()  # value == 26
            self.assertEqual(self.P.value, 26)

    def test_config_property_increment(self):
        """Test if `.increment()` and `.decrement()` work."""

        # some init
        self.P._min = 11
        self.P._step = 3
        self.P._value = 11
        with self.subTest(msg="property increment"):
            self.assertEqual(self.P.value, 11)
            self.P.increment()  # value == 14
            self.assertEqual(self.P.value, 14)
            self.P.increment(num=5)  # value == 29
            self.assertEqual(self.P.value, 29)
        with self.subTest(msg="property decrement"):
            self.P.decrement()  # value == 26
            self.assertEqual(self.P.value, 26)
            self.P.decrement(num=4)  # value == 14
            self.assertEqual(self.P.value, 14)
            with self.assertRaises(ValueError, msg="decrement below min possible"):
                self.P.decrement(num=4)  # value == 2 < min

    def test_config_property_repr(self):
        """Test if `repr()` returns correct values in string."""

        repr_out = repr(self.P)
        self.assertIn(str(self.P.description), repr_out)
        self.assertIn(str(hex(id(self.P))), repr_out)


class TestLatticeArrayScheme(DefaultTestCase):
    """Test `LatticeArrayScheme` properties."""

    # pylint: disable=protected-access

    def setUp(self):
        # tests where new instances are created delete self.A, self.S
        self.A = LatticeArray()
        self.S = self.A.scheme

    def test_has_scheme_property(self):
        """Test if `LatticeArray` instance has `scheme` property."""

        self.assertHasAttr(self.A, "scheme")
        self.assertIsInstance(self.A.scheme, LatticeArrayScheme)
        self.assertIs(self.A.scheme._lattice_array, self.A)
        self.assertEqual(
            self.A.scheme.description,
            LatticeArrayScheme.SCHEME_DESCRIPTIONS[self.A.scheme.name],
        )

        with self.assertRaises(AttributeError, msg="A.scheme is writable"):
            self.A.scheme = None
        with self.assertRaises(AttributeError, msg="A.scheme is deletable"):
            del self.A.scheme
        with self.assertRaises(AttributeError, msg="A.scheme.description is writable"):
            self.A.scheme.description = None
        with self.assertRaises(AttributeError, msg="A.scheme.description is deletable"):
            del self.A.scheme.description

    def test_scheme_init(self):
        """Test constructor."""

        with self.subTest(msg="manual direct init"):
            A = LatticeArray()

            with self.assertRaises(
                AssertionError, msg="binding to lattice with existing scheme"
            ):
                LatticeArrayScheme(lattice=A)

    def test_scheme_property_change(self):
        """Test changing scheme on `LatticeArrayScheme`."""

        with self.subTest(msg="setting fixed_number scheme by method"):
            self.A.scheme.set_fixed_number()
            self.assertEqual(self.A.scheme.name, self.A.scheme.FIXED_NUMBER_SCHEME)
            self.assertIn(
                "Number of Cells", self.A.PROPERTY_DESCRIPTIONS["borders_width"]
            )
            self.assertIn("Number of Cells", self.A.borders_width.description)
            # repeat
            self.A.scheme.set_fixed_number()
            self.assertEqual(self.A.scheme.name, self.A.scheme.FIXED_NUMBER_SCHEME)
            self.assertIn(
                "Number of Cells", self.A.PROPERTY_DESCRIPTIONS["borders_width"]
            )
            self.assertIn("Number of Cells", self.A.borders_width.description)

        with self.subTest(msg="setting fixed_width scheme by method"):
            self.A.scheme.set_fixed_width()
            self.assertEqual(self.A.scheme.name, self.A.scheme.FIXED_WIDTH_SCHEME)
            self.assertIn("Finest Units", self.A.PROPERTY_DESCRIPTIONS["borders_width"])
            self.assertIn("Finest Units", self.A.borders_width.description)
            # repeat
            self.A.scheme.set_fixed_width()
            self.assertEqual(self.A.scheme.name, self.A.scheme.FIXED_WIDTH_SCHEME)
            self.assertIn("Finest Units", self.A.PROPERTY_DESCRIPTIONS["borders_width"])
            self.assertIn("Finest Units", self.A.borders_width.description)

        with self.subTest(msg="setting fixed_number scheme by name"):
            self.A.scheme.name = LatticeArrayScheme.FIXED_NUMBER_SCHEME
            self.assertEqual(self.A.scheme.name, self.A.scheme.FIXED_NUMBER_SCHEME)
            self.assertIn(
                "Number of Cells", self.A.PROPERTY_DESCRIPTIONS["borders_width"]
            )
            self.assertIn("Number of Cells", self.A.borders_width.description)
            # repeat
            self.A.scheme.name = LatticeArrayScheme.FIXED_NUMBER_SCHEME
            self.assertEqual(self.A.scheme.name, self.A.scheme.FIXED_NUMBER_SCHEME)
            self.assertIn(
                "Number of Cells", self.A.PROPERTY_DESCRIPTIONS["borders_width"]
            )
            self.assertIn("Number of Cells", self.A.borders_width.description)

        with self.subTest(msg="setting fixed_width scheme by name"):
            self.A.scheme.name = LatticeArrayScheme.FIXED_WIDTH_SCHEME
            self.assertEqual(self.A.scheme.name, self.A.scheme.FIXED_WIDTH_SCHEME)
            self.assertIn("Finest Units", self.A.PROPERTY_DESCRIPTIONS["borders_width"])
            self.assertIn("Finest Units", self.A.borders_width.description)
            # repeat
            self.A.scheme.name = LatticeArrayScheme.FIXED_WIDTH_SCHEME
            self.assertEqual(self.A.scheme.name, self.A.scheme.FIXED_WIDTH_SCHEME)
            self.assertIn("Finest Units", self.A.PROPERTY_DESCRIPTIONS["borders_width"])
            self.assertIn("Finest Units", self.A.borders_width.description)

    def test_uninitialized_scheme(self):
        """Test if scheme instance without scheme raises errors."""

        with self.subTest(msg="check initialization"):
            self.assertEqual(self.S.name, None)
            self.assertEqual(self.S.description, "scheme not set")

        with self.subTest(msg="invalid scheme"):
            self.S._scheme = "invalid"
            self.assertEqual(self.S.name, "invalid")
            with self.assertRaises(KeyError):
                _ = self.S.description

        # fix the scheme
        self.S.set_fixed_number()
        with self.subTest(msg="after fixing scheme"):
            self.assertEqual(self.S.name, self.S.FIXED_NUMBER_SCHEME)
            self.assertEqual(
                self.S.description,
                self.S.SCHEME_DESCRIPTIONS[self.S.FIXED_NUMBER_SCHEME],
            )

    def test_scheme_name_setter(self):
        """Test if setting scheme name triggers correct methods."""

        with patch.object(self.S, "_set_scheme") as mock_width:
            # configure mock
            mock_width[self.S.FIXED_WIDTH_SCHEME] = MagicMock()
            # test
            self.S.name = self.S.FIXED_WIDTH_SCHEME
            mock_width.__getitem__.assert_called_once_with(self.S.FIXED_WIDTH_SCHEME)
            mock_width[self.S.FIXED_WIDTH_SCHEME].assert_called_once_with()

        with patch.object(self.S, "_set_scheme") as mock_number:
            # configure mock
            mock_number[self.S.FIXED_NUMBER_SCHEME] = MagicMock()
            # test
            self.S.name = self.S.FIXED_NUMBER_SCHEME
            mock_number.__getitem__.assert_called_once_with(self.S.FIXED_NUMBER_SCHEME)
            mock_number[self.S.FIXED_NUMBER_SCHEME].assert_called_once_with()

        with self.assertRaises(ConfigurationError, msg="S.name setter check fails"):
            self.S.name = None

    def test_scheme_property_change_calls_update(self):
        """Test if scheme change calls update on `LatticeArray`."""

        with patch.object(self.A, "update_properties") as mock_method:
            self.S.set_fixed_number()
            mock_method.assert_called_once_with()

    def test_scheme_property_repr(self):
        """Test if `repr()` returns correct values in string."""

        repr_out = repr(self.S)
        self.assertIn(str(self.S._lattice_array), repr_out)
        self.assertIn(str(hex(id(self.S))), repr_out)


if __name__ == "__main__":
    unittest.main()
