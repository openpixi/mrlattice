"""Configure, create and validate spacetime lattices.

Provides the modules:
- configure:
    Helper classes for configuring `LatticeArray`.
    Defines:
        - `LatticeArrayProperty`
        - `LatticeArrayScheme`
- generate:
    `LatticeArray` and classes inheriting from it.
- validate:
    Test suite to validate `LatticeArray`.
"""

# make submodules available from mrlattice.lattice

from mrlattice.lattice import configure, exceptions, generate, validate


__all__ = ["configure", "exceptions", "generate", "validate"]
