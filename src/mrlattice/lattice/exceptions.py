"""Error types for `LatticeArray`.

- ConfigurationError
- LatticeGenerationError
- ModifierHistoryError
- SkipValidation
- ValidationError
- VolatilePropertyError

Execution as main module:
If executed as the main module, a series of output is printed that
represents the error messages generated by the exceptions defined here.
"""
# remove after py 3.11
from __future__ import annotations

import traceback
import typing

# prevent circular import
if typing.TYPE_CHECKING:
    from mrlattice.lattice.generate import LatticeArray


class ConfigurationError(Exception):
    """Exception raised for invalid configurations.

    Attributes:
        message: Explanation of the error.
        prop: Property that caused the exception.
    """

    def __init__(self, message, prop):
        """Return `ConfigurationError` instance.

        Args:
            message: Explanation of the error.
            prop: Property that caused the exception.
        """
        super().__init__(message, prop)
        self.message = message
        self.prop = prop
        self.args = (f"{self.prop} is misconfigured!\n{self.message}\n", message, prop)

    def __str__(self) -> str:
        """Error message."""
        return self.args[0]


class LatticeGenerationError(Exception):
    """Exception raised for errors during lattice generation.

    Attributes:
        message: Explanation of the error.
        prop: Property that caused the exception.
    """

    def __init__(self, message, prop):
        """Return `ConfigurationError` instance.

        Args:
            message: Explanation of the error.
            prop: Property that caused the exception.
        """
        super().__init__(message, prop)
        self.prop = prop
        self.message = message
        self.args = (f"{self.prop} is misconfigured!\n{self.message}\n", message, prop)

    def __str__(self) -> str:
        """Error message."""
        return self.args[0]


class ModifierHistoryError(Exception):
    """Exception raised when replaying the history fails.

    Attributes:
        message: Explanation of the error.
        step: Step of the modifier history that caused the exception.
        modifier_history: Backup of full modifier history.
    """

    def __init__(self, message, step: int, modifier_history):
        """Return `ModifierHistoryError` instance.

        Args:
            message: Explanation of the error.
            step: Index of step in history that caused the error.
            modifier_history: Backup of full modifier history.
        """
        super().__init__(message, step, modifier_history)
        self.message = message
        self.step = step
        self.modifier_history = modifier_history
        self.args = (
            f"History replay failed for {step=}:\n{modifier_history[step]}\n"
            f"of full history:\n{modifier_history}\n{message}\n",
            step,
            modifier_history,
        )

    def __str__(self) -> str:
        """Error message."""
        return self.args[0]


class SkipValidation(Exception):
    """Exception raised for skipping validation test functions.

    Attributes:
        reason: Explanation for the skip reason.
        test: Reference to raising validation test function.
    """

    def __init__(self, reason: str, test: str):
        """Return `SkipValidation` instance.

        Args:
            reason: Explanation for the skip reason.
            test: Name of the validation test function.
        """
        super().__init__(reason, test)
        self.reason = reason
        self.test = test
        self.args = (
            f"The validation test {test} was skipped because of:\n{reason}\n",
            reason,
            test,
        )

    def __str__(self) -> str:
        """Error message."""
        return self.args[0]


class ValidationError(Exception):
    """Exception raised when validating a lattice array fails.

    Attributes:
        message: Explanation of the error.
        test: Name of test that caused exception.
    """

    def __init__(self, message, test):
        """Return `ValidationError` instance.

        Args:
            message: Explanation of the error
            test: Name of test or function that failed validation.
        """
        super().__init__(message, test)
        self.test = test
        self.message = message
        self.args = (f"Test {test} failed validation!\n{self.message}\n", test)

    def __str__(self) -> str:
        """Error message."""
        return self.args[0]


class VolatilePropertyError(Exception):
    """Exception raised for volatile property access before config.

    This exception is raised, when volatile properties of `LatticeArray`
    instances are accessed before they were fully configured or
    regenerated after the configuration parameters changed.

    Attributes:
        lattice: `LatticeArray` instance raising the error.
        message: Explanation of the error.
    """

    def __init__(self, lat: LatticeArray, prop: str):
        """Return `VolatilePropertyError` instance.

        Args:
            lat: `LatticeArray` instance raising the error.
            prop: String with name of property.
        """
        super().__init__(lat, prop)
        self.lattice = lat
        self.message = (
            f"`{prop}` property of instance {self.lattice} accessed before "
            "configuration.\nIf this happens after changing the "
            "configuration of an existing instance make sure to call "
            "`LatticeArray.generate_array` before access.\n"
        )
        self.args = (
            self.message,
            lat,
            prop,
        )

    def __str__(self) -> str:
        """Error message."""
        return self.message


def main():
    """Run when called as main module."""
    m = "Test error message."
    p = property()
    o = object()

    for error in (
        ConfigurationError(message=m, prop=p),
        LatticeGenerationError(message=m, prop=p),
        ModifierHistoryError(message=m, step=6, modifier_history="test history"),
        SkipValidation(reason=m, test="function name"),
        ValidationError(message=m, test="function name"),
        VolatilePropertyError(lat=o, prop="test_prop"),
    ):
        # pylint: disable=broad-except
        try:
            raise error
        except Exception:
            traceback.print_exc()


if __name__ == "__main__":
    main()
