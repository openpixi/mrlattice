"""Configuration properties for type `LatticeArray`.

Provides the classes:
- LatticeArrayScheme:
    Represents the scheme governing lattice coarsening.
- LatticeArrayProperty:
    Represents properties that describe the lattice.
"""
# remove after py 3.11
from __future__ import annotations

import typing

from mrlattice.lattice.exceptions import ConfigurationError

# prevent circular import
if typing.TYPE_CHECKING:
    from mrlattice.lattice.generate import LatticeArray


class LatticeArrayScheme:
    """A helper class to set the scheme for the coarsening regions."""

    # constants used to identify and describe schemes
    FIXED_WIDTH_SCHEME = "fixed_width"
    FIXED_NUMBER_SCHEME = "fixed_number"
    SCHEME_DESCRIPTIONS = {
        FIXED_WIDTH_SCHEME: "All Coarsening Regions Share Width [Finest Units]",
        FIXED_NUMBER_SCHEME: "All Coarsening Regions Share Number of Cells",
        None: "scheme not set",
    }

    def __init__(self, lattice: LatticeArray):
        """Initialize scheme bound to `lattice`.

        Not designed to be used outside of `LatticeArray.__init__`.

        Args:
            lattice: `LatticeArray` instance to bind to.

        Raises:
            AssertionError:
                If `lattice` already has a `.scheme` attribute that is
                not this instance, as a `LatticeArray` can only have
                one distinct scheme.
        """
        self._scheme = None
        self._set_scheme = {
            self.FIXED_WIDTH_SCHEME: self.set_fixed_width,
            self.FIXED_NUMBER_SCHEME: self.set_fixed_number,
        }
        # callback object
        self._lattice_array = lattice
        # validate integrity
        if hasattr(lattice, "scheme"):
            assert lattice.scheme is self

    def __repr__(self):
        """Return string representation of `LatticeArrayScheme`."""
        return (
            f"<LatticeArrayScheme(lattice={self._lattice_array})"
            f" at {hex(id(self))} with {self.name=}>"
        ).replace("self.", "")

    def set_fixed_width(self):
        """Change to fixed width scheme."""
        self._scheme = self.FIXED_WIDTH_SCHEME
        # change description
        self._lattice_array.borders_width.description = (
            self._lattice_array.borders_width.description.replace(
                "Number of Cells", "Finest Units"
            )
        )
        self._lattice_array.PROPERTY_DESCRIPTIONS[
            "borders_width"
        ] = self._lattice_array.borders_width.description
        # callback update of all properties
        self._lattice_array.update_properties()

    def set_fixed_number(self):
        """Change to fixed number scheme."""
        self._scheme = self.FIXED_NUMBER_SCHEME
        # change description
        self._lattice_array.borders_width.description = (
            self._lattice_array.borders_width.description.replace(
                "Finest Units", "Number of Cells"
            )
        )
        self._lattice_array.PROPERTY_DESCRIPTIONS[
            "borders_width"
        ] = self._lattice_array.borders_width.description
        # callback update of all properties
        self._lattice_array.update_properties()

    @property
    def name(self):
        """Name of this scheme."""
        return self._scheme

    @name.setter
    def name(self, other: str):
        try:
            self._set_scheme[other]()
        except KeyError as e:
            raise ConfigurationError(
                message=f"Cannot set {other} as scheme name. Unknown scheme!", prop=self
            ) from e

    @property
    def description(self):
        """Detailed text describing this scheme."""
        return self.SCHEME_DESCRIPTIONS[self.name]


class LatticeArrayProperty:
    """A helper class to manage `LatticeArray` properties."""

    def __init__(self, lattice: LatticeArray, description: str, name: str = None):
        """Initialize property bound to `lattice`.

        If `description` is unknown to `lattice`, then this instance
        will be added as an attribute with name `name` to `lattice`.
        Otherwise, raises AssertionError if `lattice` already has a
        property with `description` that is not this instance.

        Args:
            lattice:
                `LatticeArray` instance to bind to.
            description:
                String describing this instances use.
            name:
                String for setting attribute name of `lattice` for
                unknown properties.

        Raises:
            AssertionError:
                If `lattice` already has a property with the same
                description as `description` that is not this instance.
        """
        self.description = description
        self._value = 1
        self._min = 1
        self._step = 1
        # callback object
        self._lattice_array = lattice
        # get name of self as lattice attribute
        try:
            attr_key = tuple(lattice.PROPERTY_DESCRIPTIONS.values()).index(description)

        except ValueError:
            # pylint: disable=raise-missing-from
            # description not in lattice, add self as attribute 'name'
            if name is not None:
                setattr(lattice, name, self)
            else:
                raise ConfigurationError(
                    "`name` for property not given. Please supply a valid name.", self
                )

        else:
            attr = tuple(lattice.PROPERTY_DESCRIPTIONS.keys())[attr_key]
            if hasattr(lattice, attr):
                # validate integrity
                assertion_message = (
                    f"LatticeArray already has '{attr}'"
                    f" which differs from {name=}: {self}!"
                )
                assert getattr(lattice, attr) == self, assertion_message

    def __repr__(self):
        """Return string representation of `LatticeArrayProperty`."""
        return (
            f"<LatticeArrayProperty(lattice={self._lattice_array}, "
            f"description='{self.description}') at {hex(id(self))} "
            f"with {self.value=}, {self._min=}, {self._step=}>"
        ).replace("self.", "")

    @property
    def value(self):
        """Value of this property."""
        return self._value

    @value.setter
    def value(self, other):
        if (other - self._min) % self._step:
            raise ValueError(
                f"Setting `.value` to {other} is not allowed!\n"
                f"{other}-min(={self._min}) is not multiple of step(={self._step})."
            )
        if other < self._min:
            raise ValueError(
                f"Setting `value` to {other} is not allowed!\n"
                f"{other} is below min(={self._min})."
            )
        self._value = other
        # callback update of all properties
        self._lattice_array.update_properties()

    def _check_bounds_and_update(self):
        """Ensure valid value > min by changing value if necessary."""
        if self._value < self._min:
            self._value = self._min
        elif self._value > self._min:
            self._value -= (self._value - self._min) % self._step

    def increment(self, num: int = 1):
        """Increment `.value` by `num` times the current stepping.

        Args:
            num: optional. Number of times to increment. Default is 1.
        """
        self.value += self._step * num

    def decrement(self, num: int = 1):
        """Decrement `.value` by `num` times the current stepping.

        This is identical to `.increment(-1*num)` with a negative num.

        Args:
            num: optional. Number of times to decrement. Default is 1.
        """
        self.increment(-1 * num)
