"""Advanced spacetime lattice types extending `default.LatticeArray`.

Provides:
- HomogeneousLatticeArray
"""

import numpy as np
from mrlattice.lattice.configure import LatticeArrayScheme
from mrlattice.lattice.generate import (
    LatticeArray,
    RegionTuple,
    SuperimposeLatticeArray,
)


class HomogeneousLatticeArray(LatticeArray):
    """Generate homogeneous lattices.

    Extends super and repurposes the properties `buffer`, `coarse_steps`
    and `repeat`. Removes the properties` borders_width` and
    `finest_width`. Removes effect of `scheme` property.

    Attributes:
        PROPERTY_DESCRIPTIONS:
            Dict with keys equal to all instance attributes of type
            `LatticeArrayProperty` and vals equal to the init params of
            their `.description`.
        buffer:
            `LatticeArrayProperty`, used to set spatial dimension
        coarse_steps:
            `LatticeArrayProperty`, used to set scale of cells
        repeat:
            `LatticeArrayProperty`, used to set temporal dimension
        scheme:
            `LatticeArrayScheme`, not used
    """

    # pylint: disable=protected-access,attribute-defined-outside-init

    def __init__(
        self,
        coarse_steps=None,
        buffer=None,
        repeat=None,
        scheme=None,
    ):
        """Initializes instance configured with supplied arguments.

        Args:
            coarse_steps:
                int to set the scale of the cells as 2** `coarse_steps`.
            buffer:
                int to set the spatial dimension of the lattice.
            repeat:
                int to set the temporal dimension of the lattice.
            scheme:
                Not used.

        Raises:
            ConfigurationError:
                Invalid or no scheme set when also using optional args.
            ValueError:
                Args have invalid values.
        """
        # scheme does not matter here, but needs to be set
        sc = LatticeArrayScheme.FIXED_NUMBER_SCHEME
        super().__init__(scheme=sc, coarse_steps=1, buffer=buffer, repeat=repeat)
        # remove irrelevant config params
        del self.PROPERTY_DESCRIPTIONS["finest_width"]
        del self._finest_width
        del self.PROPERTY_DESCRIPTIONS["borders_width"]
        del self._borders_width
        # before modifying a class vairable make a copy to instance
        self.scheme.SCHEME_DESCRIPTIONS = dict(self.scheme.SCHEME_DESCRIPTIONS)
        del self.scheme.SCHEME_DESCRIPTIONS[LatticeArrayScheme.FIXED_WIDTH_SCHEME]
        # change descriptions to be meaningful for homogeneous lattices
        self.scheme.SCHEME_DESCRIPTIONS[sc] = "Has no Coarsening Regions"
        self.coarse_steps.description = (
            r"Scale of Cells: $2^{scale}$ [in Units of Finest]"
        )
        self.buffer.description = "Spatial Width [Number of Cells]"
        self.repeat.description = "Temporal Width [Number of Cells]"

        # fix hardcoded coarse_steps
        # allows to have resolution 1 = 2**0, so coarse_steps=0
        self.coarse_steps._min = 0
        self.coarse_steps._value = 0 if coarse_steps is None else coarse_steps

    def homogeneous_fill(self):
        """Homogeneous lattice with 2**`scale` resolution.

        This saves a lattice in `.array` with spatial width of `.buffer`
        times 2**`.coarse_steps` and temporal width of `.repeat`, where
        all cells are squares. The result is a homogeneous lattice.
        """
        spat_res = 2**self.coarse_steps.value
        self._array = np.empty(self.repeat.value * spat_res, dtype=object)
        self._array.fill((RegionTuple(spat_res, self.buffer.value),))

    def update_properties(self):
        """Override super to only reset volatile props."""
        self.reset_volatile_properties()

    def generate_array(self):
        """Override super with `homogeneous_fill`."""
        self.homogeneous_fill()
        self.update_volatile_properties()
        self.replay_modifier_history()


class PulseLatticeArray(LatticeArray):
    """A `LatticeArray` shaped as a pulse in the finest region."""

    # allow usage of protected LatticeArrayProperty and -Scheme members
    # pylint: disable=protected-access,attribute-defined-outside-init

    PROPERTY_DESCRIPTIONS = dict(LatticeArray.PROPERTY_DESCRIPTIONS)
    PROPERTY_DESCRIPTIONS.update(
        {
            "finest_width": "Width of Pulse Kernel [Finest Units]",
            "buffer": "Additional Width at Spatial Boundaries [Coarsest Units]",
        }
    )

    def _update_props_fixed_width(self):
        """Recalculate properties according to fixed width scheme."""
        # Kayran-Scheme

        # resolution of coarsest
        steps_power = 2**self.coarse_steps.value

        # configure borders_width
        self.borders_width._step = 2
        self.borders_width._min = steps_power * 3
        self.borders_width._check_bounds_and_update()

        # configure buffer
        self.buffer._min = 2
        self.buffer._check_bounds_and_update()

        # configure finest_width
        self.finest_width._min = 6
        self.finest_width._check_bounds_and_update()

        # configure repetitions
        self.repeat._min = 2
        self.repeat._check_bounds_and_update()

    def _update_props_fixed_number(self):
        """Recalculate properties according to fixed number scheme."""
        # Ipp-Scheme

        # configure borders_width
        self.borders_width._step = 1
        self.borders_width._min = 6
        self.borders_width._check_bounds_and_update()

        # configure buffer
        self.buffer._min = 2
        self.buffer._check_bounds_and_update()

        # configure finest_width
        self.finest_width._min = 6
        self.finest_width._check_bounds_and_update()

        # configure repetitions
        self.repeat._min = 2
        self.repeat._check_bounds_and_update()

    def _pulse_segment(self):
        # prepare self.array from spatially joined segments
        left = self.array
        right = np.copy(left)
        right = self.temporal_mirror_array(right, out=right)
        right = self.spatial_mirror_array(right, out=right)

        # only if width of finest region is lager than minimal value
        # add difference only to right_segment from the left
        if (
            add_fine_width := self.finest_width.value - 2**self.coarse_steps.value
        ) > 0:
            fine = np.empty(len(right), dtype=object)
            fine.fill((RegionTuple(1, add_fine_width),))
            right = self.spatial_extend_array(fine, right, out=right)

        # join and save
        self._array = self.spatial_extend_array(left, right, left)

    def _propagate_pulse_loop(self, out_t, out, coarsest_a, rbuffer):
        for t in range(out_t):
            # number of buffer cells
            q, r = divmod(t, coarsest_a)
            out[t] = (
                (RegionTuple(coarsest_a, int(self.buffer.value + q)),)
                + self.array[r]
                + (RegionTuple(coarsest_a, int(rbuffer - q)),)
            )

    def _propagate_pulse(self):
        # get resolution of boundaries of array in units
        # of finest mesh, times 2 for new border regions
        # NOTE: self.array must be symmetric
        coarsest_a = int(self.array[0][0].a * 2)
        out_t = int(self.repeat.value * len(self.array))
        out = np.empty(out_t, dtype=object)

        # buffer on the right
        rbuffer = self.buffer.value - 1 + self.repeat.value

        # calculate each time slice's regions
        self._propagate_pulse_loop(out_t, out, coarsest_a, rbuffer)

        # save output
        self._array = out

    def _propagate(self):
        # create pulse segment by extending mirrored self incl. kernel
        self._pulse_segment()
        # call to new propagate that propagates pulse correctly
        self._propagate_pulse()


class SuperimposePulseLatticeArray(PulseLatticeArray, SuperimposeLatticeArray):
    """Superimpose `PulseLatticeArray` with mirrored counterpart."""

    # allow usage of protected LatticeArrayProperty and -Scheme members
    # pylint: disable=protected-access

    def _update_props_fixed_width(self):
        """Recalculate properties according to fixed width scheme."""
        # Kayran-Scheme

        # resolution of coarsest
        steps_power = 2**self.coarse_steps.value
        # delta of coarsening region width min to max
        coa_sum = sum(2**coa for coa in range(1, self.coarse_steps.value))

        # configure borders_width
        self.borders_width._step = 2
        self.borders_width._min = steps_power * 3
        self.borders_width._check_bounds_and_update()

        # configure buffer
        self.buffer._min = 2
        self.buffer._check_bounds_and_update()

        # minimal width of coarsening regions
        regions_sum = sum(
            2**coa * (self.borders_width.value // 2**coa)
            for coa in range(1, self.coarse_steps.value)
        )

        # configure finest_width
        self.finest_width._step = steps_power
        self.finest_width._min = (
            steps_power - (regions_sum + coa_sum) % steps_power
        ) * 2 + coa_sum
        while self.finest_width._min < 6:
            self.finest_width._min += self.finest_width._step
        self.finest_width._check_bounds_and_update()

        # configure repetitions
        self.repeat._step = 2
        self.repeat._min = (
            2 * regions_sum + coa_sum + self.finest_width.value
        ) / steps_power + 5
        self.repeat._check_bounds_and_update()

    def _update_props_fixed_number(self):
        """Recalculate properties according to fixed number scheme."""
        # Ipp-Scheme

        # resolution of coarsest
        steps_power = 2**self.coarse_steps.value
        # delta of coarsening region width min to max
        coa_sum = sum(2**coa for coa in range(1, self.coarse_steps.value))

        # configure borders_width
        self.borders_width._step = 1
        self.borders_width._min = 6
        self.borders_width._check_bounds_and_update()

        # configure buffer
        self.buffer._min = 2
        self.buffer._check_bounds_and_update()

        # configure finest_width
        self.finest_width._step = steps_power
        self.finest_width._min = (
            steps_power - ((1 + self.borders_width.value) * coa_sum) % steps_power
        ) * 2 + coa_sum
        while self.finest_width._min < 6:
            self.finest_width._min += self.finest_width._step
        self.finest_width._check_bounds_and_update()

        # configure repetitions
        self.repeat._step = 2
        self.repeat._min = (
            2 * self.borders_width.value * coa_sum + coa_sum + self.finest_width.value
        ) / steps_power + 5
        self.repeat._check_bounds_and_update()

    def _propagate_pulse_loop(self, out_t, out, coarsest_a, rbuffer):
        # calculate each time slice's regions
        for t in range(0, out_t):
            # number of buffer cells
            q, r = divmod(t, coarsest_a)
            # save a segment
            tempA = (
                (
                    (RegionTuple(coarsest_a, int(self.buffer.value + q)),)
                    + self.array[r]
                    + (RegionTuple(coarsest_a, int(rbuffer - q)),)
                ),
            )
            # superimpose with mirrored segment
            out[t] = self.superimpose_array(
                tempA,
                self.spatial_mirror_array(tempA),
            )[0]
