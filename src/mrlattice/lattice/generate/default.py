"""`LatticeArray` and `SuperimposeLatticeArray` types.

Provides:
- RegionTuple(a,n): A namedtuple representing lattice regions.
- LatticeArray
- SuperimposeLatticeArray

A `LatticeArray` is an object that allows for the configuration and
generation of spacetime lattices.
"""

# required until py 3.11 for type(self) in methods
from __future__ import annotations

from itertools import chain
from dataclasses import dataclass
from typing import Any, Callable, TypeAlias

import numpy as np
import numpy.typing as npt

from mrlattice.lattice.configure import LatticeArrayProperty, LatticeArrayScheme
from mrlattice.lattice.exceptions import (
    ConfigurationError,
    LatticeGenerationError,
    ModifierHistoryError,
    VolatilePropertyError,
)


@dataclass(slots=True)
class RegionTuple:
    """(a[=res], n[=count]) container for lattice array regions."""

    a: int
    n: int


# "time slice" of a lattice array
RegionSlice: TypeAlias = tuple[RegionTuple, ...]


class LatticeArray:
    """Represent inhomogeneously resolved spacetime lattices.

    Per default the borders move in positive spatial direction. The
    resulting lattice is striped from the bottom left to the top right
    with regions coarsening to the left (negative spatial direction).

    All attr of custom type provide a dynamic `.description` property.

    Attributes:
        PROPERTY_DESCRIPTIONS:
            Dict with keys equal to all instance attributes of type
            `LatticeArrayProperty` and vals equal to the init params of
            their `.description`.
        borders_width:
            `LatticeArrayProperty`
        buffer:
            `LatticeArrayProperty`
        coarse_steps:
            `LatticeArrayProperty`
        finest_width:
            `LatticeArrayProperty`
        repeat:
            `LatticeArrayProperty`
        scheme:
            `LatticeArrayScheme`
        array:
            `np.ndarray` object encoding the current lattice. This is a
            1D Numpy array of `dtype=object`. It elements are nested
            tuples of varying size. Therefore `array` will be treated
            as having multiple dimensions and nested arrays.
        max_num_cells:
            Maximum number of cells per region for the entire lattice,
            i.e. across all time slices.
        resolution_list:
            Sorted list of all resolution present in the entire lattice.
        tx_dims:
            Tuple of (max_t, max_x) where max_t gives the number of time
            steps and max_x the spatial size of the lattice. Both values
            in finest units.

    A "lattice array" (`.array` property) has the following elements:
        1. Dim:
            `dtype=object` `np.ndarray` with tuples for each time slice.
        2. Dim:
            Each time slice has tuples for each region.
        3. Dim:
            Each region tuple is (temporal, spatial, num) [finest units]
            - temporal: Temporal resolution of this region.
            - spatial: Spatial resolution of this region.
            - num: Spatial width of the entire region.

    "Volatile Properties" are the properties:
    - array, - max_num_cells, - resolution_list, - tx_dims
    which get reset on any change to LatticeArrayProperties.
    """

    # pylint: disable=protected-access,attribute-defined-outside-init

    # key values are used for instance attribute names
    # of type `LatticeArrayProperties`
    # values used for `LatticeArrayProperties.description`
    # order of elements has to match update order in _update_props!
    PROPERTY_DESCRIPTIONS = {
        "coarse_steps": "Number of Borders between Coarsest and Finest",
        "borders_width": "Width per Coarsening Region [Finest Units]",
        "buffer": "Min. Width of Coarsest Region [Coarsest Units]",
        "finest_width": "Min. Width of Finest Region [Finest Units]",
        "repeat": "Number of Segment Repetitions",
    }

    def __init__(
        self,
        scheme: str = None,
        coarse_steps: int = None,
        borders_width: int = None,
        buffer: int = None,
        finest_width: int = None,
        repeat: int = None,
    ):
        """Initializes instance configured with supplied arguments.

        Args:
            scheme:
                A valid scheme constant from `LatticeArrayScheme` as
                `LatticeArrayScheme.{NAME}_SCHEME`.
            coarse_steps:
                int for number of coarsening steps.
            borders_width:
                int for width of coarsening regions.
            buffer:
                int for width of coarsest region.
            finest_width:
                int for width of finest resolved region.
            repeat:
                int for number of segment repetitions.

        Raises:
            ConfigurationError:
                Invalid or no scheme set when also using optional args.
            ValueError:
                Args have invalid values.
        """
        # history of modifications for this instance
        # elements are tuple of callable and args tuple and kwargs dict
        self._modifier_history: list[tuple[Callable, tuple, dict]] = []

        # copy class property to instance
        self.PROPERTY_DESCRIPTIONS = dict(type(self).PROPERTY_DESCRIPTIONS)

        # register update methods for different schemes
        self._update_properties = {
            LatticeArrayScheme.FIXED_WIDTH_SCHEME: self._update_props_fixed_width,
            LatticeArrayScheme.FIXED_NUMBER_SCHEME: self._update_props_fixed_number,
        }
        # register generation methods for different schemes
        self._generate_array = {
            LatticeArrayScheme.FIXED_WIDTH_SCHEME: self._generate_array_fixed_width,
            LatticeArrayScheme.FIXED_NUMBER_SCHEME: self._generate_array_fixed_number,
        }

        # set protected attributes from dict
        for prop, descr in self.PROPERTY_DESCRIPTIONS.items():
            setattr(
                self,
                "_" + prop,
                LatticeArrayProperty(
                    lattice=self,
                    description=descr,
                ),
            )

        # also use setattr for scheme to match intellisense appearance
        setattr(self, "_scheme", LatticeArrayScheme(lattice=self))

        # process init parameters
        init_param_dict = {
            "coarse_steps": coarse_steps,
            "borders_width": borders_width,
            "buffer": buffer,
            "finest_width": finest_width,
            "repeat": repeat,
        }
        # first set scheme to set up setters
        if scheme is not None:
            self.scheme.name = scheme
            # then init props based on params, errors handled by setters
            self.set_properties(init_param_dict)
        elif any(init_param_dict.values()):
            # scheme is None, raise if trying to set any values
            raise ConfigurationError(
                prop=init_param_dict,
                message="Trying to set init values for `LatticeArrayProperties` "
                "without specifying a scheme is not allowed.",
            )

    def __repr__(self):
        """Return string representation of `LatticeArray`."""
        return (
            f"<{type(self).__name__}(scheme={self.scheme.name}, "
            + ", ".join(
                f"{prop}={getattr(self, prop).value}"
                for prop in self.PROPERTY_DESCRIPTIONS
            )
            + f") at {hex(id(self))}>"
        )

    def __getattr__(self, name) -> Any:
        """Only gets called if attribute access fails."""
        # prevents pylint no-member error for dynamically set attributes
        raise AttributeError(f"'{type(self)}' object has no attribute '{name}'")
        # self.__getattribute__(name)

    @property
    def fingerprint(
        self,
    ) -> tuple[dict[str, int | str], list[tuple[Callable, tuple, dict]], type]:
        """Return collection of properties that identify self."""
        prop_dict = dict(
            (prop, getattr(self, prop).value) for prop in self.PROPERTY_DESCRIPTIONS
        )
        prop_dict["scheme"] = self.scheme.name

        return prop_dict, list(self._modifier_history), type(self)

    @property
    def finest_width(self) -> LatticeArrayProperty:
        """Width of finest region."""
        return self._finest_width

    @property
    def borders_width(self) -> LatticeArrayProperty:
        """Width per coarsening region."""
        return self._borders_width

    @property
    def coarse_steps(self) -> LatticeArrayProperty:
        """Number of coarsening borders."""
        return self._coarse_steps

    @property
    def buffer(self) -> LatticeArrayProperty:
        """Width of buffer region."""
        return self._buffer

    @property
    def repeat(self) -> LatticeArrayProperty:
        """Number of repetitions."""
        return self._repeat

    @property
    def scheme(self) -> LatticeArrayScheme:
        """Scheme for setting width of coarsening regions."""
        return self._scheme

    @property
    def array(self) -> npt.NDArray[np.object_]:
        """`np.ndarray` `dtype=object` encoding the current lattice."""
        return self.__volatile_access("_array")

    @property
    def resolution_list(self) -> list[int]:
        """Sorted list of all resolutions in `self`."""
        return self.__volatile_access("_res_list")

    @property
    def max_num_cells(self) -> dict[int, int]:
        """Maximum number of cells across all time slices."""
        return self.__volatile_access("_max_num_cells")

    @property
    def tx_dims(self) -> tuple[int, tuple[int, ...]]:
        """Tuple of (max_t, (max_x,)) dims in finest units."""
        return self.__volatile_access("_tx_dims")

    def __volatile_access(self, prop: str):
        """Access volatile properties in a protected way."""
        try:
            return getattr(self, prop)
        except AttributeError as e:
            raise VolatilePropertyError(lat=self, prop=prop) from e

    def copy(self) -> LatticeArray:
        """Return full copy of instance."""
        prop_dict, mod_hist, la_type = self.fingerprint
        copy = la_type(**prop_dict)
        copy._modifier_history = mod_hist
        if hasattr(self, "_array"):
            copy.generate_array()
        return copy

    def clear_modifier_history(self):
        """Clear all modifiers applied to instance."""
        self._modifier_history.clear()

    def set_properties(self, val_dict: dict):
        """Set instance properties' values from dict.

        Args:
            val_dict:
                dict with keys corresponding to valid
                `LatticeArrayProperties` of the calling instance.

        Raises:
            ConfigurationError: `val_dict` has invalid keys.
            ValueError: `val_dict` has invalid values.
        """
        if val_dict.keys() <= self.PROPERTY_DESCRIPTIONS.keys():
            # val_dict is subset of PROP_DESCR
            for prop in self.PROPERTY_DESCRIPTIONS:
                if (val := val_dict.get(prop, None)) is not None:
                    getattr(self, prop).value = val
        else:
            raise ConfigurationError(
                message="`val_dict` has invalid keys for `LatticeArrayProperties`",
                prop=val_dict.keys() - self.PROPERTY_DESCRIPTIONS.keys(),
            )

    def reset_volatile_properties(self):
        """Reset all volatile properties."""
        self.__dict__.pop("_array", None)
        self.__dict__.pop("_max_num_cells", None)
        self.__dict__.pop("_res_list", None)
        self.__dict__.pop("_tx_dims", None)

    def update_properties(self):
        """Recalculate all properties' values according to scheme.

        Is called on any change of `LatticeArrayProperties` or
        `LatticeArrayScheme`. Will reset any existing `.array`.

        Raises:
            ConfigurationError: Invalid or no scheme set.
        """
        # reset array if existing
        self.reset_volatile_properties()

        # update LatticeArrayProperties for set scheme
        try:
            self._update_properties[self.scheme.name]()
        except KeyError as e:
            raise ConfigurationError(
                message=f"Active scheme '{self.scheme.name}' is not recognized.\n"
                f"Is a valid scheme set? If so, make sure '{self.scheme.name}' is a "
                "valid key in `_update_properties`.",
                prop=self.scheme,
            ) from e

    def _update_props_fixed_width(self):
        """Recalculate properties according to fixed width scheme."""
        # Kayran-Scheme

        steps_power = 2**self.coarse_steps.value

        # configure borders_width
        self.borders_width._step = 2
        self.borders_width._min = steps_power * 3
        self.borders_width._check_bounds_and_update()

        # configure repeat
        self.repeat._min = 2
        self.repeat._check_bounds_and_update()

        # configure buffer
        self.buffer._min = 2
        self.buffer._check_bounds_and_update()

        # configure finest width
        self.finest_width._min = 7
        self.finest_width._check_bounds_and_update()

    def _update_props_fixed_number(self):
        """Recalculate properties according to fixed number scheme."""
        # Ipp-Scheme

        # configure borders_width
        self.borders_width._step = 1
        self.borders_width._min = 6
        self.borders_width._check_bounds_and_update()

        # configure repeat
        self.repeat._min = 2
        self.repeat._check_bounds_and_update()

        # configure buffer
        self.buffer._min = 2
        self.buffer._check_bounds_and_update()

        # configure finest width
        self.finest_width._min = 7
        self.finest_width._check_bounds_and_update()

    def _update_max_num_cells(self) -> None:
        """Recalculate `max_num_cells`."""
        self._max_num_cells = dict((k, 0) for k in self.resolution_list)
        ts: RegionSlice
        for ts in self.array:
            ts_size = dict((k, 0) for k in self.resolution_list)
            for reg in ts:
                ts_size[reg.a] += reg.n

            for res in ts_size:
                self._max_num_cells[res] = max(self._max_num_cells[res], ts_size[res])

    def _update_resolution_list(self) -> None:
        """Recalculate `resolution_list`."""
        tmp_res_list = set()
        ts: RegionSlice
        for ts in self.array:
            tmp_res_list.update(set(reg.a for reg in ts))

        self._res_list = sorted(tmp_res_list)

    def ts_resolution_list(self, time):
        """Return sorted list of resolutions for `time`."""
        return sorted(set(reg.a for reg in self.array[time]))

    def _update_tx_dims(self) -> None:
        """Recalculate `tx_dims`."""
        # based on time slice 0, as lattice is rectangular
        spatial = 0
        reg: RegionTuple
        for reg in self.array[0]:
            spatial += reg.a * reg.n

        self._tx_dims = (len(self.array), (spatial,))

    def update_volatile_properties(self):
        """Recalculate all volatile properties."""
        # order is important because of internal dependencies
        self._update_resolution_list()
        self._update_max_num_cells()
        self._update_tx_dims()

    def generate_array(self):
        """Generate lattice according to scheme and save in `.array`."""
        try:
            self._array = self._generate_lattice_array(
                *self._generate_array[self.scheme.name]()
            )
        except KeyError as e:
            raise ConfigurationError(
                message=f"Active scheme '{self.scheme.name}' is not recognized.\n"
                f"Is a valid scheme set? If so, make sure '{self.scheme.name}' is a "
                "valid key in `_generate_array`.",
                prop=self.scheme,
            ) from e

        # build lattice array
        self._propagate()
        self.update_volatile_properties()
        self.replay_modifier_history()

    def replay_modifier_history(self):
        """Replay modifications from history."""
        replay = self._modifier_history
        self._modifier_history = []
        step = 0
        try:
            for step, (func, args, kwargs) in enumerate(replay):
                func(self, *args, **kwargs)
        except (TypeError, LatticeGenerationError) as e:
            raise ModifierHistoryError(repr(self), step, replay) from e

    @staticmethod
    def _generate_lattice_array(regs_n, regs_a) -> npt.NDArray[np.object_]:
        """Generate the lattice given by the layout.

        Args:
            regs_n:
                Nested 2D array like with width of each region in units
                of the resolution for that region, for each time slice.
            regs_a:
                Nested 2D array like with resolution of the cells for
                each region, for each time slice.

        All arguments must share the same len (for the outermost list)
        and all sublists with the same position in the outer list must
        share the same len as well.

        Returns:
            `np.ndarray` of `dtype=object` that fully characterizes the
            mesh. 1st dim is the number of time steps inherited from the
            arguments. 2nd dim is a 2D tuple with the layout (2nd dim of
            tuple) for each region (1st dim of tuple).
        """
        # find number of time steps Nt
        # take max so loop below raises error for wrong arguments
        time_steps = max(len(regs_n), len(regs_a))

        out = np.empty(time_steps, dtype=object)

        try:
            for i in range(time_steps):
                spatial_regions = max(len(regs_n[i]), len(regs_a[i]))
                out[i] = tuple(
                    RegionTuple(regs_a[i][j], regs_n[i][j])
                    for j in range(spatial_regions)
                )
        except IndexError as e:
            if any(time_steps != len(a) for a in (regs_n, regs_a)):
                # offender is first loop i
                output = f"{(len(regs_n), len(regs_a))=}"
            else:
                # offender is second loop j
                output = f"{(len(regs_n[i]), len(regs_a[i]))=}"
            raise LatticeGenerationError(
                message=f"{output} do not match!",
                prop="(regs_n, regs_a)",
            ) from e

        return out

    def _generate_array_fixed_width(self):
        """Generate array according to fixed width (Kayran) scheme."""

        # determine temporal steps to consider
        time_steps = int(2**self.coarse_steps.value)
        # prepare arrays
        regs_n = np.empty(time_steps, dtype=object)
        regs_a = np.empty(time_steps, dtype=object)

        # spatial width per region
        start = tuple(
            self.borders_width.value // 2**coa
            for coa in range(self.coarse_steps.value - 1, 0, -1)
        )
        # TBD:
        # second possibility, more memory usage, but less if and calc
        start1 = tuple(s + 1 for s in start)
        sstart = (start, start1)

        # width in units of finest mesh for each region
        # include 1 for finest mesh
        # exclude 2**coarse_steps for coarsest mesh
        width_area = tuple(
            2**coa for coa in range(self.coarse_steps.value - 1, -1, -1)
        )
        coa1 = self.coarse_steps.value - 1

        # generate the lattice_array components
        for i in range(2**coa1):
            # TBD:
            # second possibility
            # binIdx = f"{i:{coa1}b}".replace(' ','0')
            # number of cells per area
            # cells_area = [ start[k] if binIdx[k]=='0' else start[k]+1
            #        for k in range(coa1) ] + [2**coarse_steps -2*i -1]
            # third version:
            # binIdx = tuple(map(int, f"{i:{coa1}b}".replace(" ", "0")))
            # cells_area = [sstart[binIdx[k]][k] for k in range(coa1)]+[
            # 2 ** self.coarse_steps.value - 2 * i - 1]

            # binary encoding of alternating rows (gives straight edge)
            binIdx = f"{i:{coa1}b}".replace(" ", "0")
            cells_area = [sstart[int(binIdx[k])][k] for k in range(coa1)] + [
                2**self.coarse_steps.value - 2 * i - 1
            ]

            regs_n[2 * i] = tuple(cells_area)
            regs_a[2 * i] = width_area[:]
            regs_n[2 * i + 1] = tuple(cells_area)
            regs_a[2 * i + 1] = width_area[:]

        return regs_n, regs_a

    def _generate_array_fixed_number(self):
        """Generate array according to fixed number (Ipp) scheme."""

        # determine temporal steps to consider
        time_steps = int(2**self.coarse_steps.value)
        # prepare return variables
        regs_n = np.empty(time_steps, dtype=object)
        regs_a = np.empty(time_steps, dtype=object)

        # spatial width per area
        start = self.borders_width.value
        # TBD:
        # second possibility, more memory usage, but less if and calc
        start1 = start + 1
        sstart = (start, start1)

        # width in units of finest mesh for each area
        # include 1 for finest mesh
        # exclude 2**coarse_steps for coarsest mesh
        width_area = tuple(
            2**coa for coa in range(self.coarse_steps.value - 1, -1, -1)
        )

        coa1 = self.coarse_steps.value - 1

        # generate the lattice_array components
        for i in range(2**coa1):
            # TBD:
            # second possibility
            # binIdx = f"{i:{coa1}b}".replace(' ','0')
            # number of cells per area
            # cells_area = [ start[k] if binIdx[k]=='0' else start[k]+1
            #        for k in range(coa1) ] + [2**coarse_steps -2*i -1]
            # third version:
            # binIdx = tuple(map(int, f"{i:{coa1}b}".replace(" ", "0")))
            # cells_area = [sstart[binIdx[k]] for k in range(coa1)] + [
            #    2 ** self.coarse_steps.value - 2 * i - 1 ]
            # binary encoding of alternating rows (gives straight edge)
            binIdx = f"{i:{coa1}b}".replace(" ", "0")
            cells_area = [sstart[int(binIdx[k])] for k in range(coa1)] + [
                2**self.coarse_steps.value - 2 * i - 1
            ]

            regs_n[2 * i] = tuple(cells_area)
            regs_a[2 * i] = width_area[:]
            regs_n[2 * i + 1] = tuple(cells_area)
            regs_a[2 * i + 1] = width_area[:]

        return regs_n, regs_a

    def _propagate(self):
        """Propagate `self.array` from lower left to upper right.

        Spacetime on the left (small x) is filled with cells of size
        `2**coarse_steps` that are one step coarser than the previous
        coarsest cells (which are on the left by default). Spacetime
        on the right is filled with finest resolution cells.

        This modifies the `.array` property.
        """
        # get resolution of boundaries of lat in units # of finest mesh
        coa = 2**self.coarse_steps.value
        l_coarsest_t = coa
        out_t = self.repeat.value * len(self.array)

        out = np.empty(out_t, dtype=object)

        # buffer on the right (in coarsest units)
        rbuffer = self.repeat.value - 1

        # calculate each time slice's regions
        for t in range(out_t):
            # number of buffer cells
            q, r = divmod(t, l_coarsest_t)

            temp = self.spatial_extend_array(
                ((RegionTuple(l_coarsest_t, self.buffer.value + q),),),
                (self.array[r],),
                [None],
            )
            temp = self.spatial_extend_array(
                temp,
                ((RegionTuple(1, (rbuffer - q) * coa + self.finest_width.value - 1),),),
                [None],
            )

            out[t] = temp[0]

        self._array = out

    @staticmethod
    def spatial_mirror_array(lat, out=None) -> npt.NDArray[np.object_]:
        """Mirror the given `lat` in spatial direction.

        Args:
            lat: Lattice array. Usually `.array` property.
            out: Lattice array to save output. Len and type like `lat`.

        Returns:
            If `out` is given, change `out` in place and return `out`.
            Else return a new lattice array.

        Raises:
            TypeError:
                If `out` doesn't have `dtype=object`.
            LatticeGenerationError:
                If `lat` and `out` don't have same len (=temporal size).
        """
        if out is None:
            out = np.empty(len(lat), dtype=object)

        time_steps = max(len(lat), len(out))
        # for each time slice
        try:
            for t in range(time_steps):
                # reverse the regions
                out[t] = tuple(reversed(lat[t]))
        except ValueError as e:
            raise TypeError(
                f"`out` has invalid dtype {out.dtype}. Has to be `object`."
            ) from e
        except IndexError as e:
            raise LatticeGenerationError(
                f"lat and out have mismatching len: {len(lat)=}, {len(out)=}.",
                prop="temporal size",
            ) from e

        return out

    def spatial_mirror(self):
        """Mirror the lattice in spatial direction."""
        self.spatial_mirror_array(self.array, out=self._array)
        self.update_volatile_properties()
        self._modifier_history.append((type(self).spatial_mirror, tuple(), {}))

    @staticmethod
    def temporal_mirror_array(lat, out=None) -> npt.NDArray[np.object_]:
        """Mirror the given `lat` in temporal direction.

        Args:
            lat: Lattice array. Usually `.array` property.
            out: Lattice array to save output. Len and type like `lat`.

        Returns:
            If `out` is given, copy `lat` to `out` and return `out`.
            Else return a new lattice array.
        """
        flipped = np.flipud(lat)
        if out is None:
            out = np.copy(flipped)
        else:
            # this also works if out is lat
            np.copyto(out, flipped)

        return out

    def temporal_mirror(self):
        """Mirror the lattice in temporal direction."""
        self.temporal_mirror_array(self.array, out=self._array)
        self.update_volatile_properties()
        self._modifier_history.append((type(self).temporal_mirror, tuple(), {}))

    @staticmethod
    def spatial_extend_array(left, right, out=None) -> npt.NDArray[np.object_]:
        """Append lattice array `right` to the spatial right of `left`.

        Args:
            left: Lattice array on the spatial left.
            right: Lattice array on the spatial right.
            out: Lattice array to save output.

        All arguments must have the same len and dtype. If the touching
        regions of `left` and `right` share resolution, they get merged.

        Returns:
            If `out` is given, change `out` in place and return `out`.
            Else return new lattice array.

        Raises:
            TypeError:
                If `out` doesn't have `dtype=object`.
            LatticeGenerationError:
                If `left`, `right` or `out` don't have same len.
        """
        if out is None:
            out = np.empty(len(left), dtype=object)

        # take max, so that loop below throws error for mismatched len
        max_len = max(len(left), len(right), len(out))

        try:
            for t in range(max_len):
                # check for merge possibility
                # both, temporal and spatial must be equal
                l_last_reg: RegionTuple = left[t][-1]
                r_first_reg: RegionTuple = right[t][0]
                if l_last_reg.a == r_first_reg.a:
                    middle = (
                        RegionTuple(
                            l_last_reg.a,
                            l_last_reg.n + r_first_reg.n,
                        ),
                    )
                    out[t] = tuple(chain(left[t][:-1], middle, right[t][1:]))

                else:
                    out[t] = tuple(chain(left[t], right[t]))

        except IndexError as e:
            raise LatticeGenerationError(
                "left, right and out have mismatching len:"
                f"{len(left)=}, {len(right)=}, {len(out)=}",
                prop="temporal size",
            ) from e
        except ValueError as e:
            raise TypeError(
                f"`out` has invalid dtype {out.dtype}. Has to be `object`."
            ) from e

        return out

    def _spatial_extend(self, prop_dict, mod_hist, lattice_type, tmpLA=None):
        if tmpLA is None:
            # create tmp copy
            tmpLA = lattice_type(**prop_dict)
            tmpLA._modifier_history = mod_hist
            tmpLA.generate_array()

        self.spatial_extend_array(self.array, tmpLA.array, out=self._array)
        self.update_volatile_properties()
        self._modifier_history.append(
            (
                type(self)._spatial_extend,
                (prop_dict, mod_hist, lattice_type),
                {},
            )
        )

    def spatial_extend(self, other: LatticeArray):
        """Append `other` to the spatial right of self."""
        self._spatial_extend(*other.fingerprint, tmpLA=other)

    @staticmethod
    def temporal_extend_array(bottom, top, out=None) -> npt.NDArray[np.object_]:
        """Append lattice array `top` on temporal top of `bottom`.

        Args:
            bottom: Lattice array on the temporal bottom.
            top: Lattice array on the temporal top.
            out: Lattice array to save output.

        All args must have the same spatial dimension and dtype.

        Returns:
            If `out` is given, change `out` in place and return `out`.
            Else return new lattice array.

        Raises:
            LatticeGenerationError:
                If `bottom` and `top` don't have same spatial dim.
            TypeError:
                If `out` doesn't have `dtype=object`.
        """
        # spatial dims of touching time slices
        bottom_slice: RegionSlice = bottom[-1]
        top_slice: RegionSlice = top[0]
        bottom_dim = sum(reg.a * reg.n for reg in bottom_slice)
        top_dim = sum(reg.a * reg.n for reg in top_slice)
        if bottom_dim != top_dim:
            raise LatticeGenerationError(
                f"Spatial dimension mismatch:\n{top=}\nand\n{bottom=}",
                "spatial dimension",
            )

        return np.concatenate((bottom, top), axis=0, out=out)

    def _temporal_extend(self, prop_dict, mod_hist, lattice_type, tmpLA=None):
        if tmpLA is None:
            # create tmp copy
            tmpLA = lattice_type(**prop_dict)
            tmpLA._modifier_history = mod_hist
            tmpLA.generate_array()

        self._array = self.temporal_extend_array(self.array, tmpLA.array)
        self.update_volatile_properties()
        self._modifier_history.append(
            (
                type(self)._temporal_extend,
                (prop_dict, mod_hist, lattice_type),
                {},
            )
        )

    def temporal_extend(self, other: LatticeArray):
        """Append `other` to the temporal top of self."""
        self._temporal_extend(*other.fingerprint, tmpLA=other)


class SuperimposeLatticeArray(LatticeArray):
    """Superimposes `LatticeArray` with mirrored counterpart.

    Extends super with restrictions for its properties that
    enforce double cell tips (cusps). Overrides `_propagate()` to
    superimpose each time slice with its spatially mirrored counterpart
    before appending it to the output. Adds `superimpose()` for this.
    """

    # pylint: disable=protected-access,attribute-defined-outside-init

    def _update_props_fixed_width(self):
        """Recalculate properties according to fixed width scheme.

        Overrides super, limits the lattice to double cell tips (cusps).
        """
        # Kayran-Scheme

        # resolution of coarsest
        steps_power = 2**self.coarse_steps.value

        # configure borders_width
        self.borders_width._step = 2
        self.borders_width._min = steps_power * 3
        self.borders_width._check_bounds_and_update()

        # width of coarsening regions
        regions_sum = sum(
            2**coa * (self.borders_width.value // 2**coa + 1)
            for coa in range(1, self.coarse_steps.value)
        )

        # configure finest_width
        self.finest_width._step = steps_power
        self.finest_width._min = steps_power - regions_sum % steps_power
        while self.finest_width._min < 7:
            self.finest_width._min += self.finest_width._step
        self.finest_width._check_bounds_and_update()

        # configure repetitions
        self.repeat._step = 2
        self.repeat._min = (
            (regions_sum + self.finest_width.value) // steps_power
            + 3
            + self.buffer.value % 2
        )
        self.repeat._check_bounds_and_update()

    def _update_props_fixed_number(self):
        """Recalculate properties according to fixed number scheme.

        Overrides super, limits the lattice to double cell tips (cusps).
        """
        # Ipp-Scheme

        # resolution of coarsest
        steps_power = 2**self.coarse_steps.value

        # configure borders_width
        self.borders_width._step = 1
        self.borders_width._min = 6
        self.borders_width._check_bounds_and_update()

        # width of coarsening regions
        regions_sum = (1 + self.borders_width.value) * sum(
            2**coa for coa in range(1, self.coarse_steps.value)
        )

        # configure finest_width
        self.finest_width._step = steps_power
        self.finest_width._min = steps_power - regions_sum % steps_power
        while self.finest_width._min < 7:
            self.finest_width._min += self.finest_width._step
        self.finest_width._check_bounds_and_update()

        # configure repetitions
        self.repeat._step = 2
        self.repeat._min = (
            (regions_sum + self.finest_width.value) // steps_power
            + 3
            + self.buffer.value % 2
        )
        self.repeat._check_bounds_and_update()

    def _propagate(self):
        """Propagate `self.array` from lower left to upper right.

        Overrides super. Every time slice is superimposed with its
        spatially mirrored counterpart.
        Before superposition the spacetime on the left (small x) is
        filled with cells of size `2**coarse_steps` that are one step
        coarser than the previous coarsest cells (which are on the left
        by default). Spacetime on the right is filled with finest
        resolution cells.
        After superposition the lattice will have transitions from
        coarse to fine when moving out from the center to the lattice
        borders. It is not guaranteed that every time slice has a region
        for each resolution in the range: coarsest <--> finest.

        Modifies the `.array` property.
        """
        # get resolution of boundaries of lat in units of finest mesh
        coa = 2**self.coarse_steps.value
        coarsest_a = coa
        out_t = self.repeat.value * len(self.array)

        out = np.empty(out_t, dtype=object)

        # finest buffer on the right (in coarsest units)
        rbuffer = self.repeat.value - 1

        # calculate each time slice's regions
        for t in range(out_t):
            # number of buffer cells
            q, r = divmod(t, coarsest_a)
            temp = self.spatial_extend_array(
                ((RegionTuple(coarsest_a, self.buffer.value + q),),),
                (self.array[r],),
                [None],
            )
            # append right buffer + finest_width of finest cells
            temp = self.spatial_extend_array(
                temp,
                ((RegionTuple(1, (rbuffer - q) * coa + self.finest_width.value - 1),),),
                temp,
            )
            # use new superimpose function on new class
            self.superimpose_array(
                temp,
                self.spatial_mirror_array(temp, [None]),
                temp,
            )
            # set time slice
            out[t] = temp[0]

        self._array = out

    @classmethod
    def superimpose_array(cls, f, g, out=None) -> npt.NDArray[np.object_]:
        """Superimpose `f` and `g` where finer resolution prevails.

        Args:
            f: Lattice array. Usually `.array` property.
            g: Lattice array. Usually `.array` property.

        Returns:
            If `out` is given, change `out` in place and return `out`.
            Else return a new lattice array.

        Raises:
            LatticeGenerationError:
                - If `f` and `g` differ in len (=number of time slices).
                - If any superimposed time slice of `f` and `g` differs
                  in spatial width.
            TypeError:
                If `out` doesn't have `dtype=object`.
        """
        # pylint: disable=too-many-branches
        # raise error for mismatched len
        if (max_t := len(f)) != len(g):
            raise LatticeGenerationError(
                "Superimposing lattices failed! Their temporal size differs:\n"
                f"{len(f)=}, {len(g)=}",
                prop="temporal size,",
            )

        if out is None:
            out = np.empty(max_t, dtype=object)

        elif len(out) != max_t:
            raise LatticeGenerationError(
                "Superimposing lattices failed! Temporal size of `out` differs:\n"
                f"{len(f)=}, {len(g)=}, {len(out)=}",
                prop="temporal size,",
            )

        # loop through all time slices in f and g
        for t in range(max_t):

            # the current time slices
            ft, gt = f[t], g[t]

            # manual iteration through ft, gt required
            # create the iterables through the regions
            f_iter, g_iter = iter(ft), iter(gt)
            # fetch first elements
            f_r: RegionTuple = next(f_iter)
            g_r: RegionTuple = next(g_iter)

            # output time slice
            temp = [(RegionTuple(-1, -1),)]
            # offset leftover for mismatched regions
            f_offset, g_offset = 0, 0

            try:
                # fake iteration over ft, gt
                while True:

                    # set current size
                    # leftovers of current regions
                    # or new regions' widths
                    f_size = f_offset if f_offset > 0 else f_r.n * f_r.a
                    g_size = g_offset if g_offset > 0 else g_r.n * g_r.a

                    # finer resolution prevails: min
                    r_m = min(f_r.a, g_r.a)

                    # regions share size
                    if f_size == g_size:
                        # append next region
                        cls._superimpose_step_spatial_extend(f_size, r_m, temp)
                        # no offset, regions have same size
                        f_offset, g_offset = 0, 0
                        # advance iterables
                        f_r = next(f_iter)
                        g_r = next(g_iter)

                    elif f_size < g_size:
                        # append next region
                        cls._superimpose_step_spatial_extend(f_size, r_m, temp)
                        # calculate covered region
                        g_offset = g_size - f_size
                        f_offset = 0
                        # advance iterable, g_r has leftover
                        f_r = next(f_iter)

                    # l_size > g_size
                    else:
                        # append next region
                        cls._superimpose_step_spatial_extend(g_size, r_m, temp)
                        # calculate covered region
                        f_offset = f_size - g_size
                        g_offset = 0
                        # advance iterable, l_r has leftover
                        g_r = next(g_iter)

            except StopIteration as e:
                # will catch next([*]_iter) depletion

                # check same spatial width for both input arrays
                if f_offset or g_offset:
                    # if other than 0, there is leftover
                    raise LatticeGenerationError(
                        "Superimposing lattices failed! Their spatial size differs:\n"
                        f"l[{t}]={ft}\ng[{t}]={gt}",
                        prop="spatial size",
                    ) from e

            try:
                # append new time slice in o to out
                # discard first, empty region
                out[t] = tuple(temp[0][1:])

            except ValueError as e:
                raise TypeError(
                    f"`out` has invalid dtype {out.dtype}. Has to be `object`."
                ) from e

        return out

    @classmethod
    def _superimpose_step_spatial_extend(cls, size, res, output):
        # this division has no remainder for double cell tips
        # int division used to get int type
        num_coa = size // res
        # add num_coa cells, change in place
        cls.spatial_extend_array(
            output,
            ((RegionTuple(res, num_coa),),),
            out=output,
        )

    def _superimpose(self, prop_dict, mod_hist, lattice_type, tmpLA=None):
        if tmpLA is None:
            # create tmp copy
            tmpLA = lattice_type(**prop_dict)
            tmpLA._modifier_history = mod_hist
            tmpLA.generate_array()

        self.superimpose_array(self.array, tmpLA.array, out=self._array)
        self.update_volatile_properties()
        self._modifier_history.append(
            (
                type(self)._superimpose,
                (prop_dict, mod_hist, lattice_type),
                {},
            )
        )

    def superimpose(self, other: LatticeArray):
        """Superimpose `other` with self, finer prevails."""
        self._superimpose(*other.fingerprint, tmpLA=other)
