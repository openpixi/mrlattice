"""Create `LatticeArray` and subclasses.

Available classes are:
- LatticeArray
- SuperimposeLatticeArray
- HomogeneousLatticeArray
- PulseLatticeArray
- SuperimposePulseLatticeArray
"""

# make classes available from mrlattice.lattice.generate

from mrlattice.lattice.generate.default import (
    LatticeArray,
    RegionTuple,
    RegionSlice,
    SuperimposeLatticeArray,
)
from mrlattice.lattice.generate.advanced import (
    HomogeneousLatticeArray,
    PulseLatticeArray,
    SuperimposePulseLatticeArray,
)

__all__ = [
    "RegionTuple",
    "RegionSlice",
    "LatticeArray",
    "SuperimposeLatticeArray",
    "HomogeneousLatticeArray",
    "PulseLatticeArray",
    "SuperimposePulseLatticeArray",
]
