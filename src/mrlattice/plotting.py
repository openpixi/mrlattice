"""Plotting functionality.

Provides:
- Functions:
    - generate_plot_lattice
    - plot_mesh
    - generate_print_lattice
"""
# remove after py 3.11
from __future__ import annotations

import typing

import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt

if typing.TYPE_CHECKING:
    from mrlattice.lattice.generate import RegionSlice, RegionTuple


__all__ = [
    "generate_plot_lattice",
    "generate_print_lattice",
    "plot_mesh",
]


def generate_plot_lattice(lattice_array: npt.NDArray[np.object_]):
    """Generate a Numpy array for mesh plotting.

    Args:
        lattice_array: Valid `LatticeArray.array` or Numpy array.

    Returns:
        Boolean Numpy array representation of the lattice with
        `shape = (2*Nt+1, 2*Ns+1)`. Nt: number of time slices,
        Ns: number of spatial cells, both given in units of the overall
        finest resolution. The return array is meant to be used with the
        custom plotting routines in the `plotting` module.
    """
    # time steps in finest mesh units
    time_steps = len(lattice_array)

    # find spatial resolution in finest mesh units
    # is the same for all time slices => take 0th
    # times two because of border pixels
    slice_zero: RegionSlice = lattice_array[0]
    fine_mesh = sum(ar.a * ar.n * 2 for ar in slice_zero)

    # double size, borders take 1px
    # +1 for outermost boundary
    # bool: 0 => cell content, 1 => border
    full_lattice = np.zeros((time_steps * 2 + 1, fine_mesh + 1), dtype=np.bool_)
    # needs different type for figuring out time step width of cells
    time_lattice = np.zeros((time_steps * 2 + 1, fine_mesh + 1), dtype=np.uint64)

    # set outermost boundary
    full_lattice[-1] = True
    full_lattice[:, -1] = True

    # fill lattice_array
    areas: RegionTuple
    for i in range(time_steps):
        start = 0
        for areas in lattice_array[i]:
            # times two, because of border pixels
            stop = start + areas.a * areas.n * 2
            # set correct spatial step with proper stepping through mesh
            full_lattice[2 * i, start : stop : areas.a * 2] = areas.a
            full_lattice[2 * i + 1, start : stop : areas.a * 2] = areas.a
            # set time step for all elements
            time_lattice[2 * i, start:stop] = areas.a
            time_lattice[2 * i + 1, start:stop] = areas.a
            # set new start for next slice
            start = stop

    # set subsequent cells with dt>1 to zero
    # just like for coarser spatial stepping
    for i in range(0, time_steps * 2, 2):
        # set smallest time slice borders
        time_lattice[i + 1] = 0
        # if there exist coarser time steps
        for t in range(1, np.max(time_lattice[i])):
            time_lattice[i + 2 * t][time_lattice[i] > t] = 0

    # only keep 0 (empty) where both spatial and time lattice are 0
    # borders (1) overwrite cell content
    np.logical_or(full_lattice, time_lattice, out=full_lattice)
    return full_lattice


def plot_mesh(lattice: npt.NDArray, alpha_bg=False, fig=None):
    """Plot the mesh corresponding to `lattice`.

    Args:
        lattice:
            Boolean Numpy array with True where to draw borders. Takes
            return value of `generate_plot_lattice`. Shape needs to
            be (2*Nt+1, 2*Ns+1). Nt: number of time slices, Ns: number
            of spatial cells, both given in units of the overall finest
            resolution.
        alpha_bg:
            If set to True, draw black lines on transparent background.
            Else Viridis colormap from Matplotlib is used for (0, 1) =>
            color conversion of array values. Default: False.
        ax:
            Figure to use for plotting. A new axis will be created. If
            `None` a new figure will be created.

    Returns:
        Tuple of `(fig, ax)`, where `fig` is the used Matplotlib
        figure and `ax` the axes with the plot.
    """
    if fig is None:
        fig, ax = plt.subplots()
    else:
        ax = fig.add_subplot()
    ax.set_aspect("equal")
    # remove axes and ticks
    ax.set_xticks([])
    ax.set_yticks([])
    # allow alpha background
    fig.patch.set_visible(False)
    ax.set_frame_on(False)

    ax.set_xlabel(r"$x\rightarrow$")
    ax.set_ylabel(r"$t\rightarrow$")

    # fix interpolation to 'none' for keeping sharp edges
    if alpha_bg:
        # convert lattice array to masked array with values only for
        # for cell borders and transparent cells (masked)
        grid = np.ma.masked_equal(lattice, False, copy=False)
        ax.imshow(grid, origin="lower", interpolation="none", cmap="gray")
    else:
        ax.imshow(lattice, origin="lower", interpolation="none")

    return fig, ax


def generate_print_lattice(lattice_array: npt.NDArray[np.object_]):
    """Numbers for each cell representing resolution."""
    slice_zero: RegionSlice = lattice_array[0]
    fine_mesh = sum(ar.a * ar.n for ar in slice_zero)
    time_steps = len(lattice_array)
    print_lattice = np.empty((time_steps, fine_mesh), dtype=np.uint64)

    areas: RegionTuple
    for i in range(time_steps):
        start = 0
        for areas in lattice_array[i]:
            stop = start + areas.n * areas.a
            print_lattice[i, start:stop] = areas.a
            start = stop
    return print_lattice
