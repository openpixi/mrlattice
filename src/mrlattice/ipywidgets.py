"""Customized ipywidgets for use with jupyter.

Provides:
- LatticeArrayPlotWidget
    Versatile widget for plotting all types of `LatticeArrays`.
- SuperimposeLatticeArrayPlotWidget
    Specialized widget for plotting subtypes of `LattiveArrays`.
- LatticeArrayModifierWidget
    Widget for applying modifiers to `LatticeArrays`.
"""

from packaging import version, specifiers

from IPython.core import getipython
from IPython.core.magic import register_line_magic
from IPython.display import display

import ipywidgets as widgets

import mrlattice
import mrlattice.lattice
import mrlattice.lattice.exceptions
import mrlattice.plotting
from mrlattice.lattice.generate import (
    LatticeArray,
    SuperimposeLatticeArray,
    PulseLatticeArray,
    SuperimposePulseLatticeArray,
)

__all__ = [
    "LatticeArrayPlotWidget",
    "SuperimposeLatticeArrayPlotWidget",
    "PulseLatticeArrayPlotWidget",
    "SuperimposePulseLatticeArrayPlotWidget",
    "LatticeArrayModifierWidget",
]

# pylint: disable=protected-access


@register_line_magic
def mrlattice_version(line: str):
    """Check if installed mrlattice version matches set version."""

    nb_version_lock = specifiers.SpecifierSet(line)
    installed_version = version.parse(mrlattice.__version__)

    warning_msg = (
        f"The installed version of mrlattice {installed_version}"
        f" does not match the required version {nb_version_lock}!"
        " Please install the correct version."
    )

    if installed_version not in nb_version_lock:
        raise ImportError(warning_msg)


# as per ipython documentation
del mrlattice_version


class LatticeArrayPlotWidget(widgets.Box):
    """Generate all widgets for plotting `LatticeArray`."""

    def __init__(self, lattice=None, max_vals=None):
        """Return new custom widget.

        Args:
            lattice: `LatticeArray` to use. If `None` a new instance is
                created that is bound to this widget.
            max_vals: Dict where each key is a `LatticeArrayProperty`
                name of `lattice` and val is a dict with the two valid
                schemes as keys and an integer for the max of that
                property. Used to update the default max values for
                properties inherited from `LatticeArray`.
        """
        super().__init__()

        # LatticeArray instance
        self.lattice = LatticeArray() if lattice is None else lattice
        # alias for later use
        fws = self.lattice.scheme.FIXED_WIDTH_SCHEME
        fns = self.lattice.scheme.FIXED_NUMBER_SCHEME
        # get list of schemes
        self._valid_schemes = dict(self.lattice.scheme.SCHEME_DESCRIPTIONS)
        del self._valid_schemes[None]

        if not self.lattice.scheme.name:
            self.lattice.scheme.set_fixed_width()

        # plot output
        self.plot_out = widgets.Output()
        self.fig_out, self.ax_out = None, None

        # widget layouting
        auto = widgets.Layout(width="auto")

        # sliders' max values
        self._default_max_vals = {
            "borders_width": {
                fws: 200,
                fns: 200,
            },
            "coarse_steps": {
                fws: 6,
                fns: 6,
            },
            "repeat": {
                fws: 400,
                fns: 400,
            },
            "buffer": {
                fws: 100,
                fns: 100,
            },
            "finest_width": {
                fws: 400,
                fns: 400,
            },
        }
        self.max_vals = dict(self._default_max_vals)
        if max_vals is not None:
            # check for invalid keys
            if not max_vals.keys() <= self.lattice.PROPERTY_DESCRIPTIONS.keys():
                raise mrlattice.lattice.exceptions.ConfigurationError(
                    message="max_vals has invalid keys for properties of lattice",
                    prop=max_vals.keys() - self.lattice.PROPERTY_DESCRIPTIONS.keys(),
                )
            self.max_vals.update(max_vals)

        # capture slider_callback functions output
        self._slider_callback_captured = self.plot_out.capture()(self._slider_callback)

        # isntance properties derived from lattice properties
        for prop in self.lattice.PROPERTY_DESCRIPTIONS:
            # values
            setattr(self, prop + "_text", widgets.BoundedIntText(layout=auto))

            # sliders
            setattr(
                self, prop + "_slider", widgets.IntSlider(readout=False, layout=auto)
            )
            getattr(self, prop + "_slider").lattice_prop = getattr(self.lattice, prop)

            # link slider and values
            for v in ("min", "max", "step", "value"):
                widgets.link(
                    (getattr(self, prop + "_slider"), v),
                    (getattr(self, prop + "_text"), v),
                )

            # register slider callbacks
            getattr(self, prop + "_slider").observe(
                self._slider_callback_captured,
                names="value",
            )

            # sliders' max values
            self._update_slider_max_vals(getattr(self, prop + "_slider"), key=prop)
            # sliders' values
            self._update_slider_widgets(getattr(self, prop + "_slider"))

            # labels
            setattr(self, prop + "_label", widgets.Label())
            getattr(self, prop + "_label").lattice_prop = getattr(self.lattice, prop)
            self._update_label_widgets(getattr(self, prop + "_label"))

        # scheme labels
        self.scheme_title = widgets.Label("Scheme for Coarsening Regions:")
        self.scheme_label = widgets.Label(self.lattice.scheme.description)
        self.scheme_label.lattice_prop = self.lattice.scheme

        # radio buttons
        self.scheme = widgets.RadioButtons(
            options=[
                (k.title().replace("_", " "), k) for k in self._valid_schemes.keys()
            ],
            value=self.lattice.scheme.name,
            layout={"width": "max-content", "margin": "10px 10px 10px 10px"},
        )
        self.scheme.lattice_prop = self.lattice.scheme
        self.scheme.observe(
            self.plot_out.capture()(self._scheme_callback),
            names="value",
        )

        # checkboxes
        self.alpha_bg = widgets.Checkbox(
            description="Black Mesh on Alpha Background", indent=False
        )
        self.flip_x = widgets.Checkbox(
            description="Flip Lattice in Spatial Direction",
            indent=False,
        )
        self.flip_t = widgets.Checkbox(
            description="Flip Lattice in Temporal Direction",
            indent=False,
        )

        # buttons
        self.b = widgets.Button(description="Plot Lattice", tooltip="Click Me")
        self.clear = widgets.Button(description="Clear Output", tooltip="Click Me")
        self.snip = widgets.Button(description="Export Snippet", tooltip="Click Me")
        # button callbacks
        self.b.on_click(self.plot_out.capture()(self._plot_button_on_click))
        self.clear.on_click(self.plot_out.capture()(self._clear_output))
        self.snip.on_click(self.plot_out.capture()(self._export_snip))

        # register all widgets with self
        self.children = (
            widgets.VBox(
                [
                    widgets.HBox(
                        [
                            widgets.VBox(
                                list(
                                    getattr(self, label + "_label")
                                    for label in self.lattice.PROPERTY_DESCRIPTIONS
                                )
                                + [self.scheme_title, self.scheme_label],
                                layout=widgets.Layout(width="40%"),
                            ),
                            widgets.VBox(
                                list(
                                    getattr(self, label + "_slider")
                                    for label in self.lattice.PROPERTY_DESCRIPTIONS
                                )
                                + [widgets.Label(), self.scheme],
                                layout=widgets.Layout(width="30%"),
                            ),
                            widgets.VBox(
                                tuple(
                                    getattr(self, label + "_text")
                                    for label in self.lattice.PROPERTY_DESCRIPTIONS
                                ),
                                layout=widgets.Layout(width="8%"),
                            ),
                            widgets.VBox(
                                # empty VBox used to insert blank space
                                layout=widgets.Layout(width="10pt"),
                            ),
                            widgets.VBox(
                                [
                                    self.alpha_bg,
                                    self.flip_x,
                                    self.flip_t,
                                ],
                                layout=widgets.Layout(width="40%"),
                            ),
                        ]
                    ),
                    widgets.HBox(
                        [
                            self.b,
                            self.clear,
                            self.snip,
                        ]
                    ),
                ],
                layout=widgets.Layout(width="100%"),
            ),
        )

    def _ipython_display_(self, **kwargs):
        """Append to super `display()` call to display `plot_out`."""
        super()._ipython_display_(**kwargs)
        display(self.plot_out)

    def _update_slider_widgets(self, widget=None):
        if widget is not None:
            # unobserve to prevent recursion loop from _slider_callback
            widget.unobserve(self._slider_callback_captured, names="value")
            widget.min = widget.lattice_prop._min
            widget.step = widget.lattice_prop._step
            widget.value = widget.lattice_prop.value
            widget.observe(self._slider_callback_captured, names="value")
        else:
            for prop in self.lattice.PROPERTY_DESCRIPTIONS:
                self._update_slider_widgets(getattr(self, prop + "_slider"))

    def _update_slider_max_vals(self, widget=None, key=None):
        if widget is not None:
            widget.max = self.max_vals[key][self.lattice.scheme.name]
        else:
            for prop in self.lattice.PROPERTY_DESCRIPTIONS:
                self._update_slider_max_vals(getattr(self, prop + "_slider"), key=prop)

    def _update_label_widgets(self, widget=None):
        if widget is not None:
            widget.value = widget.lattice_prop.description
        else:
            for prop in list(self.lattice.PROPERTY_DESCRIPTIONS) + [
                "scheme",
            ]:
                self._update_label_widgets(getattr(self, prop + "_label"))

    def _slider_callback(self, change):
        # propagate change
        change["owner"].lattice_prop.value = change["new"]
        # get new values from LatticeArray
        self._update_slider_widgets()

    def _scheme_callback(self, change):
        # propagate change
        change["owner"].lattice_prop.name = change["new"]
        # get new values from LatticeArray
        self._update_slider_max_vals()
        self._update_slider_widgets()
        self._update_label_widgets()

    def _clear_output(self, *_):
        self.plot_out.clear_output()
        if self.fig_out:
            mrlattice.plotting.plt.close(self.fig_out)
            self.fig_out, self.ax_out = None, None

    def _plot_button_on_click(self, *_):
        self._clear_output()
        # reset mod hist before doing potential mirroring
        self.lattice._modifier_history = []
        self.lattice.generate_array()

        if self.flip_x.value:
            self.lattice.spatial_mirror()
        if self.flip_t.value:
            self.lattice.temporal_mirror()
        with self.plot_out:
            self.fig_out, self.ax_out = mrlattice.plotting.plot_mesh(
                mrlattice.plotting.generate_plot_lattice(self.lattice.array),
                alpha_bg=self.alpha_bg.value,
            )
            mrlattice.plotting.plt.show()

    def _pretty_print_mod_hist(self, mod_hist):
        out = "[\n\t"
        for mod in mod_hist:
            out += "("
            out += mod[0].__qualname__ + ", "
            out += "("
            if mod[1]:
                out += (
                    f"{mod[1][0]}, {self._pretty_print_mod_hist(mod[1][1])}, "
                    f"{mod[1][2].__qualname__}"
                )
            out += f"), {mod[2]}),\n\t"
        out += "]"
        return out

    def _export_snip(self, *args):
        # print code snippet to reproduce lattice
        prop_dict, mod_hist, la_type = self.lattice.fingerprint
        args = ", ".join(
            f'{k}="{v}"' if isinstance(v, str) else f"{k}={v}"
            for k, v in prop_dict.items()
        )
        print(
            f"lattice={la_type.__qualname__}({args})\n"
            f"lattice._modifier_history={self._pretty_print_mod_hist(mod_hist)}\n"
            "lattice.generate_array()"
        )


class SuperimposeLatticeArrayPlotWidget(LatticeArrayPlotWidget):
    """Uses `SuperimposeLatticeArray` per default."""

    def __init__(self, lattice=None, max_vals=None):  # noqa: D107
        new_lattice = SuperimposeLatticeArray() if lattice is None else lattice
        super().__init__(lattice=new_lattice, max_vals=max_vals)
        # disable spatial mirror
        self.flip_x.disabled = True


class PulseLatticeArrayPlotWidget(LatticeArrayPlotWidget):
    """Uses `PulseLatticeArray` per default."""

    def __init__(self, lattice=None, max_vals=None):  # noqa: D107
        new_lattice = PulseLatticeArray() if lattice is None else lattice
        super().__init__(lattice=new_lattice, max_vals=max_vals)


class SuperimposePulseLatticeArrayPlotWidget(SuperimposeLatticeArrayPlotWidget):
    """Uses `SuperimposePulseLatticeArray` per default."""

    def __init__(self, lattice=None, max_vals=None):  # noqa: D107
        new_lattice = SuperimposePulseLatticeArray() if lattice is None else lattice
        super().__init__(lattice=new_lattice, max_vals=max_vals)
        # this lattice is symmetric in t and x
        self.flip_t.disabled = True


# widget to modify lattice arrays
class LatticeArrayModifierWidget(widgets.Box):
    """Generate widgets for modifying `LatticeArray`."""

    def __init__(self):
        """Prepare new instance."""
        super().__init__()

        # plot output
        self.plot_out = widgets.Output()
        self.fig_out, self.ax_out = None, None
        self.lattice = None

        # two dropdown widgets for involved LA
        self.owner_dropdown = widgets.Dropdown(description="LA 1")
        self.other_dropdown = widgets.Dropdown(description="LA 2")

        # two checkboxes for copy toggle
        self.owner_copy_chckbox = widgets.Checkbox(
            description="Work with Copy of LA 1",
            indent=False,
            value=False,
            layout=widgets.Layout(width="20%"),
        )
        self.other_copy_chckbox = widgets.Checkbox(
            description="Work with Copy of LA 2",
            indent=False,
            value=False,
        )
        # alpha bg toggle
        self.alpha_bg = widgets.Checkbox(
            description="Black Mesh on Alpha Background", indent=False
        )

        # input field to bind new name if copy for LA 1 is True
        self.owner_copy_name = widgets.Text(
            placeholder="Enter Name", disabled=True, layout=widgets.Layout(width="20%")
        )
        self.owner_copy_label = widgets.Label("Name of Copy")
        # callback for chckbox trigger
        self.owner_copy_chckbox.observe(
            self.plot_out.capture()(self._owner_copy_name_observe),
            names="value",
        )

        # dropdown for modifier list
        self.modifier_dropdown = widgets.Dropdown(
            # using "names" of methods, because they need to be called
            # on the instance, so require getattr()
            options=[
                None,
                "spatial_extend",
                "temporal_extend",
                "spatial_mirror",
                "temporal_mirror",
                "superimpose",
            ],
            # value=None,
            description="Modifier",
        )

        # buttons
        self.plot_btn = widgets.Button(description="Plot Lattice", tooltip="Click Me")
        self.clear_btn = widgets.Button(description="Clear Output", tooltip="Click Me")
        self.refresh_btn = widgets.Button(
            description="Refresh Options", tooltip="Click Me"
        )
        self.snip = widgets.Button(description="Export Snippet", tooltip="Click Me")
        # button callbacks
        self.plot_btn.on_click(self.plot_out.capture()(self._plot_btn_on_click))
        self.clear_btn.on_click(self.plot_out.capture()(self._clear_btn_on_click))
        self.refresh_btn.on_click(self.plot_out.capture()(self._refresh_btn_on_click))
        self.snip.on_click(self.plot_out.capture()(self._export_snip))

        # populate list with existing
        self._refresh_btn_on_click()

        # register all widgets with self
        self.children = (
            widgets.VBox(
                [
                    widgets.HBox(
                        [
                            self.owner_dropdown,
                            self.owner_copy_chckbox,
                            self.owner_copy_label,
                            self.owner_copy_name,
                        ]
                    ),
                    widgets.HBox(
                        [
                            self.modifier_dropdown,
                            self.alpha_bg,
                        ]
                    ),
                    widgets.HBox(
                        [
                            self.other_dropdown,
                            self.other_copy_chckbox,
                        ]
                    ),
                    widgets.HBox(
                        [
                            self.plot_btn,
                            self.clear_btn,
                            self.refresh_btn,
                            self.snip,
                        ]
                    ),
                ],
                layout=widgets.Layout(width="100%"),
            ),
        )

    def _ipython_display_(self, **kwargs):
        """Append to super `display()` call to display `plot_out`."""
        super()._ipython_display_(**kwargs)
        display(self.plot_out)

    def _plot_btn_on_click(self, *_):
        self._clear_btn_on_click()

        # prepare lattice array instances
        output_lattice = None
        if isinstance(self.owner_dropdown.value, LatticeArray):
            output_lattice = self.owner_dropdown.value
        elif isinstance(self.owner_dropdown.value, LatticeArrayPlotWidget):
            output_lattice = self.owner_dropdown.value.lattice

        other_lattice = None
        if isinstance(self.other_dropdown.value, LatticeArray):
            other_lattice = self.other_dropdown.value
        elif isinstance(self.other_dropdown.value, LatticeArrayPlotWidget):
            other_lattice = self.other_dropdown.value.lattice

        # create copies if selected
        if self.owner_copy_chckbox.value:
            output_lattice = output_lattice.copy()
            # save new output array to globals
            getipython.get_ipython().push(
                {self.owner_copy_name.value: output_lattice}, interactive=True
            )

        if self.other_copy_chckbox.value:
            other_lattice = other_lattice.copy()

        # call selected modifier
        try:
            if (mod := self.modifier_dropdown.value) is not None:
                if other_lattice is not None:
                    getattr(output_lattice, mod)(other_lattice)
                else:
                    getattr(output_lattice, mod)()

        except AttributeError:
            # output_lattice has no attr superimpose
            if self.modifier_dropdown.value == "superimpose":
                res = SuperimposeLatticeArray.superimpose_array(
                    output_lattice.array, other_lattice.array
                )
                output_lattice = LatticeArray()
                output_lattice._array = res
                print("Warning! Input LA1 cannot save superimpose to its history!\n")
            else:
                raise

        except TypeError as e:
            raise Exception(
                "Please make sure the lattices are suitable for the selected modifier!"
            ) from e

        self.lattice = output_lattice

        # plot lattice
        with self.plot_out:
            self.fig_out, self.ax_out = mrlattice.plotting.plot_mesh(
                mrlattice.plotting.generate_plot_lattice(self.lattice.array),
                alpha_bg=self.alpha_bg.value,
            )
            mrlattice.plotting.plt.show()

    def _clear_btn_on_click(self, *_):
        self.lattice = None
        self.plot_out.clear_output()
        if self.fig_out:
            mrlattice.plotting.plt.close(self.fig_out)
            self.fig_out, self.ax_out = None, None

    def _refresh_btn_on_click(self, *_):
        # update available LatticeArrays from globals
        user_ns = getipython.get_ipython().user_ns
        options = [("None", None)] + [
            name
            for name in user_ns.items()
            if isinstance(name[1], (LatticeArray, LatticeArrayPlotWidget))
        ]
        self.owner_dropdown.options = options
        self.owner_dropdown.label = "None"
        self.other_dropdown.options = options
        self.other_dropdown.label = "None"

    def _owner_copy_name_observe(self, change):
        # disable text field when checkbox is unchecked
        self.owner_copy_name.disabled = not change["owner"].value
        self.owner_copy_name.value = ""

    def _pretty_print_mod_hist(self, mod_hist):
        return LatticeArrayPlotWidget._pretty_print_mod_hist(self, mod_hist)

    def _export_snip(self, *args):
        LatticeArrayPlotWidget._export_snip(self, *args)
