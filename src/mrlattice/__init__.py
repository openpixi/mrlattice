"""Create and plot inhomogeneously resolved spacetime lattices."""

# version as tuple for simple comparisons
VERSION = (0, 3, 0)
# version string created from tuple; used by setup.cfg
__version__ = ".".join([str(x) for x in VERSION])

# module list for star import
__all__ = ["lattice", "plotting"]
