# Documentation notebooks

A collection of documentary notebooks featuring the development history of mrlattice.
Its implementation details are explained with examples useful for understanding the different modules.
The notebooks are separated into features that can cover various module code.
Rerunning the notebooks requires a specific version of mrlattice which will be disclosed in the notebook itself.
If the version mismatches there will be a warning issued.
