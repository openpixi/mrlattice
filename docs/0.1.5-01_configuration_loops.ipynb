{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "incorporate-dinner",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "import sys\n",
    "import os\n",
    "\n",
    "from mrlattice.lattice.generate import *\n",
    "from mrlattice.lattice.validate import ValidateLattice"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "pressing-blame",
   "metadata": {},
   "outputs": [],
   "source": [
    "# this notebook requires mrlattice version:\n",
    "VERSION = \"0.1.5\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "pharmaceutical-andorra",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The following names are now available:\n",
      "\n",
      "<class '__main__.LatticeArrayPlotWidget'>\n",
      "<class '__main__.SuperimposeLatticeArrayPlotWidget'>\n"
     ]
    }
   ],
   "source": [
    "%run prepare_notebook.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "educated-turtle",
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext jupyter_spaces"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "assigned-brazil",
   "metadata": {},
   "source": [
    "# Summary\n",
    "\n",
    "We introcude the `ValidateLattice` class and expand it with a loop construct that increments each lattice array property as part of a nested loop. The number of nested loops dynamically adjusts to the number of lattice array properties. This makes it possible to \"loop through\" a large number of lattice array configurations for the purpose of validating them.\n",
    "\n",
    "\n",
    "# Lattice Validation Tests\n",
    "\n",
    "We now want to tackle the task of validating generated lattice arrays. \"Validation\" in this context shall mean checking if the lattice array follows a set of restrictions. We will derive these restrictions in the next notebooks.\n",
    "\n",
    "At first we introduce the `ValidateLattice` class from the `mrlattice.lattice.validate` module. In its current state this class only provides an empty \"testing suite\" that can be configured with test functions. Calling the configured class instance will execute all tests on a given lattice array. The lattice array has to be supplied when creating an instance. By default, the test log is printed to stdout, but this behavior is configurable.\n",
    "\n",
    "Our goal is not only to come up with a set of validation tests, but also to exectue them on a large set of possible lattice configurations. For this the obvious approach would be to nest loops, one for each property, and increment each property a number of times, generating a new lattice and testing it for evey step.  \n",
    "However, there is a problem. As we need a loop for each property, we somehow must know how many properties exist for the given lattice and most importantly the order in which to nest them. Properties that depend on the values of others must be deeper nested. Hardcoding this information into `ValidateLattice` would make it hard to execute the same suite on different types of lattice arrays. The solution we present here makes use of a recursive function instead.\n",
    "\n",
    "\n",
    "### Replacing Nested Loops with a Recursive Function\n",
    "\n",
    "Take for example the `HomogeneousLatticeArray` with its three properties:\n",
    "- coarse_steps\n",
    "- buffer\n",
    "- repeat\n",
    "\n",
    "The code snipped would look like this:\n",
    "```python\n",
    "array.buffer.value = array.buffer._min\n",
    "for b in range(b_steps):\n",
    "    array.coarse_steps.value = array.coarse_steps._min\n",
    "    for c in range(c_steps):\n",
    "        array.repeat.value = array.repeat._min\n",
    "        for r in range(r_steps):\n",
    "            array.generate_array()\n",
    "            # run tests ...\n",
    "            \n",
    "            array.repeat.increment()\n",
    "            \n",
    "        array.coarse_steps.increment()\n",
    "        \n",
    "    array.buffer.increment()\n",
    "```\n",
    "\n",
    "Now consider the method `launch_property_loops()` from the class definition of `NewValidateLattice` below. This is a recursive function with a loop. It makes use of the `.sorted_props` list. In the simplest case (no recursion) this list only has one element. In the for loop the if clause will always be true and we basically have replicated the innermost loop of the nested loops from above. In the case where the list contains more elements, the else clause will be true and instead of executing the test suite, the next property in the list will be reset to its minimum value and the function calls itself with an incremented `idx` argument. This will build the number of outer loops required dynamically, without ever explicitly using the names of the lattice array properties. We also chose the same number of steps for all properties for now. But these can be adjusted with the `.increments` attribute, where each element gives the count of loop repetitions for the lattice array property at the same list index in `.sorted_props`.\n",
    "\n",
    "We now need to build this list of properties and sort it correctly. We require this list to be supplied to `NewValidateLattice` instances, where the properties in the list must be sorted with ascending dependencies. E.g. the property that depends on all the others must come last."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "involved-music",
   "metadata": {},
   "outputs": [],
   "source": [
    "class NewValidateLattice(ValidateLattice):\n",
    "    \n",
    "    def __init__(self, lattice, preset=None, sorted_props=None, increments=None, **funcs):\n",
    "        super().__init__(lattice, preset, **funcs)\n",
    "        \n",
    "        self.sorted_props = sorted_props if sorted_props else list()\n",
    "        self.increments = increments if increments else [3] * len(sorted_props)\n",
    "    \n",
    "    def launch_property_loops(self, idx=0, log_out=open(os.devnull, 'w')):\n",
    "        \"\"\"Run nested prop change loops and validate each time.\n",
    "        \n",
    "        Depends on `self.sorted_props` for the lattice array properties\n",
    "        to loop over and increment. Depends on `self.increments` for\n",
    "        the number of times each property will be incremented.\n",
    "        \n",
    "        Args:\n",
    "            idx:\n",
    "                Start index for picking properties from `self.sorted_props`\n",
    "                and nesting loops for each. Defaults to: 0.\n",
    "            log_out:\n",
    "                File like object to redirect stdout. Defaults to: Null.\n",
    "        \"\"\"\n",
    "        for step in range(self.increments[idx]):\n",
    "            # for the innermost loop\n",
    "            if len(self.sorted_props[idx:]) == 1:\n",
    "                self.lattice.generate_array()\n",
    "                print(\"\\n\\n\", self.lattice, file=log_out)\n",
    "                # call test suite\n",
    "                self(stdout=log_out)\n",
    "                # increment current prop\n",
    "                getattr(self.lattice, self.sorted_props[idx]).increment()\n",
    "                \n",
    "            else:\n",
    "                # reset value of next nesting's prop\n",
    "                next_prop = getattr(self.lattice, self.sorted_props[idx+1])\n",
    "                next_prop.value = next_prop._min\n",
    "                # recursive call\n",
    "                self.launch_property_loops(idx+1, log_out)\n",
    "                # increment current prop\n",
    "                getattr(self.lattice, self.sorted_props[idx]).increment()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "executed-category",
   "metadata": {},
   "source": [
    "### Usage Example for HomogeneousLatticeArray"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "moving-sapphire",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%capture output\n",
    "%%space homogeneous\n",
    "\n",
    "l = HomogeneousLatticeArray()\n",
    "prop_sort = [\n",
    "    \"buffer\",\n",
    "    \"coarse_steps\",\n",
    "    \"repeat\",\n",
    "]\n",
    "v = NewValidateLattice(l, sorted_props=prop_sort)\n",
    "v.launch_property_loops(log_out=sys.stdout)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "promotional-district",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " <HomogeneousLatticeArray(scheme=fixed_number, coarse_steps=1, buffer=1, repeat=1 at 0x7fb23cebcc40>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <HomogeneousLatticeArray(scheme=fixed_number, coarse_steps=1, buffer=1, repeat=2 at 0x7fb23cebcc40>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <HomogeneousLatticeArray(scheme=fixed_number, coarse_steps=1, buffer=1, repeat=3 at 0x7fb23cebcc40>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <HomogeneousLatticeArray(scheme=fixed_number, coarse_steps=2, buffer=1, repeat=1 at 0x7fb23cebcc40>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <HomogeneousLatticeArray(scheme=fixed_number, coarse_steps=2, buffer=1, repeat=2 at 0x7fb23cebcc40>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <HomogeneousLatticeArray(scheme=fixed_number, coarse_steps=2, buffer=1, repeat=3 at 0x7fb23cebcc40>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <HomogeneousLatticeArray(scheme=fixed_number, coarse_steps=3, buffer=1, repeat=1 at 0x7fb23cebcc40>\n"
     ]
    }
   ],
   "source": [
    "# restrict number of lines in output\n",
    "[ print(o) for o in output.stdout.split(\"\\n\\n\")[:20] ];"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "awful-compatibility",
   "metadata": {},
   "source": [
    "### Usage Example for SuperimposeLatticeArray"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "understanding-alias",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%capture output\n",
    "%%space superimpose\n",
    "\n",
    "l = SuperimposeLatticeArray()\n",
    "l.scheme.set_fixed_width()\n",
    "prop_sort = [\n",
    "    \"buffer\",\n",
    "    \"coarse_steps\",\n",
    "    \"borders_width\",\n",
    "    \"finest_width\",\n",
    "    \"repeat\",\n",
    "]\n",
    "increm = [\n",
    "    1,\n",
    "    2,\n",
    "    2,\n",
    "    2,\n",
    "    5,\n",
    "]\n",
    "v = NewValidateLattice(l, sorted_props=prop_sort, increments=increm)\n",
    "v.launch_property_loops(log_out=sys.stdout)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "endangered-hypothetical",
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " <SuperimposeLatticeArray(scheme=fixed_width, coarse_steps=1, borders_width=2, buffer=1, finest_width=2, repeat=3 at 0x7fb23cebc040>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <SuperimposeLatticeArray(scheme=fixed_width, coarse_steps=1, borders_width=2, buffer=1, finest_width=2, repeat=5 at 0x7fb23cebc040>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <SuperimposeLatticeArray(scheme=fixed_width, coarse_steps=1, borders_width=2, buffer=1, finest_width=2, repeat=7 at 0x7fb23cebc040>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <SuperimposeLatticeArray(scheme=fixed_width, coarse_steps=1, borders_width=2, buffer=1, finest_width=2, repeat=9 at 0x7fb23cebc040>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <SuperimposeLatticeArray(scheme=fixed_width, coarse_steps=1, borders_width=2, buffer=1, finest_width=2, repeat=11 at 0x7fb23cebc040>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <SuperimposeLatticeArray(scheme=fixed_width, coarse_steps=1, borders_width=2, buffer=1, finest_width=4, repeat=4 at 0x7fb23cebc040>\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 0 tests:\n",
      "\t0 passed\n",
      "PASSED!\n",
      "\n",
      " <SuperimposeLatticeArray(scheme=fixed_width, coarse_steps=1, borders_width=2, buffer=1, finest_width=4, repeat=6 at 0x7fb23cebc040>\n"
     ]
    }
   ],
   "source": [
    "# restrict number of lines in output\n",
    "[ print(o) for o in output.stdout.split(\"\\n\\n\")[:20] ];"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
